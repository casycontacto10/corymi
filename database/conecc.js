// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDdZzqvK4kQNHIJe_8pApSkATstGH561L0",
  authDomain: "corymi-database.firebaseapp.com",
  databaseURL: "https://corymi-database-default-rtdb.firebaseio.com",
  projectId: "corymi-database",
  storageBucket: "corymi-database.appspot.com",
  messagingSenderId: "560375888952",
  appId: "1:560375888952:web:a2d14d1b20d55809f3facd"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);