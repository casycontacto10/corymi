import React from 'react';
import { Row } from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import Breadcrumb from 'containers/navs/Breadcrumb';
import TableResultados from 'components/admin/tablaResultados';

// import { NotificationManager } from 'components/common/react-notifications';
// import { database } from '../../helpers/Firebase';

const Start = ({ match }) => (
  <>
    <Row>
      <Colxx xxs="12">
        <Breadcrumb heading="menu.start" match={match} />
        <Separator className="mb-5" />
      </Colxx>
    </Row>
    <Row>
      <Colxx xxs="12" className="mb-4">
        <p>
          Resultados
        </p>
      </Colxx>
    </Row>
    <Row>
      <Colxx xxs="12">
        <TableResultados />
      </Colxx>
    </Row>
  </>
);
export default Start;
