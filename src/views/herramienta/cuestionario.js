import React from 'react';
import { Row } from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import DatosGenerales from 'containers/wizard/DatosGenerales';

const Cuestionario = () => {
  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      <Row>
        <Colxx xxs="12" className="mb-5">
          <h3 className="text-center mb-4">Por favor contesta las siguientes preguntas:</h3>
          <DatosGenerales />
        </Colxx>
      </Row>
    </>
  );
};

export default Cuestionario;
