import React from 'react';
import { Card, CardBody, CardText, CardHeader } from 'reactstrap';

const Consentimiento = () => {
  return (
    <>
      <Card body inverse color="primary">
        <CardHeader tag="h1" className="text-center">
          Carta de consentimiento Informado para participar en este estudio
        </CardHeader>
        <CardBody>
          <CardText className="text-justified">
            <p>
              La Red para la promoción de la salud, educación y bienestar
              psicosocial en comunidades rurales y migrantes (RED CORYMI),
              integrada por investigadores de diferentes universidades y por
              organizaciones de la sociedad civil, te invita a participar en un
              estudio que tiene como objetivo generar conocimiento que pueda
              aplicarse al diseño o mejora de programas de intervención que
              beneficien la salud física y emocional de los trabajadores agrícolas
              y de sus familias, así como a las personas que tienen la experiencia
              de la migración.
            </p>
            <p>
              La Red para la promoción de la salud, educación y bienestar
              psicosocial en comunidades rurales y migrantes (RED CORYMI),
              integrada por investigadores de diferentes universidades y por
              organizaciones de la sociedad civil, te invita a participar en un
              estudio que tiene como objetivo generar conocimiento que pueda
              aplicarse al diseño o mejora de programas de intervención que
              beneficien la salud física y emocional de los trabajadores agrícolas
              y de sus familias, así como a las personas que tienen la experiencia
              de la migración.
            </p>
            <p>
              En caso de que tengas alguna duda, puedes comunicarte con la
              responsable general de esta investigación la Dra. María Elena Rivera
              Heredia, Al correo electrónico: contacto@redcorymi.org.
            </p>
            <p>Agradecemos tu tiempo y disposición.</p>
            <p>
              He leído y comprendido los compromisos y las responsabilidades del
              estudio. Y es mi deseo libre el participar en el mismo.
            </p>
          </CardText>
          <div className="text-center">
            <a
              className="btn btn-light btn-xl mr-2 mb-2 mt-2"
              href="/herramienta/cuestionario"
            >
              CONTINUAR <i className="simple-icon-arrow-right" />
            </a>
          </div>
        </CardBody>
      </Card>
      <div className="text-center mt-4">
        <img src="../assets/img/landing-page/logos_pie_pagina.png" style={{ maxWidth: '200px' }} alt="" />
      </div>
    </>
  );
};

export default Consentimiento;
