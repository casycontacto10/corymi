import React from 'react';
import { Row } from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import DatosTisteza from 'containers/wizard/DatosTristeza';
// import { useLocation } from "react-router-dom";

const CuestionarioTristeza = () => {
  // const location = useLocation();
  // console.log('desde enojo:', location.state.regitro_id);

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      <Row>
        <Colxx xxs="12" className="mb-5">
          <h2 className="mb-4">Dificultades en el manejo de la tristeza</h2>
          <p>Por favor selecciona la opción con la que te sientas mas identificado</p>
          <DatosTisteza />
        </Colxx>
      </Row>
    </>
  );
};

export default CuestionarioTristeza;
