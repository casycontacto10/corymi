import React from 'react';
import { Row } from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import DatosOptimismo from 'containers/wizard/DatosOptimismo';
// import { useLocation } from "react-router-dom";

const CuestionarioOptimismo = () => {
  // const location = useLocation();
  // console.log('desde enojo:', location.state.regitro_id);

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      <Row>
        <Colxx xxs="12" className="mb-5">
          <h2 className="mb-4">Optimismo</h2>
          <p>Por favor selecciona la opción con la que te sientas mas identificado</p>
          <DatosOptimismo />
        </Colxx>
      </Row>
    </>
  );
};

export default CuestionarioOptimismo;
