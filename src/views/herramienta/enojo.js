import React from 'react';
import { Row } from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import DatosEnojo from 'containers/wizard/DatosEnojo';
// import { useLocation } from "react-router-dom";

const CuestionarioEnojo = () => {
  // const location = useLocation();
  // console.log('desde enojo:', location.state.regitro_id);

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      <Row>
        <Colxx xxs="12" className="mb-5">
          <h2 className="mb-4">Dificultades en el manejo del enojo</h2>
          <p>Por favor selecciona la opción con la que te sientas mas identificado</p>
          <DatosEnojo />
        </Colxx>
      </Row>
    </>
  );
};

export default CuestionarioEnojo;
