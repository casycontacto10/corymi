import React, { Suspense } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import AppLayout from 'layout/AppLayout';

const Cuestionario = React.lazy(() =>
  import(/* webpackChunkName: "herramienta-cuestionario" */ './cuestionario')
);

const Ejercicios = React.lazy(() =>
  import(/* webpackChunkName: "herramienta-ejercicios" */ './ejercicios')
);

const Enojo = React.lazy(() =>
  import(/* webpackChunkName: "herramienta-ejercicios" */ './enojo')
);

const Tristeza = React.lazy(() =>
  import(/* webpackChunkName: "herramienta-ejercicios" */ './tristeza')
);

const Autocontrol = React.lazy(() =>
  import(/* webpackChunkName: "herramienta-ejercicios" */ './autocontrol')
);

const Equilibrio = React.lazy(() =>
  import(/* webpackChunkName: "herramienta-ejercicios" */ './equilibrio')
);

const Reflexion = React.lazy(() =>
  import(/* webpackChunkName: "herramienta-ejercicios" */ './reflexion')
);

const RedApoyo = React.lazy(() =>
  import(/* webpackChunkName: "herramienta-ejercicios" */ './redApoyo')
);

const SolAyuda = React.lazy(() =>
  import(/* webpackChunkName: "herramienta-ejercicios" */ './solAyuda')
);

const Optimismo = React.lazy(() =>
  import(/* webpackChunkName: "herramienta-ejercicios" */ './optimismo')
);

const Consentimiento = React.lazy(()=>
  import(/* webpackChunkName: "herramienta-ejercicios" */ './consentimiento')
)
const Herramienta = ({ match }) => {
  return (
    <AppLayout>
      <div className="dashboard-wrapper">
        <Suspense fallback={<div className="loading" />}>
          <Switch>
            <Redirect
              exact
              from={`${match.url}/`}
              to={`${match.url}/consentimiento`}
            />
            <Route
              path={`${match.url}/cuestionario`}
              render={(props) => <Cuestionario {...props} />}
            />
            <Route
              path={`${match.url}/ejercicios`}
              render={(props) => <Ejercicios {...props} />}
            />
            <Route
              path={`${match.url}/enojo`}
              render={(props) => <Enojo {...props} />}
            />
            <Route
              path={`${match.url}/tristeza`}
              render={(props) => <Tristeza {...props} />}
            />
            <Route
              path={`${match.url}/autocontrol`}
              render={(props) => <Autocontrol {...props} />}
            />
            <Route
              path={`${match.url}/equilibrio`}
              render={(props) => <Equilibrio {...props} />}
            />
            <Route
              path={`${match.url}/reflexion`}
              render={(props) => <Reflexion {...props} />}
            />
            <Route
              path={`${match.url}/redApoyo`}
              render={(props) => <RedApoyo {...props} />}
            />
            <Route
              path={`${match.url}/solAyuda`}
              render={(props) => <SolAyuda {...props} />}
            />
            <Route
              path={`${match.url}/optimismo`}
              render={(props) => <Optimismo {...props} />}
            />
            <Route
              path={`${match.url}/consentimiento`}
              render={(props) => <Consentimiento {...props} />}
            />
          </Switch>
        </Suspense>
      </div>
    </AppLayout>
  );
};

export default Herramienta;
