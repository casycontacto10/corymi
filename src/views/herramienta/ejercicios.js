import React, { useState, useMemo } from 'react';
import { Row, Button, Card, CardBody, CardHeader, Collapse } from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import EnojoNiUbEjer from 'components/ejercicios/enojo/enojoNiUbEjer';
import EnojoAdsltUbEjer from 'components/ejercicios/enojo/enojoAdsltUbEjer';
import EnojoAdtUbEjer from 'components/ejercicios/enojo/enojoAdtUbEjer';
import EnojoBajo from 'components/ejercicios/enojo/enojoBajo';
import EnojoMedio from 'components/ejercicios/enojo/enojoMedio';
import EnojoAlto from 'components/ejercicios/enojo/enojoAlto';
import EnojoNiRuEjer from 'components/ejercicios/enojo/enojoNiRuEjer';
import EnojoAdsltRuEjer from 'components/ejercicios/enojo/enojoAdsltRuEjer';
import EnojoAdtRuEjer from 'components/ejercicios/enojo/enojoAdtRuEjer';
import EnojoNiIdEjer from 'components/ejercicios/enojo/enojoNiIdEjer';
import EnojoAdsltIdEjer from 'components/ejercicios/enojo/enojoAdsltIdEjer';
import EnojoAdtIdEjer from 'components/ejercicios/enojo/enojoAdtIdEjer';
import TristezaBajo from 'components/ejercicios/tristeza/tristezaBajo';
import TristezaMedio from 'components/ejercicios/tristeza/tristezaMedio';
import TristezaAlto from 'components/ejercicios/tristeza/tristezaAlto';
import TristezaNiUbEjer from 'components/ejercicios/tristeza/tristezaNiUbEjer';
import TristezaAdsltUbEjer from 'components/ejercicios/tristeza/tristezaAdsltUbEjer';
import TristezaAdtUbEjer from 'components/ejercicios/tristeza/tristezaAdtUbEjer';
import TristezaNiRuEjer from 'components/ejercicios/tristeza/tristezaNiRuEjer';
import TristezaAdsltRuEjer from 'components/ejercicios/tristeza/tristezaAdsltRuEjer';
import TristezaAdtRuEjer from 'components/ejercicios/tristeza/tristezaAdtRuEjer';
import TristezaNiIdEjer from 'components/ejercicios/tristeza/tristezNiIdEjer';
import TristezaAdsltIdEjer from 'components/ejercicios/tristeza/tristezaAdsltIdEjer';
import TristezaAdtIdEjer from 'components/ejercicios/tristeza/tristezaAdtIdEjer';
import AutoctrlBajo from 'components/ejercicios/autoctrl/autoctrlBajo';
import AutoctrlMedio from 'components/ejercicios/autoctrl/autoctrlMedio';
import AutoctrlAlto from 'components/ejercicios/autoctrl/autoctrlAlto';
import AutoctrlNiUbEjer from 'components/ejercicios/autoctrl/autoctrlNiUbEjer';
import AutoctrlAdsltUbEjer from 'components/ejercicios/autoctrl/autoctrlAdsltUbEjer';
import AutoctrlAdtUbEjer from 'components/ejercicios/autoctrl/autoctrlAdtUbEjer';
import AutoctrlNiRuEjer from 'components/ejercicios/autoctrl/autoctrlNiRuEjer';
import AutoctrlAdsltRuEjer from 'components/ejercicios/autoctrl/autoctrlAdsltRuEjer';
import AutoctrlAdtRuEjer from 'components/ejercicios/autoctrl/autoctrlAdtRuEjer';
import AutoctrlNiIdEjer from 'components/ejercicios/autoctrl/autoctrlNiIdEjer';
import AutoctrlAdsltIdEjer from 'components/ejercicios/autoctrl/autoctrlAdsltIdEjer';
import AutoctrlAdtIdEjer from 'components/ejercicios/autoctrl/autoctrlAdtIdEjer';
import EquibAlto from 'components/ejercicios/equilibrio/equibAlto';
import EquibBajo from 'components/ejercicios/equilibrio/equibBajo';
import EquibMedio from 'components/ejercicios/equilibrio/equibMedio';
import EquibNiUbEjer from 'components/ejercicios/equilibrio/equibNiUbEjer';
import EquibAdsltUbEjer from 'components/ejercicios/equilibrio/equibAdsltUbEjer';
import EquibAdtUbEjer from 'components/ejercicios/equilibrio/equibAdtUbEjer';
import EquibNiRuEjer from 'components/ejercicios/equilibrio/equibNiRuEjer';
import EquibAdsltRuEjer from 'components/ejercicios/equilibrio/equibAdsltRuEjer';
import EquibAdtRuEjer from 'components/ejercicios/equilibrio/equibAdtRuEjer';
import EquibNiIdEjer from 'components/ejercicios/equilibrio/equibNiIdEjer';
import EquibAdsltIdEjer from 'components/ejercicios/equilibrio/equibAdsltIdEjer';
import EquibAdtIdEjer from 'components/ejercicios/equilibrio/equibAdtIdEjer';
import OptimismoAlto from 'components/ejercicios/optimismo/optimismoAlto';
import OptimismoMedio from 'components/ejercicios/optimismo/optimismoMedio';
import OptimismoBajo from 'components/ejercicios/optimismo/optimismoBajo';
import OptimNiUbEjer from 'components/ejercicios/optimismo/optimNiUbEjer';
import OptimAdsltUbEjer from 'components/ejercicios/optimismo/optimAdsltUbEjer';
import OptimAdtUbEjer from 'components/ejercicios/optimismo/optimAdtUbEjer';
import OptimNiRuEjer from 'components/ejercicios/optimismo/optimNiRuEjer';
import OptimAdsltRuEjer from 'components/ejercicios/optimismo/optimAdsltRuEjer';
import OptimAdtRuEjer from 'components/ejercicios/optimismo/optimAdtRuEjer';
import OptimNiIdEjer from 'components/ejercicios/optimismo/optimNiIdEjer';
import OptimAdsltIdEjer from 'components/ejercicios/optimismo/optimAdsltIdEjer';
import OptimAdtIdEjer from 'components/ejercicios/optimismo/optimAdtIdEjer';
import RApoyoAlto from 'components/ejercicios/redApoyo/rApoyoAlto';
import RApoyoMedio from 'components/ejercicios/redApoyo/rApoyoMedio';
import RApoyoBajo from 'components/ejercicios/redApoyo/rApoyoBajo';
import RApoyoNiUbEjer from 'components/ejercicios/redApoyo/rApoyoNiUbEjer';
import RApoyoAdsltUbEjer from 'components/ejercicios/redApoyo/rApoyoAdsltUbEjer';
import RApoyoAdtUbEjer from 'components/ejercicios/redApoyo/rApoyoAdtUbEjer';
import RApoyoNiRuEjer from 'components/ejercicios/redApoyo/rApoyoNiRuEjer';
import RApoyoAdsltRuEjer from 'components/ejercicios/redApoyo/rApoyoAdsltRuEjer';
import RApoyoAdtRuEjer from 'components/ejercicios/redApoyo/rApoyoAdtRuEjer';
import RApoyoNiIdEjer from 'components/ejercicios/redApoyo/rApoyoNiIdEjer';
import RApoyoAdsltIdEjer from 'components/ejercicios/redApoyo/rApoyoAdsltIdEjer';
import RApoyoAdtIdEjer from 'components/ejercicios/redApoyo/rApoyoAdtIdEjer';
import ReflexAlto from 'components/ejercicios/reflexion/reflexAlto';
import ReflexMedio from 'components/ejercicios/reflexion/reflexMedio';
import ReflexBajo from 'components/ejercicios/reflexion/reflexBajo';
import ReflexNiUbEjer from 'components/ejercicios/reflexion/reflexNiUbEjer';
import ReflexAdsltUbEjer from 'components/ejercicios/reflexion/reflexAdsltUbEjer';
import ReflexAdtUbEjer from 'components/ejercicios/reflexion/reflexAdtUbEjer';
import ReflexNiRuEjer from 'components/ejercicios/reflexion/reflexNiRuEjer';
import ReflexAdsltRuEjer from 'components/ejercicios/reflexion/reflexAdsltRuEjer';
import ReflexAdtRuEjer from 'components/ejercicios/reflexion/reflexAdtRuEjer';
import ReflexNiIdEjer from 'components/ejercicios/reflexion/reflexNiIdEjer';
import ReflexAdsltIdEjer from 'components/ejercicios/reflexion/reflexAdsltIdEjer';
import ReflexAdtIdEjer from 'components/ejercicios/reflexion/reflexAdtIdEjer';
import SolApoyoAlto from 'components/ejercicios/solAyuda/solApoyoAlto';
import SolApoyoMedio from 'components/ejercicios/solAyuda/solApoyoMedio';
import SolApoyoBajo from 'components/ejercicios/solAyuda/solApoyoBajo';
import SolApoyoAdtIdEjer from 'components/ejercicios/solAyuda/solApoyoAdtIdEjer';
import SolApoyoAdsltIdEjer from 'components/ejercicios/solAyuda/solApoyoAdsltIdEjer';
import SolApoyoNiIdEjer from 'components/ejercicios/solAyuda/solApoyoNiIdEjer';
import SolApoyoNiRuEjer from 'components/ejercicios/solAyuda/solApoyoNiRuEjer';
import SolApoyoAdsltRuEjer from 'components/ejercicios/solAyuda/solApoyoAdsltRuEjer';
import SolApoyoAdtRuEjer from 'components/ejercicios/solAyuda/solApoyoAdtRuEjer';
import SolApoyoAdtUbEjer from 'components/ejercicios/solAyuda/solApoyoAdtUbEjer';
import SolApoyoAdsltUbEjer from 'components/ejercicios/solAyuda/solApoyoAdsltUbEjer';
import SolApoyoNiUbEjer from 'components/ejercicios/solAyuda/solApoyoNiUbEjer';
// import { useLocation } from "react-router-dom";
import { NotificationManager } from 'components/common/react-notifications';
import { useHistory } from 'react-router-dom';
import { database, auth } from '../../helpers/Firebase';

const sumaPuntuacion = (respuestasArray) => {
  return (
    respuestasArray.reduce(
      (accumulator, currentValue) => accumulator + Number(currentValue.value),
      0
    ) / respuestasArray.length
  );
};

const diagnostico = (recursosArray, suma) => {
  const sumaTem =
    (suma < 2.2 && suma > 2) || (suma < 3.2 && suma > 3)
      ? Math.trunc(suma)
      : suma;

  return recursosArray
    .filter((d) => sumaTem >= d.min && sumaTem <= d.max)
    .map((r) => {
      return r.diagnostico;
    });
};

const resultado = (recursosArray, categoria, poblacion, edad) => {
  return recursosArray[categoria][poblacion][edad];
};

const Ejercicios = () => {
  // const location = useLocation();
  // console.log('desde enojo:', location.state.regitro_id);

  // const id = '-Mp4MC_xWKBotpoiY5QL';
  const id = localStorage.getItem('recursoId');
  const history = useHistory();
  const [nombre,setNombre] = useState('');
  const [resultados, setResultados] = useState({
    enojo_bajo: false,
    enojo_medio: false,
    enojo_alto: false,
    enojo_ni_ru_ejer: false,
    enojo_adslt_ru_ejer: false,
    enojo_adt_ru_ejer: false,
    enojo_ni_ub_ejer: false,
    enojo_adslt_ub_ejer: false,
    enojo_adt_ub_ejer: false,
    enojo_ni_id_ejer: false,
    enojo_adslt_id_ejer: false,
    enojo_adt_id_ejer: false,
    tristeza_bajo: false,
    tristeza_medio: false,
    tristeza_alto: false,
    tristeza_ni_ub_ejer: false,
    tristeza_adslt_ub_ejer: false,
    tristeza_adt_ub_ejer: false,
    tristeza_ni_ru_ejer: false,
    tristeza_adslt_ru_ejer: false,
    tristeza_adt_ru_ejer: false,
    tristeza_ni_id_ejer: false,
    tristeza_adslt_id_ejer: false,
    tristeza_adt_id_ejer: false,
    autoctrl_medio: false,
    autoctrl_bajo: false,
    autoctrl_alto: false,
    autoctrl_ni_ub_ejer: false,
    autoctrl_adslt_ub_ejer: false,
    autoctrl_adt_ub_ejer: false,
    autoctrl_ni_ru_ejer: false,
    autoctrl_adslt_ru_ejer: false,
    autoctrl_adt_ru_ejer: false,
    autoctrl_ni_id_ejer: false,
    autoctrl_adslt_id_ejer: false,
    autoctrl_adt_id_ejer: false,
    equib_alto: false,
    equib_bajo: false,
    equib_medio: false,
    equib_ni_ub_ejer: false,
    equib_adslt_ub_ejer: false,
    equib_adt_ub_ejer: false,
    equib_ni_ru_ejer: false,
    equib_adslt_ru_ejer: false,
    equib_adt_ru_ejer: false,
    equib_ni_id_ejer: false,
    equib_adslt_id_ejer: false,
    equib_adt_id_ejer: false,
    optimismo_alto: false,
    optimismo_bajo: false,
    optimismo_medio: false,
    optim_ni_ub_ejer: false,
    optim_adslt_ub_ejer: false,
    optim_adt_ub_ejer: false,
    optim_ni_ru_ejer: false,
    optim_adslt_ru_ejer: false,
    optim_adt_ru_ejer: false,
    optim_ni_id_ejer: false,
    optim_adslt_id_ejer: false,
    optim_adt_id_ejer: false,
    r_apoyo_alto: false,
    r_apoyo_bajo: false,
    r_apoyo_medio: false,
    r_apoyo_ni_ub_ejer: false,
    r_apoyo_adslt_ub_ejer: false,
    r_apoyo_adt_ub_ejer: false,
    r_apoyo_ni_ru_ejer: false,
    r_apoyo_adslt_ru_ejer: false,
    r_apoyo_adt_ru_ejer: false,
    r_apoyo_ni_id_ejer: false,
    r_apoyo_adslt_id_ejer: false,
    r_apoyo_adt_id_ejer: false,
    reflex_alto: false,
    reflex_medio: false,
    reflex_bajo: false,
    reflex_ni_ub_ejer: false,
    reflex_adslt_ub_ejer: false,
    reflex_adt_ub_ejer: false,
    reflex_ni_ru_ejer: false,
    reflex_adslt_ru_ejer: false,
    reflex_adt_ru_ejer: false,
    reflex_ni_id_ejer: false,
    reflex_adslt_id_ejer: false,
    reflex_adt_id_ejer: false,
    sol_apoyo_alto: false,
    sol_apoyo_medio: false,
    sol_apoyo_bajo: false,
    sol_apoyo_adt_id_ejer: false,
    sol_apoyo_adslt_id_ejer: false,
    sol_apoyo_ni_id_ejer: false,
    sol_apoyo_ni_ru_ejer: false,
    sol_apoyo_adslt_ru_ejer: false,
    sol_apoyo_adt_ru_ejer: false,
    sol_apoyo_adt_ub_ejer: false,
    sol_apoyo_adslt_ub_ejer: false,
    sol_apoyo_ni_ub_ejer: false,
  });

  useMemo(() => {
    const refe = database.ref();

    refe
      .child(`respuestasPersona/${id}`)
      .get()
      .then((snapshot) => {
        refe.child('recursos').once(
          'value',
          (snapshotRecursos) => {
            const newResultado = [];
            const recursos = snapshotRecursos.val();
            const respuestas = snapshot.val();
            const poblacion = respuestas.preguntasGenerales[6].value;
            const edad = respuestas.preguntasGenerales[2].value;
            setNombre(respuestas.preguntasGenerales[0].value);

            const enojoSumPuntuacion = sumaPuntuacion(
              respuestas.recursos.enojo
            );
            const tristezaSumPuntuacion = sumaPuntuacion(
              respuestas.recursos.tristeza
            );
            const autoctrlSumPuntuacion = sumaPuntuacion(
              respuestas.recursos.autocontrol
            );

            const equilibrioSumPuntuacion = sumaPuntuacion(
              respuestas.recursos.equilibrio
            );

            const optimismoSumPuntuacion = sumaPuntuacion(
              respuestas.recursos.optimismo
            );
            const redApoyoSumPuntuacion = sumaPuntuacion(
              respuestas.recursos.redApoyo
            );

            const reflexionSumPuntuacion = sumaPuntuacion(
              respuestas.recursos.reflexion
            );

            const solAyudaSumPuntuacion = sumaPuntuacion(
              respuestas.recursos.solAyuda
            );

            const categoriaEnojo = diagnostico(
              recursos.enojo.puntuacion,
              enojoSumPuntuacion
            );

            const categoriaTristeza = diagnostico(
              recursos.tristeza.puntuacion,
              tristezaSumPuntuacion
            );

            const categoriaAutoctrl = diagnostico(
              recursos.autocontrol.puntuacion,
              autoctrlSumPuntuacion
            );

            const categoriaEquilibrio = diagnostico(
              recursos.equilibrio.puntuacion,
              equilibrioSumPuntuacion
            );

            const categoriaOptimismo = diagnostico(
              recursos.optimismo.puntuacion,
              optimismoSumPuntuacion
            );

            const categoriaRedApoyo = diagnostico(
              recursos.redApoyo.puntuacion,
              redApoyoSumPuntuacion
            );

            const categoriaReflexion = diagnostico(
              recursos.reflexion.puntuacion,
              reflexionSumPuntuacion
            );

            const categoriaSolAyuda = diagnostico(
              recursos.solAyuda.puntuacion,
              solAyudaSumPuntuacion
            );

            const ejerciciosEnojo = resultado(
              recursos.enojo.diagnostico,
              categoriaEnojo[0],
              poblacion,
              edad
            );

            const ejerciciosTristeza = resultado(
              recursos.tristeza.diagnostico,
              categoriaTristeza[0],
              poblacion,
              edad
            );

            const ejerciciosAutoctrl = resultado(
              recursos.autocontrol.diagnostico,
              categoriaAutoctrl[0],
              poblacion,
              edad
            );

            const ejerciciosEqilibrio = resultado(
              recursos.equilibrio.diagnostico,
              categoriaEquilibrio[0],
              poblacion,
              edad
            );

            const ejerciciosOptimismo = resultado(
              recursos.optimismo.diagnostico,
              categoriaOptimismo[0],
              poblacion,
              edad
            );

            const ejerciciosRedApoyo = resultado(
              recursos.redApoyo.diagnostico,
              categoriaRedApoyo[0],
              poblacion,
              edad
            );

            const ejerciciosReflexion = resultado(
              recursos.reflexion.diagnostico,
              categoriaReflexion[0],
              poblacion,
              edad
            );

            const ejerciciosSolAyuda = resultado(
              recursos.solAyuda.diagnostico,
              categoriaSolAyuda[0],
              poblacion,
              edad
            );

            for (let index = 0; index < ejerciciosEnojo.length; index += 1) {
              newResultado[ejerciciosEnojo[index].clave] = true;
            }
            for (let index = 0; index < ejerciciosTristeza.length; index += 1) {
              newResultado[ejerciciosTristeza[index].clave] = true;
            }

            for (let index = 0; index < ejerciciosAutoctrl.length; index += 1) {
              newResultado[ejerciciosAutoctrl[index].clave] = true;
            }

            for (
              let index = 0;
              index < ejerciciosEqilibrio.length;
              index += 1
            ) {
              newResultado[ejerciciosEqilibrio[index].clave] = true;
            }

            for (
              let index = 0;
              index < ejerciciosOptimismo.length;
              index += 1
            ) {
              newResultado[ejerciciosOptimismo[index].clave] = true;
            }

            for (let index = 0; index < ejerciciosRedApoyo.length; index += 1) {
              newResultado[ejerciciosRedApoyo[index].clave] = true;
            }

            for (
              let index = 0;
              index < ejerciciosReflexion.length;
              index += 1
            ) {
              newResultado[ejerciciosReflexion[index].clave] = true;
            }

            for (let index = 0; index < ejerciciosSolAyuda.length; index += 1) {
              newResultado[ejerciciosSolAyuda[index].clave] = true;
            }
            setResultados(newResultado);
          },
          [resultados]
        );
      })
      .catch(() => {
        NotificationManager.warning(
          'ERROR',
          'Usuario no autentificado',
          3000,
          null,
          null,
          ''
        );
      });
  }, [resultados]);

  const [selectedItem, setSelectedItem] = useState(false);
  const handleSelected = (index) => {
    setSelectedItem(index);
  };
  return (
    <>
      <div className="ejercicios">
        <Row>
          <Colxx xxs="12">
            <Separator className="mb-5" />
          </Colxx>
        </Row>

        <Row>
          <Colxx xxs="12" className="mb-5 text-center">
            <h1 className="mb-4">{`¡Gracias por contestar las preguntas ${nombre}!`}</h1>
            <p style={{ paddingLeft: '10%',paddingRight: '10%' }}>
              Con base a tus respuestas, el resultado de la evaluación de tus recursos psicológicos los puedes ver haciendo clic sobre el nombre del recurso, donde va a aparecer tu evaluación y algunos ejercicios que te proponemos para mejorarlos.
            </p>
          </Colxx>
        </Row>

        <Card body inverse color="danger">
          <CardHeader tag="h1" onClick={() => handleSelected(1)}>
            1. Dificultades en el manejo del enojo{' '}
          </CardHeader>
          <Collapse isOpen={selectedItem === 1}>
            <CardBody>
              {resultados.enojo_alto ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EnojoAlto />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.enojo_medio ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EnojoMedio />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.enojo_bajo ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EnojoBajo />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.enojo_ni_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EnojoNiRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.enojo_adslt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EnojoAdsltRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.enojo_adt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EnojoAdtRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.enojo_ni_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EnojoNiUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.enojo_adslt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EnojoAdsltUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.enojo_adt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EnojoAdtUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.enojo_ni_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EnojoNiIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.enojo_adslt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EnojoAdsltIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.enojo_adt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EnojoAdtIdEjer />
                  </Colxx>
                </Row>
              ) : null}
            </CardBody>
          </Collapse>
        </Card>

        <Card body inverse color="danger">
          <CardHeader tag="h1" onClick={() => handleSelected(2)}>
            2. Dificultades en el manejo de la tristeza{' '}
          </CardHeader>
          <Collapse isOpen={selectedItem === 2}>
            <CardBody>
              {resultados.tristeza_bajo ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <TristezaBajo />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.tristeza_medio ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <TristezaMedio />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.tristeza_alto ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <TristezaAlto />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.tristeza_ni_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <TristezaNiUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.tristeza_adslt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <TristezaAdsltUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.tristeza_adt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <TristezaAdtUbEjer />
                  </Colxx>
                </Row>
              ) : null}
              {resultados.tristeza_ni_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <TristezaNiRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.tristeza_adslt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <TristezaAdsltRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.tristeza_adt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <TristezaAdtRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.tristeza_ni_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <TristezaNiIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.tristeza_adslt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <TristezaAdsltIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.tristeza_adt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <TristezaAdtIdEjer />
                  </Colxx>
                </Row>
              ) : null}
            </CardBody>
          </Collapse>
        </Card>

        <Card body inverse color="danger">
          <CardHeader tag="h1" onClick={() => handleSelected(3)}>
            3. Autocontrol{' '}
          </CardHeader>
          <Collapse isOpen={selectedItem === 3}>
            <CardBody>
              {resultados.autoctrl_bajo ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <AutoctrlBajo />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.autoctrl_medio ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <AutoctrlMedio />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.autoctrl_alto ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <AutoctrlAlto />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.autoctrl_ni_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <AutoctrlNiUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.autoctrl_adslt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <AutoctrlAdsltUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.autoctrl_adt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <AutoctrlAdtUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.autoctrl_ni_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <AutoctrlNiRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.autoctrl_adslt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <AutoctrlAdsltRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.autoctrl_adt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <AutoctrlAdtRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.autoctrl_ni_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <AutoctrlNiIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.autoctrl_adslt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <AutoctrlAdsltIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.autoctrl_adt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <AutoctrlAdtIdEjer />
                  </Colxx>
                </Row>
              ) : null}
            </CardBody>
          </Collapse>
        </Card>

        <Card body inverse color="danger">
          <CardHeader tag="h1" onClick={() => handleSelected(4)}>
            4. Recuperación del equilibrio{' '}
          </CardHeader>
          <Collapse isOpen={selectedItem === 4}>
            <CardBody>
              {resultados.equib_alto ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EquibAlto />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.equib_bajo ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EquibBajo />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.equib_medio ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EquibMedio />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.equib_adslt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EquibAdsltUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.equib_ni_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EquibNiUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.equib_adt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EquibAdtUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.equib_ni_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EquibNiRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.equib_adslt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EquibAdsltRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.equib_adt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EquibAdtRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.equib_ni_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EquibNiIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.equib_adslt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EquibAdsltIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.equib_adt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <EquibAdtIdEjer />
                  </Colxx>
                </Row>
              ) : null}
            </CardBody>
          </Collapse>
        </Card>

        <Card body inverse color="danger">
          <CardHeader tag="h1" onClick={() => handleSelected(5)}>
            5. Optimismo{' '}
          </CardHeader>
          <Collapse isOpen={selectedItem === 5}>
            <CardBody>
              {resultados.optimismo_alto ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <OptimismoAlto />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.optimismo_medio ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <OptimismoMedio />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.optimismo_bajo ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <OptimismoBajo />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.optim_ni_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <OptimNiUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.optim_adslt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <OptimAdsltUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.optim_adt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <OptimAdtUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.optim_ni_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <OptimNiRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.optim_adslt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <OptimAdsltRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.optim_adt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <OptimAdtRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.optim_ni_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <OptimNiIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.optim_adslt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <OptimAdsltIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.optim_adt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <OptimAdtIdEjer />
                  </Colxx>
                </Row>
              ) : null}
            </CardBody>
          </Collapse>
        </Card>

        <Card body inverse color="danger">
          <CardHeader tag="h1" onClick={() => handleSelected(6)}>
            6. Red de apoyo{' '}
          </CardHeader>
          <Collapse isOpen={selectedItem === 6}>
            <CardBody>
              {resultados.r_apoyo_alto ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <RApoyoAlto />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.r_apoyo_medio ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <RApoyoMedio />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.r_apoyo_bajo ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <RApoyoBajo />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.r_apoyo_ni_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <RApoyoNiUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.r_apoyo_adslt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <RApoyoAdsltUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.r_apoyo_adt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <RApoyoAdtUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.r_apoyo_ni_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <RApoyoNiRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.r_apoyo_adslt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <RApoyoAdsltRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.r_apoyo_adt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <RApoyoAdtRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.r_apoyo_ni_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <RApoyoNiIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.r_apoyo_adslt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <RApoyoAdsltIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.r_apoyo_adt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <RApoyoAdtIdEjer />
                  </Colxx>
                </Row>
              ) : null}
            </CardBody>
          </Collapse>
        </Card>

        <Card body inverse color="danger">
          <CardHeader tag="h1" onClick={() => handleSelected(7)}>
            7. Reflexión ante los problemas{' '}
          </CardHeader>
          <Collapse isOpen={selectedItem === 7}>
            <CardBody>
              {resultados.reflex_alto ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <ReflexAlto />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.reflex_medio ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <ReflexMedio />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.reflex_bajo ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <ReflexBajo />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.reflex_ni_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <ReflexNiUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.reflex_adslt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <ReflexAdsltUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.reflex_adt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <ReflexAdtUbEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.reflex_ni_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <ReflexNiRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.reflex_adslt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <ReflexAdsltRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.reflex_adt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <ReflexAdtRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.reflex_ni_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <ReflexNiIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.reflex_adslt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <ReflexAdsltIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.reflex_adt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <ReflexAdtIdEjer />
                  </Colxx>
                </Row>
              ) : null}
            </CardBody>
          </Collapse>
        </Card>

        <Card body inverse color="danger">
          <CardHeader tag="h1" onClick={() => handleSelected(8)}>
            8. Dificultades para pedir ayuda{' '}
          </CardHeader>
          <Collapse isOpen={selectedItem === 8}>
            <CardBody>
              {resultados.sol_apoyo_alto ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <SolApoyoAlto />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.sol_apoyo_medio ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <SolApoyoMedio />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.sol_apoyo_bajo ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <SolApoyoBajo />
                  </Colxx>
                </Row>
              ) : null}
              {resultados.sol_apoyo_adt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <SolApoyoAdtIdEjer />
                  </Colxx>
                </Row>
              ) : null}
              {resultados.sol_apoyo_adslt_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <SolApoyoAdsltIdEjer />
                  </Colxx>
                </Row>
              ) : null}
              {resultados.sol_apoyo_ni_id_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <SolApoyoNiIdEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.sol_apoyo_ni_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <SolApoyoNiRuEjer />
                  </Colxx>
                </Row>
              ) : null}

              {resultados.sol_apoyo_adslt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <SolApoyoAdsltRuEjer />
                  </Colxx>
                </Row>
              ) : null}
              {resultados.sol_apoyo_adt_ru_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <SolApoyoAdtRuEjer />
                  </Colxx>
                </Row>
              ) : null}
              {resultados.sol_apoyo_adt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <SolApoyoAdtUbEjer />
                  </Colxx>
                </Row>
              ) : null}
              {resultados.sol_apoyo_adslt_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <SolApoyoAdsltUbEjer />
                  </Colxx>
                </Row>
              ) : null}
              {resultados.sol_apoyo_ni_ub_ejer ? (
                <Row>
                  <Colxx xxs="12" className="mb-5">
                    <SolApoyoNiUbEjer />
                  </Colxx>
                </Row>
              ) : null}
            </CardBody>
          </Collapse>
        </Card>
      </div>
      <Row className="text-center">
        <Colxx xxs="12" className="mb-5">
          <Button
            onClick={() => {
              auth
                .signOut()
                .then(() => {
                  history.push({
                    pathname: '/',
                  });
                })
                .catch((error) => {
                  console.log(error); // An error happened.
                });
            }}
            className="btn-lg primary"
          >
            FINALIZAR ESTUDIO
            <br /> <small>Y REGRESAR AL INICIO</small>
          </Button>
        </Colxx>
      </Row>
    </>
  );
};

export default Ejercicios;
