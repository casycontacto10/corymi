import React, { useState } from 'react';
import {
  Row,
  Card,
  CardTitle,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from 'reactstrap';
import { NavLink, useHistory } from 'react-router-dom';
// import { connect } from 'react-redux';
// import { registerUser } from 'redux/actions';

import IntlMessages from 'helpers/IntlMessages';
import { Colxx } from 'components/common/CustomBootstrap';
import { NotificationManager } from 'components/common/react-notifications';
// import { adminRoot } from 'constants/defaultValues';
import { auth } from '../../helpers/Firebase';

// const Register = ({ history }) => {
const Register = () => {
  // const [email, setEmail] = useState('');
  // const [password, setPassword] = useState('');
  // const [name, setName] = useState('');
  const [values, setValues] = useState({ name: '', password: '', email: '' });
  const history = useHistory();

  const handleChange = (event) => {
    values[event.target.name] = event.target.value;
    setValues(values);
  };

  const onUserRegister = () => {
    if (values.email !== '' && values.password !== '') {
      auth
        .createUserWithEmailAndPassword(values.email, values.password)
        .then(() => history.push({ pathname: '/app' }))
        .catch((error) =>
          NotificationManager.warning(
            error.message,
            'Login Error',
            3000,
            null,
            null,
            ''
          )
        );
    }
    // call registerUserAction()
  };

  return (
    <Row className="h-100">
      <Colxx xxs="12" md="10" className="mx-auto my-auto">
        <Card className="auth-card">
          <div className="position-relative image-side ">
            <p className="text-white h2">MAGIC IS IN THE DETAILS</p>
            <p className="white mb-0">
              Please use this form to register. <br />
              If you are a member, please{' '}
              <NavLink to="/user/login" className="white">
                login
              </NavLink>
              .
            </p>
          </div>
          <div className="form-side">
            <NavLink to="/" className="white">
              <span className="logo-single" />
            </NavLink>
            <CardTitle className="mb-4">
              <IntlMessages id="user.register" />
            </CardTitle>
            <Form>
              <FormGroup className="form-group has-float-label  mb-4">
                <Label>
                  <IntlMessages id="user.fullname" />
                </Label>
                <Input type="name" name="name" onChange={handleChange} />
              </FormGroup>

              <FormGroup className="form-group has-float-label  mb-4">
                <Label>
                  <IntlMessages id="user.email" />
                </Label>
                <Input type="email" name="email" onChange={handleChange} />
              </FormGroup>

              <FormGroup className="form-group has-float-label  mb-4">
                <Label>
                  <IntlMessages id="user.password" />
                </Label>
                <Input
                  type="password"
                  name="password"
                  onChange={handleChange}
                />
              </FormGroup>

              <div className="d-flex justify-content-end align-items-center">
                <Button
                  color="primary"
                  className="btn-shadow"
                  size="lg"
                  onClick={() => onUserRegister()}
                >
                  <IntlMessages id="user.register-button" />
                </Button>
              </div>
            </Form>
          </div>
        </Card>
      </Colxx>
    </Row>
  );
};
// const mapStateToProps = () => {};
export default Register;
/* export default connect(mapStateToProps, {
  registerUserAction: registerUser,
})(Register); */
