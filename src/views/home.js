/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/no-array-index-key, react/no-danger */
import React, { useState, useEffect, useRef } from 'react';
import classnames from 'classnames';
import { scroller } from 'react-scroll';
import Headroom from 'react-headroom';
import { useHistory } from 'react-router-dom';
import { database, auth } from '../helpers/Firebase';

const Home = () => {
  const history = useHistory();
  const [showMobileMenu, setShowMobileMenu] = useState(false);
  const refRowHome = useRef(null);
  const refSectionHome = useRef(null);
  const handleClick = (e) => {
    e.preventDefault();
    auth
      .signInAnonymously()
      .then(() => {
        const refe = database.ref();
        refe
          .child(`estadoCuestionario/inicio`)
          .get()
          .then((snapshot) => {
            const updateInicio = refe.child('estadoCuestionario');
            updateInicio.update({
              inicio: snapshot.val() + 1,
            });
          });
        history.push({
          pathname: '/herramienta/consentimiento',
        });
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  const onWindowResize = (event) => {
    const homeRect = refRowHome.current.getBoundingClientRect();

    const homeSection = refSectionHome.current;
    homeSection.style.backgroundPositionX = `${homeRect.x - 580}px`;

    if (event.target.innerWidth >= 992) {
      setShowMobileMenu(false);
    }
  };

  const onWindowClick = () => {
    setShowMobileMenu(false);
  };

  const onWindowScroll = () => {
    setShowMobileMenu(false);
  };

  useEffect(() => {
    window.addEventListener('scroll', onWindowScroll);
    window.addEventListener('resize', onWindowResize);
    window.addEventListener('click', onWindowClick);

    document.body.classList.add('no-footer');
    return () => {
      window.removeEventListener('scroll', onWindowScroll);
      window.removeEventListener('resize', onWindowResize);
      window.removeEventListener('click', onWindowClick);
      document.body.classList.remove('no-footer');
    };
  }, []);

  const scrollTo = (event, target) => {
    event.preventDefault();
    scroller.scrollTo(target, {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
      offset: -100,
    });
    return false;
  };

  return (
    <div
      className={classnames('landing-page', {
        'show-mobile-menu': showMobileMenu,
      })}
    >
      {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
      <div className="mobile-menu" onClick={(event) => event.stopPropagation()}>
        <a
          className="logo-mobile c-pointer"
          href="#scroll"
          onClick={(event) => scrollTo(event, 'home')}
        >
          <span />
        </a>
        <ul className="navbar-nav">
          <li className="nav-item">
            <a
              className="c-pointer"
              href="#scroll"
              onClick={(event) => scrollTo(event, 'layouts')}
            >
              PÁGINA PRINCIPAL
            </a>
          </li>
        </ul>
      </div>

      <div className="main-container">
        <Headroom className="landing-page-nav">
          <nav>
            <div className="container d-flex align-items-center justify-content-between">
              <ul className="navbar-nav d-none d-lg-flex flex-row">
                <li className="nav-item">
                  <a
                    className="c-pointer"
                    href="#scroll"
                    onClick={(event) => scrollTo(event, 'layouts')}
                  >
                    INICIO
                  </a>
                </li>
              </ul>
              {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
              <span
                className="mobile-menu-button"
                onClick={(event) => {
                  setShowMobileMenu(!showMobileMenu);
                  event.stopPropagation();
                }}
              >
                <i className="simple-icon-menu" />
              </span>
            </div>
          </nav>
        </Headroom>
        <div className="content-container" id="home">
          <div className="section home" ref={refSectionHome} style={{ backgroundPosition: 'left top', backgroundSize: '200%', paddingBottom: '4em' }}>
            <div className="container">
              <div className="row home-row" ref={refRowHome}>
                <div className="col-12 col-xl-6 col-lg-6 col-md-8">
                  <div className="home-text">
                    <div className="display-1">
                      Conoce y mejora tus recursos psicológicos <br />
                    </div>
                    <p className="white mb-5">
                      En la plataforma encontrarás una serie de preguntas que ayudaran a conocer el estado en el que se encuentran tus recursos psicológicos, mismos que se definen como aquellos elementos tangibles e intangibles que nos ayudan a superar los obstáculos de la vida cotidiana, pero especialmente utilizados en las situaciones de mayor dificultad, son dinámicos, ya que pueden desarrollarse, mantenerse, fortalecerse, intercambiarse o disminuir, dependen de la cultura de cada persona y de su historia personal. Los recursos que se evalúan se dividen en: recursos afectivos, cognitivos y sociales. Los cuales a su vez se dividen en otros subtipos, por ejemplo, en los afectivos se encuentran, dificultad para el manejo de la tristeza, la dificultad para el manejo del enojo, el autocontrol para el enojo, la recuperación del equilibrio y, así en los otros tipos. <br /><br />
                      Es nuestro deseo que encuentres en este espacio un lugar para conocerte más con relación a tus recursos psicológicos, así como para apoyarte en su mejoramiento. Pues sin más, bienvenido y que te sea de mucho beneficio y agrado. 
                    </p>
                    {/* eslint-disable-next-line react/jsx-no-target-blank */}
                    <a
                      className="btn btn-light btn-xl mr-2 mb-2"
                      href="#"
                      onClick={handleClick}
                    >
                      INICIAR DIAGNÓSTICO
                      <i className="simple-icon-arrow-right" />
                    </a>
                  </div>
                </div>
                <div className="col-12 col-xl-5 offset-xl-1 col-lg-6 col-md-4  d-none d-md-block">
                  {/* eslint-disable-next-line react/jsx-no-target-blank */}
                  <a href="#home" target="_blank">
                    <img
                      alt="hero"
                      src="/assets/img/landing-page/bienvenida_plataforma.png"
                      style={{maxWidth :'100%'}}
                    />
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="row" style={{ padding: '4em 0' }}>
              <div className="col-12 text-center">
              <img src="assets/img/landing-page/logo_conacyt_png.png" alt="" style={{ maxWidth: '200px', marginRight: '3em' }} /> Proyecto apoyado por el CONACYT en el año 2021.
              </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
