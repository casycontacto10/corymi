/* eslint-disable no-param-reassign */
import React, { createRef, useState } from 'react';
import { Card, CardBody, FormGroup, Label, Spinner, Row } from 'reactstrap';
import { Wizard, Steps, Step } from 'react-albus';
import { Formik, Form, Field } from 'formik';
import BottomNavigation from 'components/wizard/BottomNavigation';
import TopNavigation from 'components/wizard/TopNavigation';
import { Colxx } from 'components/common/CustomBootstrap';
import { useHistory } from 'react-router-dom';
import Firebase from '../../util/firebase';

const validateRequerido = (value) => {
  let error;
  if (!value) {
    error = 'Dato requerido';
  }
  return error;
};

const DatosEnojo = () => {
  const history = useHistory();
  const recursoId = localStorage.getItem('recursoId');
  const forms = [
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
  ];
  const [bottomNavHidden, setBottomNavHidden] = useState(false);
  const [loading, setLoading] = useState(false);
  const [fields, setFields] = useState([
    {
      valid: false,
      value: 0,
      preguntaId: 1,
      name: 'pregunta1',
    },
    {
      valid: false,
      value: 0,
      preguntaId: 2,
      name: 'pregunta2',
    },
    {
      valid: false,
      value: 0,
      preguntaId: 3,
      name: 'pregunta3',
    },
    {
      valid: false,
      value: 0,
      preguntaId: 4,
      name: 'pregunta4',
    },
    {
      valid: false,
      value: 0,
      preguntaId: 5,
      name: 'pregunta5',
    },
  ]);

  const asyncLoading = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 3000);
  };

  const onClickNext = (goToNext, steps, step) => {
    if (steps.length - 1 <= steps.indexOf(step)) {
      return;
    }
    const formIndex = steps.indexOf(step);
    const form = forms[formIndex].current;
    const { name } = fields[formIndex];
    form.submitForm().then(() => {
      const newFields = [...fields];

      newFields[formIndex].value = form.values[name];
      newFields[formIndex].valid = !form.errors[name];
      setFields(newFields);
      const cuestionarioActual = Firebase.database().ref(
        `respuestasPersona/${recursoId}`
      );
      // cuestionarioActual.child('')
      cuestionarioActual.update({
        recursos: { enojo: [...fields] },
        ejercicios: {
          autocontrol: {
            autoctrl_ni_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            autoctrl_adslt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            autoctrl_adt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            autoctrl_ni_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            autoctrl_adslt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            autoctrl_adt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            autoctrl_ni_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            autoctrl_adslt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            autoctrl_adt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
          },
          enojo: {
            enojo_ni_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            enojo_adslt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            enojo_adt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            enojo_ni_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            enojo_adslt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            enojo_adt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            enojo_ni_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            enojo_adslt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            enojo_adt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
          },
          equilibrio: {
            equib_ni_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            equib_adslt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            equib_adt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            equib_ni_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            equib_adslt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            equib_adt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            equib_ni_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            equib_adslt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            equib_adt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
          },
          optimismo: {
            optim_ni_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            optim_adslt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            optim_adt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            optim_ni_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            optim_adslt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            optim_adt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            optim_ni_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            optim_adslt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            optim_adt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
          },
          redApoyo: {
            r_apoyo_ni_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            r_apoyo_adslt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            r_apoyo_adt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            r_apoyo_ni_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            r_apoyo_adslt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            r_apoyo_adt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            r_apoyo_ni_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            r_apoyo_adslt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            r_apoyo_adt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
          },
          reflexion: {
            reflex_ni_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            reflex_adslt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            reflex_adt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            reflex_ni_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            reflex_adslt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            reflex_adt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            reflex_ni_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            reflex_adslt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            reflex_adt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
          },
          solAyuda: {
            sol_apoyo_adt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            sol_apoyo_adslt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            sol_apoyo_ni_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            sol_apoyo_ni_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            sol_apoyo_adslt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            sol_apoyo_adt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            sol_apoyo_adt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            sol_apoyo_adslt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            sol_apoyo_ni_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
          },
          tristeza: {
            tristeza_ni_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            tristeza_adslt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            tristeza_adt_ub_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            tristeza_ni_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            tristeza_adslt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            tristeza_adt_ru_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            tristeza_ni_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            tristeza_adslt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
            tristeza_adt_id_ejer: {
              diagnostico: false,
              respuestas: false,
              archivo: false,
            },
          },
        },
      });

      if (!form.errors[name] && form.touched[name]) {
        goToNext();
        step.isDone = true;
        if (steps.length - 2 <= steps.indexOf(step)) {
          setBottomNavHidden(true);
          asyncLoading();
        }
      }
    });
  };

  const onClickPrev = (goToPrev, steps, step) => {
    if (steps.indexOf(step) <= 0) {
      return;
    }
    goToPrev();
  };
  return (
    <Card>
      <CardBody className="wizard wizard-default">
        <Wizard>
          <TopNavigation className="justify-content-center" disableNav />
          <Steps>
            <Step id="step1" name="" desc="">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[0]}
                  initialValues={{
                    pregunta1: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right">
                          <FormGroup>
                            <Label>Pierdo el control cuando me enojo.</Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta1"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi siempre
                              </span>
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta1"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Algunas veces
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta1"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Rara vez
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta1"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi nunca
                              </span>
                            </div>
                            {errors.pregunta1 && touched.pregunta1 && (
                              <div className="invalid-feedback d-block">
                                {errors.pregunta1}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step2" name="" desc="">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[1]}
                  initialValues={{
                    pregunta2: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right">
                          <FormGroup>
                            <Label>
                              Cuando me enojo aviento lo primero que tengo.
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta2"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi siempre
                              </span>
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta2"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Algunas veces
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta2"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Rara vez
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta2"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi nunca
                              </span>
                            </div>
                            {errors.pregunta2 && touched.pregunta2 && (
                              <div className="invalid-feedback d-block">
                                {errors.pregunta2}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step3" name="" desc="">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[2]}
                  initialValues={{
                    pregunta3: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right">
                          <FormGroup>
                            <Label>Cuando me enojo hablo más fuerte.</Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta3"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi siempre
                              </span>
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta3"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Algunas veces
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta3"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Rara vez
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta3"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi nunca
                              </span>
                            </div>
                            {errors.pregunta3 && touched.pregunta3 && (
                              <div className="invalid-feedback d-block">
                                {errors.pregunta3}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step4" name="" desc="">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[3]}
                  initialValues={{
                    pregunta4: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right">
                          <FormGroup>
                            <Label>Ofendo a los demás cuando me enojo.</Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta4"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi siempre
                              </span>
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta4"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Algunas veces
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta4"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Rara vez
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta4"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi nunca
                              </span>
                            </div>
                            {errors.pregunta4 && touched.pregunta4 && (
                              <div className="invalid-feedback d-block">
                                {errors.pregunta4}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step5" name="" desc="">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[4]}
                  initialValues={{
                    pregunta5: '',
                  }}
                  onSubmit={() => {
                    history.push({
                      pathname: '/herramienta/tristeza',
                    });
                  }}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right">
                          <FormGroup>
                            <Label>
                              Me altero tanto cuando me enojo que me empieza a
                              doler algo.
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta5"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi siempre
                              </span>
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta5"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Algunas veces
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta5"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Rara vez
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta5"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi nunca
                              </span>
                            </div>
                            {errors.pregunta5 && touched.pregunta5 && (
                              <div className="invalid-feedback d-block">
                                {errors.pregunta5}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step6" hideTopNav>
              <div className="wizard-basic-step text-center pt-3">
                {loading ? (
                  <div>
                    <Spinner color="primary" className="mb-1" />
                    <p>f</p>
                  </div>
                ) : (
                  <div>
                    <h2 className="mb-2">f</h2>
                    <p>f</p>
                  </div>
                )}
              </div>
            </Step>
          </Steps>
          <BottomNavigation
            onClickNext={onClickNext}
            onClickPrev={onClickPrev}
            className={`justify-content-center ${
              bottomNavHidden && 'invisible'
            }`}
            prevLabel="anterior"
            nextLabel="siguiente"
          />
        </Wizard>
      </CardBody>
    </Card>
  );
};
export default DatosEnojo;
