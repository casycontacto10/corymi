/* eslint-disable no-param-reassign */
import React, { createRef, useState } from 'react';
import { Card, CardBody, FormGroup, Label, Spinner, Row } from 'reactstrap';
import { Wizard, Steps, Step } from 'react-albus';
import { Formik, Form, Field } from 'formik';
import BottomNavigation from 'components/wizard/BottomNavigation';
import TopNavigation from 'components/wizard/TopNavigation';
import { Colxx } from 'components/common/CustomBootstrap';
import { useHistory } from 'react-router-dom';
import { database } from '../../helpers/Firebase';

const validateRequerido = (value) => {
  let error;
  if (!value) {
    error = 'Dato requerido';
  }
  return error;
};

const Validation = () => {
  const history = useHistory();
  const forms = [
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
  ];
  const [bottomNavHidden, setBottomNavHidden] = useState(false);
  const [loading, setLoading] = useState(false);
  const [fields, setFields] = useState([
    {
      valid: false,
      name: 'nombre',
      value: '',
      id: 1,
    },
    {
      valid: false,
      name: 'sexo',
      value: '',
      id: 2,
    },
    {
      valid: false,
      name: 'edad',
      value: '',
      id: 3,
    },
    {
      valid: false,
      name: 'pareja',
      value: '',
      id: 4,
    },
    {
      valid: false,
      name: 'escolaridad',
      value: '',
      id: 5,
    },
    {
      valid: false,
      name: 'residencia',
      value: '',
      id: 6,
    },
    {
      valid: false,
      name: 'poblacion',
      value: '',
      id: 7,
    },
    {
      valid: false,
      name: 'ocupacion',
      value: '',
      id: 8,
    },
    {
      valid: false,
      name: 'internet',
      value: '',
      id: 9,
    },
    {
      valid: false,
      name: 'celular',
      value: '',
      id: 10,
    },
    {
      valid: false,
      name: 'computadora',
      value: '',
      id: 11,
    },
    {
      valid: false,
      name: 'pais',
      value: '',
      id: 12,
    },
    {
      valid: false,
      name: 'experiencia',
      value: '',
      id: 13,
    },
  ]);

  const asyncLoading = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 3000);
  };

  const onClickNext = (goToNext, steps, step) => {
    if (steps.length - 1 <= steps.indexOf(step)) {
      return;
    }
    const formIndex = steps.indexOf(step);
    const form = forms[formIndex].current;
    const { name } = fields[formIndex];
    form.submitForm().then(() => {
      const newFields = [...fields];

      newFields[formIndex].value = form.values[name];
      newFields[formIndex].valid = !form.errors[name];
      setFields(newFields);
      if (formIndex === 0) {
        const newcuestionario = database.ref('respuestasPersona').push();
        newcuestionario.set({ preguntasGenerales: [...fields] });
        const cuestionarioId = newcuestionario.key;
        localStorage.setItem('recursoId', cuestionarioId);
        const refe = database.ref();
        refe
          .child(`estadoCuestionario`)
          .get()
          .then((snapshot) => {
            const estados = snapshot.val();

            const updateCuestionarioEstado = refe.child('estadoCuestionario');

            updateCuestionarioEstado.update({
              inicio: estados.inicio - 1,
            });
            updateCuestionarioEstado.update({
              contestado: estados.contestado + 1,
            });
          });
      } else {
        const recursoId = localStorage.getItem('recursoId');
        const cuestionarioActual = database.ref(
          `respuestasPersona/${recursoId}`
        );
        cuestionarioActual.update({ preguntasGenerales: [...fields] });
      }

      if (!form.errors[name] && form.touched[name]) {
        goToNext();
        step.isDone = true;
        if (steps.length - 2 <= steps.indexOf(step)) {
          setBottomNavHidden(true);
          asyncLoading();
        }
      }
    });
  };

  const onClickPrev = (goToPrev, steps, step) => {
    if (steps.indexOf(step) <= 0) {
      return;
    }
    goToPrev();
  };

  return (
    <Card>
      <CardBody className="wizard wizard-default">
        <Wizard>
          <TopNavigation
            className="justify-content-center text-one"
            disableNav
          />
          <Steps>
            <Step id="step1">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[0]}
                  initialValues={{
                    nombre: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right text-center">
                          <FormGroup>
                            <Label className="text-large">
                              <b>Iniciales</b> de tu nombre y apellidos <br />
                            </Label>
                            <Field
                              className="form-control text-center text-large"
                              name="nombre"
                              validate={validateRequerido}
                            />
                            {errors.nombre && touched.nombre && (
                              <div className="invalid-feedback d-block text-one">
                                {errors.nombre}
                              </div>
                            )}
                            <small>
                              (Ejemplo: José Martínez Pérez sería JMP)
                            </small>
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step2">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[1]}
                  initialValues={{
                    sexo: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right">
                          <FormGroup>
                            <Label className="text-large">Sexo</Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                name="sexo"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">Hombre</span>
                              <Field
                                className="form-control"
                                name="sexo"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">Mujer</span>
                              <Field
                                className="form-control"
                                name="sexo"
                                type="radio"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">Otro</span>
                              {errors.sexo && touched.sexo && (
                                <div className="invalid-feedback d-block">
                                  {errors.sexo}
                                </div>
                              )}
                            </div>
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step3">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[2]}
                  initialValues={{
                    edad: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right error-l-75">
                          <FormGroup>
                            <Label className="text-large">¿Qué edad tienes?</Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                name="edad"
                                type="radio"
                                value="ni"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Menos de 12 años
                              </span>
                              <Field
                                className="form-control"
                                name="edad"
                                type="radio"
                                value="adslt"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Entre 13 y 18 años
                              </span>
                              <Field
                                className="form-control"
                                name="edad"
                                type="radio"
                                value="adt"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Más de 19 años
                              </span>
                            </div>
                            {errors.edad && touched.edad && (
                              <div className="invalid-feedback d-block">
                                {errors.edad}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step4">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[3]}
                  initialValues={{
                    pareja: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right error-l-75">
                          <FormGroup>
                            <Label className="text-large">
                              ¿Cuál es tu situación de pareja?
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                name="pareja"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Soltero (a)
                              </span>
                              <Field
                                className="form-control"
                                name="pareja"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Unión Libre
                              </span>
                              <Field
                                className="form-control"
                                name="pareja"
                                type="radio"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casado(a)
                              </span>
                              <Field
                                className="form-control"
                                name="pareja"
                                type="radio"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Separado(a)
                              </span>
                              <Field
                                className="form-control"
                                name="pareja"
                                type="radio"
                                value="5"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Divorciado(a)
                              </span>
                              <Field
                                className="form-control"
                                name="pareja"
                                type="radio"
                                value="Otro"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Viudo(a)
                              </span>
                            </div>
                            {errors.pareja && touched.pareja && (
                              <div className="invalid-feedback d-block">
                                {errors.pareja}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step5">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[4]}
                  initialValues={{
                    escolaridad: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right error-l-75">
                          <FormGroup>
                            <Label className="text-large">
                              ¿Cuál es el último grado que cursaste?
                            </Label>
                            <Field
                              className="form-control"
                              name="escolaridad"
                              validate={validateRequerido}
                            />
                            {errors.escolaridad && touched.escolaridad && (
                              <div className="invalid-feedback d-block">
                                {errors.escolaridad}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step6">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[5]}
                  initialValues={{
                    residencia: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right error-l-75">
                          <FormGroup>
                            <Label className="text-large">
                              Lugar donde vives actualmente:<br />
                              Población, municipio, estado.<br />
                              <small>
                                (Ejemplo: Cheranástico, Paracho, Michoacán).
                              </small>
                            </Label>
                            <Field
                              className="form-control"
                              name="residencia"
                              validate={validateRequerido}
                            />
                            {errors.residencia && touched.residencia && (
                              <div className="invalid-feedback d-block">
                                {errors.residencia}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step7">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[6]}
                  initialValues={{
                    poblacion: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right error-l-75">
                          <FormGroup>
                            <Label className="text-large">
                              ¿A que tipo de población perteneces?
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                name="poblacion"
                                type="radio"
                                value="urbano"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">Una ciudad</span>
                              <Field
                                className="form-control"
                                name="poblacion"
                                type="radio"
                                value="rural"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">El Campo</span>
                              <Field
                                className="form-control"
                                name="poblacion"
                                type="radio"
                                value="indigena"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Una comunidad indígena en el campo
                              </span>
                            </div>
                            {errors.poblacion && touched.poblacion && (
                              <div className="invalid-feedback d-block">
                                {errors.poblacion}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step8">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[7]}
                  initialValues={{
                    ocupacion: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right error-l-75">
                          <FormGroup>
                            <Label className="text-large">
                              ¿En qué trabajas?
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                name="ocupacion"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                En el campo
                              </span>
                              <Field
                                className="form-control"
                                name="ocupacion"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Empleado/a de gobierno
                              </span>
                              <Field
                                className="form-control"
                                name="ocupacion"
                                type="radio"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Albañil o en el sector de la construcción
                              </span>
                              <Field
                                className="form-control"
                                name="ocupacion"
                                type="radio"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Empleada doméstica
                              </span>
                              <Field
                                className="form-control"
                                name="ocupacion"
                                type="radio"
                                value="5"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                En el hogar
                              </span>
                              <Field
                                className="form-control"
                                name="ocupacion"
                                type="radio"
                                value="6"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Comerciante
                              </span>
                              <Field
                                className="form-control"
                                name="ocupacion"
                                type="radio"
                                value="7"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Empleado/a de servicios (hotel, restaurante,
                                tiendas)
                              </span>
                              <Field
                                className="form-control"
                                name="ocupacion"
                                type="radio"
                                value="8"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Empleado de una fábrica o maquiladora
                              </span>
                              <Field
                                className="form-control"
                                name="ocupacion"
                                type="radio"
                                value="9"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Profesionista independiente (ingenieros,
                                dentistas, abogados, etc.)
                              </span>
                              <Field
                                className="form-control"
                                name="ocupacion"
                                type="radio"
                                value="10"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Actualmente no trabajo
                              </span>
                              <Field
                                className="form-control"
                                name="ocupacion"
                                type="radio"
                                value="11"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">Otra</span>
                            </div>
                            {errors.ocupacion && touched.ocupacion && (
                              <div className="invalid-feedback d-block">
                                {errors.ocupacion}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step9">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[8]}
                  initialValues={{
                    internet: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right error-l-75">
                          <FormGroup>
                            <Label className="text-large">
                              ¿Tienes internet?
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                name="internet"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">Si</span>
                              <Field
                                className="form-control"
                                name="internet"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">No</span>
                            </div>
                            {errors.internet && touched.internet && (
                              <div className="invalid-feedback d-block">
                                {errors.internet}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step10">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[9]}
                  initialValues={{
                    celular: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right error-l-75">
                          <FormGroup>
                            <Label className="text-large">
                              ¿Tienes celular con acceso a internet?
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                name="celular"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">Si</span>
                              <Field
                                className="form-control"
                                name="celular"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">No</span>
                            </div>
                            {errors.celular && touched.celular && (
                              <div className="invalid-feedback d-block">
                                {errors.celular}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step11">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[10]}
                  initialValues={{
                    computadora: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right error-l-75">
                          <FormGroup>
                            <Label className="text-large">
                              ¿Tienes equipo de cómputo?
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                name="computadora"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">Si</span>
                              <Field
                                className="form-control"
                                name="computadora"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">No</span>
                            </div>
                            {errors.computadora && touched.computadora && (
                              <div className="invalid-feedback d-block">
                                {errors.computadora}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step12">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[11]}
                  initialValues={{
                    pais: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right error-l-75">
                          <FormGroup>
                            <Label className="text-large">
                              ¿En qué país vives?
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                name="pais"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">México</span>
                              <Field
                                className="form-control"
                                name="pais"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Estados unidos
                              </span>
                              <Field
                                className="form-control"
                                name="pais"
                                type="radio"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">Canadá</span>
                              <Field
                                className="form-control"
                                name="pais"
                                type="radio"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">Otra</span>
                            </div>
                            {errors.pais && touched.pais && (
                              <div className="invalid-feedback d-block">
                                {errors.pais}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step13">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[12]}
                  initialValues={{
                    experiencia: '',
                  }}
                  onSubmit={() => {
                    history.push({
                      pathname: '/herramienta/enojo',
                    });
                  }}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right error-l-75">
                          <FormGroup>
                            <Label className="text-large">
                              ¿Cuál es tu experiencia en migración?
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                name="experiencia"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Nací en México pero soy migrante y vivo en otro
                                país (Estados Unidos, Canadá o algún otro)
                              </span>
                              <Field
                                className="form-control"
                                name="experiencia"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Vivo en México y tengo familiares migrantes que
                                viven fuera de México
                              </span>
                              <Field
                                className="form-control"
                                name="experiencia"
                                type="radio"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Nací en México, pero migré a otro país y ya
                                estoy de regreso en México
                              </span>
                              <Field
                                className="form-control"
                                name="experiencia"
                                type="radio"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Soy Dreamer, es decir, nací en México pero crecí
                                en Estados Unidos
                              </span>
                              <Field
                                className="form-control"
                                name="experiencia"
                                type="radio"
                                value="5"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Soy ciudadano americano, hijo o hija de padres
                                migrantes
                              </span>
                              <Field
                                className="form-control"
                                name="experiencia"
                                type="radio"
                                value="6"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Vivo una parte del tiempo en México y otra en
                                Estados Unidos
                              </span>
                              <Field
                                className="form-control"
                                name="experiencia"
                                type="radio"
                                value="7"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Nací en otro país pero vivo en México y ya me
                                quedé a vivir ahí
                              </span>
                              <Field
                                className="form-control"
                                name="experiencia"
                                type="radio"
                                value="8"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Nací en otro país y estoy de paso por México
                                porque espero llegar a Estados Unidos
                              </span>
                              <Field
                                className="form-control"
                                name="experiencia"
                                type="radio"
                                value="9"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Por trabajo he cambiado de un estado a otro o de
                                un municipio a otro dentro de México
                              </span>
                              <Field
                                className="form-control"
                                name="experiencia"
                                type="radio"
                                value="9"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                No he sido migrante, ni tengo familiares
                                migrantes
                              </span>
                            </div>
                            {errors.experiencia && touched.experiencia && (
                              <div className="invalid-feedback d-block">
                                {errors.experiencia}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step15" hideTopNav>
              <div className="wizard-basic-step text-center pt-3">
                {loading ? (
                  <div>
                    <Spinner color="primary" className="mb-1" />
                    <p>b</p>
                  </div>
                ) : (
                  <div>
                    <h2 className="mb-2">h</h2>
                    <p>h</p>
                  </div>
                )}
              </div>
            </Step>
          </Steps>
          <BottomNavigation
            onClickNext={onClickNext}
            onClickPrev={onClickPrev}
            className={`justify-content-center ${
              bottomNavHidden && 'invisible'
            }`}
            prevLabel="anterior"
            nextLabel="siguiente"
          />
        </Wizard>
      </CardBody>
    </Card>
  );
};
export default Validation;
