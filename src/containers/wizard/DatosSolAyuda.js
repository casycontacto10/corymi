/* eslint-disable no-param-reassign */
import React, { createRef, useState } from 'react';
import { Card, CardBody, FormGroup, Label, Spinner, Row } from 'reactstrap';
import { Wizard, Steps, Step } from 'react-albus';
import { Formik, Form, Field } from 'formik';
import BottomNavigation from 'components/wizard/BottomNavigation';
import TopNavigation from 'components/wizard/TopNavigation';
import { Colxx } from 'components/common/CustomBootstrap';
import { useHistory } from 'react-router-dom';
import { database } from '../../helpers/Firebase';

const validateRequerido = (value) => {
  let error;
  if (!value) {
    error = 'Dato requerido';
  }
  return error;
};

const DatosSolAyuda = () => {
  const history = useHistory();
  const recursoId = localStorage.getItem('recursoId');
  const forms = [
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
    createRef(null),
  ];
  const [bottomNavHidden, setBottomNavHidden] = useState(false);
  const [loading, setLoading] = useState(false);
  const [fields, setFields] = useState([
    {
      valid: false,
      value: '',
      preguntaId: 1,
      name: 'pregunta1',
      frecuencia: '',
    },
    {
      valid: false,
      value: '',
      preguntaId: 2,
      name: 'pregunta2',
    },
    {
      valid: false,
      value: '',
      preguntaId: 3,
      name: 'pregunta3',
    },
    {
      valid: false,
      value: '',
      preguntaId: 4,
      name: 'pregunta4',
    },
    {
      valid: false,
      value: '',
      preguntaId: 5,
      name: 'pregunta5',
    },
  ]);

  const asyncLoading = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 3000);
  };

  const onClickNext = (goToNext, steps, step) => {
    if (steps.length - 1 <= steps.indexOf(step)) {
      return;
    }
    const formIndex = steps.indexOf(step);
    const form = forms[formIndex].current;
    const { name } = fields[formIndex];
    form.submitForm().then(() => {
      const newFields = [...fields];

      newFields[formIndex].value = form.values[name];
      newFields[formIndex].valid = !form.errors[name];
      setFields(newFields);
      const cuestionarioActual = database.ref(
        `respuestasPersona/${recursoId}/recursos`
      );
      cuestionarioActual.update({ solAyuda: [...fields] });

      if (!form.errors[name] && form.touched[name]) {
        goToNext();
        step.isDone = true;
        if (steps.length - 2 <= steps.indexOf(step)) {
          setBottomNavHidden(true);
          asyncLoading();
        }
      }
    });
  };

  const onClickPrev = (goToPrev, steps, step) => {
    if (steps.indexOf(step) <= 0) {
      return;
    }
    goToPrev();
  };

  return (
    <Card>
      <CardBody className="wizard wizard-default">
        <Wizard>
          <TopNavigation className="justify-content-center" disableNav />
          <Steps>
            <Step id="step1" name="" desc="">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[0]}
                  initialValues={{
                    pregunta1: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right">
                          <FormGroup>
                            <Label>
                              Para mí es muy difícil pedir ayuda a los demás.
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta1"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi siempre
                              </span>
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta1"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Algunas veces
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta1"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Rara vez
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta1"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi nunca
                              </span>
                            </div>
                            {errors.pregunta1 && touched.pregunta1 && (
                              <div className="invalid-feedback d-block">
                                {errors.pregunta1}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step2" name="" desc="">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[1]}
                  initialValues={{
                    pregunta2: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right">
                          <FormGroup>
                            <Label>
                              Si tuviera un problema muy grave, no sabría a
                              quién recurrir.
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta2"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi siempre
                              </span>
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta2"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Algunas veces
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta2"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Rara vez
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta2"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi nunca
                              </span>
                            </div>
                            {errors.pregunta2 && touched.pregunta2 && (
                              <div className="invalid-feedback d-block">
                                {errors.pregunta2}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step3" name="" desc="">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[2]}
                  initialValues={{
                    pregunta3: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right">
                          <FormGroup>
                            <Label>
                              Evito acercarme a la gente cuando tengo problemas.
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta3"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi siempre
                              </span>
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta3"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Algunas veces
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta3"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Rara vez
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta3"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi nunca
                              </span>
                            </div>
                            {errors.pregunta3 && touched.pregunta3 && (
                              <div className="invalid-feedback d-block">
                                {errors.pregunta3}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step4" name="" desc="">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[3]}
                  initialValues={{
                    pregunta4: '',
                  }}
                  onSubmit={() => {}}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right">
                          <FormGroup>
                            <Label>
                              Cuando he pedido ayuda a los miembros de mi
                              familia me han fallado.
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta4"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi siempre
                              </span>
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta4"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Algunas veces
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta4"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Rara vez
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta4"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi nunca
                              </span>
                            </div>
                            {errors.pregunta4 && touched.pregunta4 && (
                              <div className="invalid-feedback d-block">
                                {errors.pregunta4}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step5" name="" desc="">
              <div className="wizard-basic-step">
                <Formik
                  innerRef={forms[4]}
                  initialValues={{
                    pregunta5: '',
                  }}
                  onSubmit={() => {
                    const refe = database.ref();
                    refe
                      .child(`estadoCuestionario`)
                      .get()
                      .then((snapshot) => {
                        const estados = snapshot.val();

                        const updateCuestionarioEstado =
                          refe.child('estadoCuestionario');

                        updateCuestionarioEstado.update({
                          contestado: estados.contestado - 1,
                        });
                        updateCuestionarioEstado.update({
                          completo: estados.completo + 1,
                        });
                      });
                    history.push({
                      pathname: '/herramienta/ejercicios',
                    });
                  }}
                >
                  {({ errors, touched }) => (
                    <Row className="justify-content-md-center mt-5">
                      <Colxx xxs="12" xl="6" className="mb-5">
                        <Form className="av-tooltip tooltip-label-right">
                          <FormGroup>
                            <Label>
                              Me da pena acercarme a la gente cuando la
                              necesito.
                            </Label>
                            <div role="group" aria-labelledby="my-radio-group">
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta5"
                                value="4"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi siempre
                              </span>
                              <Field
                                className="form-control"
                                type="radio"
                                name="pregunta5"
                                value="3"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Algunas veces
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta5"
                                type="radio"
                                value="2"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Rara vez
                              </span>
                              <Field
                                className="form-control"
                                name="pregunta5"
                                type="radio"
                                value="1"
                                validate={validateRequerido}
                              />
                              <span className="form-control-label">
                                Casi nunca
                              </span>
                            </div>
                            {errors.pregunta4 && touched.pregunta4 && (
                              <div className="invalid-feedback d-block">
                                {errors.pregunta5}
                              </div>
                            )}
                          </FormGroup>
                        </Form>
                      </Colxx>
                    </Row>
                  )}
                </Formik>
              </div>
            </Step>
            <Step id="step6" hideTopNav>
              <div className="wizard-basic-step text-center pt-3">
                {loading ? (
                  <div>
                    <Spinner color="primary" className="mb-1" />
                    <p>h</p>
                  </div>
                ) : (
                  <div>
                    <h2 className="mb-2">h</h2>
                    <p>h</p>
                  </div>
                )}
              </div>
            </Step>
          </Steps>
          <BottomNavigation
            onClickNext={onClickNext}
            onClickPrev={onClickPrev}
            className={`justify-content-center ${
              bottomNavHidden && 'invisible'
            }`}
            prevLabel="anterior"
            nextLabel="siguiente"
          />
        </Wizard>
      </CardBody>
    </Card>
  );
};
export default DatosSolAyuda;
