// Import the functions you need from the SDKs you need
import Firebase from 'firebase'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDdZzqvK4kQNHIJe_8pApSkATstGH561L0",
  authDomain: "corymi-database.firebaseapp.com",
  databaseURL: "https://corymi-database-default-rtdb.firebaseio.com",
  projectId: "corymi-database",
  storageBucket: "corymi-database.appspot.com",
  messagingSenderId: "560375888952",
  appId: "1:560375888952:web:a2d14d1b20d55809f3facd"
};

// Initialize Firebase
// Firebase.initializeApp(firebaseConfig);
if (!Firebase.apps.length) {
    Firebase.initializeApp(firebaseConfig);
 }else {
    Firebase.app(); // if already initialized, use that one
 }
// Firebase.analytics()
export default Firebase;