import React, {  useState, useMemo } from 'react';
import { NotificationManager } from 'components/common/react-notifications';

import { database } from '../../helpers/Firebase';

const sumaPuntuacion = (respuestasArray) => {
  if (!respuestasArray) return 'no contesto';
  return (
    respuestasArray.reduce(
      (accumulator, currentValue) => accumulator + Number(currentValue.value),
      0
    ) / respuestasArray.length
  );
};

const diagnostico = (recursosArray, suma) => {
  const sumaTem =
    (suma < 2.2 && suma > 2) || (suma < 3.2 && suma > 3)
      ? Math.trunc(suma)
      : suma;

  return recursosArray
    .filter((d) => sumaTem >= d.min && sumaTem <= d.max)
    .map((r) => {
      return r.diagnostico;
    });
};

/* const resultado = (recursosArray, categoria, poblacion, edad) => {
  return recursosArray[categoria][poblacion][edad];
}; */

const Resultados = () => {
  const [resultados, setResultados] = useState([]);
  useMemo(() => {
    const refe = database.ref();
    refe
      .child(`respuestasPersona/`)
      .get()
      .then((snapshot) => {
        refe
          .child('recursos')
          .get()
          .then((snapshotRecursos) => {
            const newResultado = [];
            const respuestaPersona = snapshot.val();
            const recursos = snapshotRecursos.val();
            // const arrayTable = [];
            Object.keys(respuestaPersona).forEach((key) => {
              const temp = [];
              temp.push({
                id: key,
                iniciales: respuestaPersona[key].preguntasGenerales[0]
                  ? respuestaPersona[key].preguntasGenerales[0].value
                  : 'sin respuesta',
                enojo: { puntuacion: false, clasificacion: false },
                autocontrol: { puntuacion: false, clasificacion: false },
                equilibrio: { puntuacion: false, clasificacion: false },
                optimismo: { puntuacion: false, clasificacion: false },
                redApoyo: { puntuacion: false, clasificacion: false },
                reflexion: { puntuacion: false, clasificacion: false },
                solAyuda: { puntuacion: false, clasificacion: false },
                tisteza: { puntuacion: false, clasificacion: false },
              });
              /* ENOJO */
              const recursoEnojo = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.enojo
                : false;
              const enojoSumPuntuacion = sumaPuntuacion(recursoEnojo);

              temp[0].enojo.puntuacion = enojoSumPuntuacion;

              const categoriaEnojo = diagnostico(
                recursos.enojo.puntuacion,
                enojoSumPuntuacion
              );
              temp[0].enojo.clasificacion = categoriaEnojo.length
                ? categoriaEnojo[0]
                : 'no contesto';
              /* ENOJO */
              /* AUTOCONTROL */
              const recursoAutocontrol = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.autocontrol
                : false;

              const autocontrolSumPuntuacion =
                sumaPuntuacion(recursoAutocontrol);
              
              temp[0].autocontrol.puntuacion = autocontrolSumPuntuacion;

              const categoriaAutocontrol = diagnostico(
                recursos.autocontrol.puntuacion,
                autocontrolSumPuntuacion
              );
              temp[0].autocontrol.clasificacion = categoriaAutocontrol.length
                ? categoriaAutocontrol[0]
                : 'no contesto';

              /* AUTOCONTROL */
              /* EQUILIBRIO */
              const recursoEquilibrio = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.equilibrio
                : false;

              const equilibrioSumPuntuacion = sumaPuntuacion(recursoEquilibrio);

              temp[0].equilibrio.puntuacion = equilibrioSumPuntuacion;

              const categoriaEquilibrio = diagnostico(
                recursos.equilibrio.puntuacion,
                equilibrioSumPuntuacion
              );
              temp[0].equilibrio.clasificacion = categoriaEquilibrio.length
                ? categoriaEquilibrio[0]
                : 'no contesto';
              /* EQUILIBRIO */
              /*  OPTIMISMO */
              const recursoOptimismo = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.optimismo
                : false;

              const optimismoSumPuntuacion = sumaPuntuacion(recursoOptimismo);

              temp[0].optimismo.puntuacion = optimismoSumPuntuacion;

              const categoriaOptimismo = diagnostico(
                recursos.optimismo.puntuacion,
                optimismoSumPuntuacion
              );
              temp[0].optimismo.clasificacion = categoriaOptimismo.length
                ? categoriaOptimismo[0]
                : 'no contesto';
              /* OPTIMISMO */
              /*  REDAPOYO */
              const recursoRedApoyo = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.redApoyo
                : false;

              const redApoyoSumPuntuacion = sumaPuntuacion(recursoRedApoyo);

              temp[0].redApoyo.puntuacion = redApoyoSumPuntuacion;

              const categoriaRedApoyo = diagnostico(
                recursos.redApoyo.puntuacion,
                redApoyoSumPuntuacion
              );
              temp[0].redApoyo.clasificacion = categoriaRedApoyo.length
                ? categoriaRedApoyo[0]
                : 'no contesto';
              /* REDAPOYO */
              /*  REFLEXION */
              const recursoReflexion = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.reflexion
                : false;

              const reflexionSumPuntuacion = sumaPuntuacion(recursoReflexion);

              temp[0].reflexion.puntuacion = reflexionSumPuntuacion;

              const categoriaReflexion = diagnostico(
                recursos.reflexion.puntuacion,
                reflexionSumPuntuacion
              );
              temp[0].reflexion.clasificacion = categoriaReflexion.length
                ? categoriaReflexion[0]
                : 'no contesto';
              /* REFLEXION */
              /*  SOLAYUDA */
              const recursoSolAyuda = respuestaPersona[key].recursos
              ? respuestaPersona[key].recursos.solAyuda
              : false;

            const solAyudaSumPuntuacion = sumaPuntuacion(recursoSolAyuda);

            temp[0].solAyuda.puntuacion = solAyudaSumPuntuacion;

            const categoriaSolAyuda = diagnostico(
              recursos.solAyuda.puntuacion,
              solAyudaSumPuntuacion
            );
            temp[0].solAyuda.clasificacion = categoriaSolAyuda.length
              ? categoriaSolAyuda[0]
              : 'no contesto';
            /* SOLAYUDA */
            /*  TRISTEZA */
             const recursoTristeza = respuestaPersona[key].recursos
             ? respuestaPersona[key].recursos.tisteza
             : false;

           const tristezaSumPuntuacion = sumaPuntuacion(recursoTristeza);

           temp[0].tisteza.puntuacion = tristezaSumPuntuacion;

           const categoriaTristeza = diagnostico(
             recursos.tristeza.puntuacion,
              tristezaSumPuntuacion
           );
           temp[0].tisteza.clasificacion = categoriaTristeza.length
             ? categoriaTristeza[0]
             : 'no contesto';
           /* TRISTEZA */

           newResultado.push(temp[0]);

            });
            setResultados(newResultado);
            console.log(resultados);
          })
          .catch((error) => {
            console.log(error);
            NotificationManager.warning(
              'ERROR RECURSOS',
              '',
              3000,
              null,
              null,
              ''
            );
          });
      })
      .catch(() => {
        NotificationManager.warning(
          'ERRORRESPUESTAS',
          '',
          3000,
          null,
          null,
          ''
        );
      });
  });
  { resultados.map((resultado, index) =>{

    return (

          <tr key={index}>
            <td>{resultado.iniciales}</td>
            <td>{resultado.enojo.puntuacion}</td>
            <td>{resultado.enojo.clasificacion}</td>
  
            <td>{resultado.tristeza.puntuacion}</td>
            <td>{resultado.tristeza.clasificacion}</td>
  
            <td>{resultado.autocontrol.puntuacion}</td>
            <td>{resultado.autocontrol.clasificacion}</td>
  
            <td>{resultado.equilibrio.puntuacion}</td>
            <td>{resultado.equilibrio.clasificacion}</td>
  
            <td>{resultado.optimismo.puntuacion}</td>
            <td>{resultado.optimismo.clasificacion}</td>
                      
            <td>{resultado.redApoyo.puntuacion}</td>
            <td>{resultado.redApoyo.clasificacion}</td>
  
            <td>{resultado.reflexion.puntuacion}</td>
            <td>{resultado.reflexion.clasificacion}</td>
  
            <td>{resultado.solAyuda.puntuacion}</td>
            <td>{resultado.solAyuda.clasificacion}</td>
  
            <td>Detalle</td>
          </tr>
    )

  })};
};
export default Resultados;
