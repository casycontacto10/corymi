/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-key */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/display-name */
import React from 'react';
import { useTable, usePagination, useSortBy } from 'react-table';
import classnames from 'classnames';
import DatatablePagination from 'components/DatatablePagination';

function Table({ columns, data, divided = false, defaultPageSize = 10 }) {
  const {
    getTableProps,
    getTableBodyProps,
    prepareRow,
    headerGroups,
    page,
    canPreviousPage,
    canNextPage,
    pageCount,
    gotoPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, pageSize: defaultPageSize },
    },
    useSortBy,
    usePagination
  );

  return (
    <>
      <table
        {...getTableProps()}
        className={`r-table table ${classnames({ 'table-divided': divided })}`}
      >
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column, columnIndex) => (
                <th
                  key={`th_${columnIndex}`}
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                  className={
                    column.isSorted
                      ? column.isSortedDesc
                        ? 'sorted-desc'
                        : 'sorted-asc'
                      : ''
                  }
                >
                  {column.render('Header')}
                  <span />
                </th>
              ))}
            </tr>
          ))}
        </thead>

        <tbody {...getTableBodyProps()}>
          {page.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell, cellIndex) => (
                  <td
                    key={`td_${cellIndex}`}
                    {...cell.getCellProps({
                      className: cell.column.cellClass,
                    })}
                  >
                    {cell.render('Cell')}
                  </td>
                ))}
              </tr>
            );
          })}
        </tbody>
      </table>

      <DatatablePagination
        page={pageIndex}
        pages={pageCount}
        canPrevious={canPreviousPage}
        canNext={canNextPage}
        pageSizeOptions={[4, 10, 20, 30, 40, 50]}
        showPageSizeOptions={false}
        showPageJump={false}
        defaultPageSize={pageSize}
        onPageChange={(p) => gotoPage(p)}
        onPageSizeChange={(s) => setPageSize(s)}
        paginationMaxSize={pageCount}
      />
    </>
  );
}
const TableContainer = ({ data }) => {
  const headersTable = React.useMemo(
    () => [
      {
        Header: 'clave',
        accessor: 'id',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'iniciales',
        accessor: 'iniciales',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Puntaje enojo',
        accessor: 'enojoPuntuacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Diagnostico enojo',
        accessor: 'enojoClasificacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Puntaje tristeza',
        accessor: 'tristezaPuntuacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Diagnostico tristeza',
        accessor: 'tristezaClasificacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Puntaje autocontrol',
        accessor: 'autocontrolPuntuacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Diagnostico autocontrol',
        accessor: 'autocontrolClasificacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Puntaje equilibrio',
        accessor: 'equilibrioPuntuacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Diagnostico equilibrio',
        accessor: 'equilibrioClasificacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Puntaje optimismo',
        accessor: 'optimismoPuntuacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Diagnostico optimismo',
        accessor: 'optimismoClasificacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Puntaje red apoyo',
        accessor: 'redApoyoPuntuacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Diagnostico red apoyo',
        accessor: 'redApoyoClasificacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Puntaje reflexion',
        accessor: 'reflexionPuntuacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Diagnostico reflexion',
        accessor: 'reflexionClasificacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Puntaje solicitar ayuda',
        accessor: 'solAyudaPuntuacion',
        cellClass: 'text-muted w-40',
      },
      {
        Header: 'Diagnostico solicitar ayuda',
        accessor: 'solAyudaClasificacion',
        cellClass: 'text-muted w-40',
      },
    ],
    []
  );

  return <Table columns={headersTable} data={data} divided />;
};

export default TableContainer;
