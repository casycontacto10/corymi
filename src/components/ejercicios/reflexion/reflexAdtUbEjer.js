import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const ReflexAdtUbEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta: false,
  });
  const handleChange = (event) => {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const reflexAdtUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/reflexion/reflex_adt_ub_ejer`
    );
    reflexAdtUbEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Cómo crees que se sintió Jazmín después de reflexionar los esfuerzos de sus padres para que ella siga estudiando? ',
            respuesta: event.target.value,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.respuesta = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.respuesta = event.target.value;
          setEstado(estado);
        }
      }
    );
  };
  return (
    <Card body inverse color="success">
      <CardHeader tag="h1">Ejercicio reflexión</CardHeader>
      <CardBody>
        <CardText>
          <p>
            -Ante una situación problemática, es necesario que antes de tomar
            decisiones, nos tomemos un tiempo para reflexionar acerca de las
            posibles soluciones o alternativas. A veces bastan sólo unos
            minutos, pero en otras ocasiones requerimos más tiempo porque se
            trata de decisiones importantes. Por ello, también es necesario
            reconocer que en ocasiones necesitamos orientación de un profesional
            para tomar la mejor decisión.
          </p>
          <p>Ejemplo:</p>
          <p>
            Jazmín tiene 11 años y vive con sus abuelos porque sus papás están
            en Estados Unidos desde hace 7 años. Jazmín apenas los recuerda,
            porque era muy pequeña cuando se fueron a trabajar al país del
            Norte. En la escuela casi no hablaba con nadie porque creía que por
            no tener a sus papás cerca, sus demás compañeras no la aceptarían.
            Sin embargo, un día al llegar a su entrenamiento de básquet-bol, se
            percató de que había olvidado sus tenis, situación que le generó
            mucha preocupación porque el juego estaba a punto de comenzar,
            entonces su compañera Daniela se dio cuenta de lo que pasaba y le
            dijo que no se preocupara, que podía usar unos tenis que tenía de
            repuesto, y mientras Jazmín se abrochaba las agujetas comenzaron a
            platicar sobre el equipo contrario, sonrieron y desde ese día están
            siempre juntas. De esta manera Jazmín, se dio cuenta de que tener a
            sus padres lejos no es un problema porque se fueron en busca de un
            futuro mejor para ella y para la familia, y que eso no debería
            perjudicar su relación con otras personas.
          </p>
          <p>
            ¿Cómo crees que se sintió Jazmín después de reflexionar los
            esfuerzos de sus padres para que ella siga estudiando? Elije una
            opción:
          </p>
          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="Agradecida porque sabe que hacen un esfuerzo para que ella pueda estudiar"
                  onChange={handleChange}
                />
              </td>
              <td>
                Agradecida porque sabe que hacen un esfuerzo para que ella pueda
                estudiar
              </td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="Enojada con su familia"
                  onChange={handleChange}
                />
              </td>
              <td>Enojada con su familia</td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta}</p>
                <p>Actualizado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert>
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default ReflexAdtUbEjer;
