import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const ReflexAdtIdEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  function handleChange(event) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const reflexAdtIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/reflexion/reflex_adt_id_ejer`
    );
    reflexAdtIdEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿qué decisión apoyarías?',
            respuesta: event.target.value,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = event.target.value;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="success">
      <CardHeader tag="h1">Ejercicio reflexión</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta las preguntas de reflexión:</p>
          <p>
            Desde hace muchos años, la familia Pérez tiene la costumbre de
            regalar arreglos florales para la fiesta del pueblo, pero este año
            han tenido pocas ventas en su negocio y no podrían hacer ese gasto
            pero tampoco quieren romper la tradición familiar. El hijo mayor
            opina que podrían usar los ahorros de la familia pero su hermana le
            recuerda que ese ahorro es para emergencias y deben pensar muy bien
            si es más importante seguir la tradición o seguir guardando el
            dinero.
          </p>
          <p>Si formaras parte de la familia Pérez ¿qué decisión apoyarías?</p>

          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="resp"
                  value="Seguir con la tradición familiar"
                  onChange={handleChange}
                />
              </td>
              <td>Seguir con la tradición familiar</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="resp"
                  value="Guardar el dinero ahorrado"
                  onChange={handleChange}
                />
              </td>
              <td>Guardar el dinero ahorrado</td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>

          <br />

          <p>Recuerda que antes de tomar una decisión importante puedes:</p>

          <ol>
            <li>Pedir la opinión de alguien más</li>
            <li>Pensar y escribir diferentes soluciones</li>
            <li>Pensar en las ventajas y desventajas de la decisión</li>
          </ol>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default ReflexAdtIdEjer;
