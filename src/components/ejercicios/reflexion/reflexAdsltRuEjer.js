import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const ReflexAdsltRuEjer = () => {
  const [preguntas, setPreguntas] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  const handleChange = (event) => {
    setPreguntas(event.target.value);
  };

  function setRespuesta(event) {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const reflexAdsltRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/reflexion/reflex_adslt_ru_ejer`
    );
    reflexAdsltRuEjer.update(
      {
        respuestas: [
          {
            pregunta: 'Describe la forma en la cual este ejercicio te ayudo',
            respuesta: preguntas,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = preguntas;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse color="success">
      <CardHeader tag="h1">Ejercicio reflexión</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Cuando tengas un problema y no lo puedas resolver intenta lo
            siguiente:
          </p>

          <ol>
            <li>Describe exactamente cual es problema que quieres resolver</li>
            <li>
              Haz una lluvia de ideas con diferentes formas en las cuales
              consideras se puede resolver el problema o la situación, lo
              importante es tener varias opciones
            </li>
            <li>
              A partir de la lista de soluciones identifica cual sería la que
              funcionaria mejor y la que es más posible de realizar
            </li>
            <li>Diseña un plan para implementar la solución</li>
            <li>
              Evalúa que tan efectiva fue la solución y en caso de no haber
              funcionado trata con otra alternativa
            </li>
          </ol>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>Describe la forma en la cual este ejercicio te ayudo</td>
              </tr>
              <tr>
                <td>
                  <textarea
                    name="preguntas"
                    onChange={handleChange}
                    rows="5"
                    style={{
                      height: '100%',
                      width: '100%',
                      marginTop: '0.5em',
                    }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default ReflexAdsltRuEjer;
