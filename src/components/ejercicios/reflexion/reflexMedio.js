import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const ReflexMedio = () => {
  return (
    <Card body inverse color="success">
      <CardHeader tag="h1">Reflexión ante los problemas</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: MEDIO</CardTitle>
        <CardText>
          <p>
            Tu puntaje en Reflexión ante los problemas indica que en ocasiones
            para ti es difícil pensar en una situación problemática y analizarla
            para buscar una solución.{' '}
          </p>
          <p>
            A veces si te es posible reflexionar cuidadosamente de las cosas
            antes de tomar una decisión, así como sobre tu propio desempeño, y
            de las situaciones en torno a los problemas que enfrentas. Sin
            embargo, tu esfuerzo se queda en un nivel intermedio y podría
            mejorar ti capacidad de reflexión y aprendizaje ante los problemas
            que vas enfrentando en la vida.
          </p>
          <p>
            A continuación, te pedimos que realices un ejercicio que te ayudará
            a mejorar tu capacidad para reflexionar y buscar soluciones a los
            problemas a los que puedas enfrentarte.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default ReflexMedio;
