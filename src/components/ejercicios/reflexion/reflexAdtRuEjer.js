import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const ReflexAdtRuEjer = () => {
  const [preguntas, setPreguntas] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta: '',
  });

  const handleChange = (event) => {
    setPreguntas(event.target.value);
  };

  function setRespuesta(event) {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const reflexAdsltRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/reflexion/reflex_adt_ru_ejer`
    );
    reflexAdsltRuEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Cuál de las técnicas anteriormente presentadas es la que te gusta más?',
            respuesta: preguntas,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.respuesta = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.respuesta = preguntas;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse color="success">
      <CardHeader tag="h1">Ejercicio reflexión</CardHeader>
      <CardBody>
        <CardText>
          <p>Aplicando las técnicas para solucionar problemas.</p>
          <p>
            Cuando te tienes que enfrentar a un problema difícil, ¿por dónde
            empiezas? ¿Qué técnicas de resolución de problemas puedes utilizar
            que te puedan ayudar a tomar mejores decisiones?
          </p>
          <ol>
            <li>
              Piensa en las posibles soluciones que se tienen. Céntrate en
              generar muchas ideas, no las priorices ni evalúes hasta que las
              hayas anotado todas
            </li>
            <li>
              Replantea los problemas con preguntas. Es más fácil conseguir un
              resultado cuando no piensas en el problema como tal. Generar
              preguntas sobre el tema ayuda mucho. Por ejemplo: en lugar de
              pensar que “No tengo tiempo para poder hacer todo lo que necesito”
              puedes transformar esa idea en preguntas que puedan hacerte ver de
              otra forma el problema ¿Qué cosas necesito terminar de forma
              inmediata? ¿podría pedir ayuda de alguien? Etc
            </li>
            <li>
              Remplaza la palabra “pero” por una solución. Un ejemplo sería “Me
              gustaría volver a estudiar, pero no tengo tiempo” por “Me gustaría
              volver a estudiar, de modo que voy a buscar las opciones que me
              ajusten mejor” o también se podría cambiar el pero de lugar… “No
              tengo tiempo, pero me gustaría volver a estudiar”
            </li>
          </ol>
          <p>Contesta la siguiente pregunta:</p>

          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  ¿Cuál de las técnicas anteriormente presentadas es la que te
                  gusta más?
                </td>
              </tr>
              <tr>
                <td>
                  <textarea
                    name="preguntas"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default ReflexAdtRuEjer;
