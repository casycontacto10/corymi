import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const ReflexAdsltIdEjer = () => {
  const [preguntas, setPreguntas] = useState({
    p1: '',
    p2: '',
    otro: '',
    disable: true,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    resp2: false,
    otro: false,
  });
  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
    if (event.target.name === 'p2' && event.target.value !== 'otro') {
      preguntas.otro = '';
      preguntas.disable = true;
    }
    if (event.target.value === 'otro') {
      preguntas.disable = false;
    }
  };

  function setRespuesta(event) {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const reflexAdsltIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/reflexion/reflex_adslt_id_ejer`
    );
    reflexAdsltIdEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Qué podría decir Carlos para convencer a sus papás de que lo dejen irse a la ciudad?',
            respuesta: preguntas.p1,
          },
          {
            pregunta: '¿Tú qué haces cuando tienes que resolver un problema?',
            respuesta: preguntas.p2,
            otro_texto: preguntas.otro,
            otro: !preguntas.disable,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.resp2 = false;
        estado.otro = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = preguntas.p1;
          estado.resp2 = preguntas.p2;
          estado.otro = preguntas.otro;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="success">
      <CardHeader tag="h1">Ejercicio reflexión</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta las preguntas de reflexión:</p>
          <p>
            Carlos está a punto de terminar la preparatoria y quiere ser
            ingeniero, pero en su comunidad no hay universidad, así que quiere
            irse a la ciudad para seguir estudiando. Sus papás no quieren que se
            vaya porque allá no conoce a nadie y tienen miedo de que, si algo le
            pasa, nadie lo podría ayudar. Carlos sueña que cuando sea ingeniero,
            podrá conseguir un buen trabajo para apoyar económicamente a sus
            padres, pero si no lo dejan ir, no podrá cumplir ese sueño.
          </p>
          <p>
            ¿Qué podría decir Carlos para convencer a sus papás de que lo dejen
            irse a la ciudad?
          </p>

          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="Decirles que él quiere cumplir su sueño"
                  onChange={handleChange}
                />
              </td>
              <td>Decirles que él quiere cumplir su sueño</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="No decir nada y quedarse para que ellos estén tranquilos"
                  onChange={handleChange}
                />
              </td>
              <td>No decir nada y quedarse para que ellos estén tranquilos</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="Explicarles que quiere ayudarlos económicamente"
                  onChange={handleChange}
                />
              </td>
              <td>Explicarles que quiere ayudarlos económicamente</td>
            </tr>
          </table>

          <br />
          <p>¿Tú qué haces cuando tienes que resolver un problema?</p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p2"
                    value="Buscar que alguien me ayude"
                    onChange={handleChange}
                  />
                </td>
                <td>Buscar que alguien me ayude</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p2"
                    value="Dejar que se resuelva solo"
                    onChange={handleChange}
                  />
                </td>
                <td>Dejar que se resuelva solo</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p2"
                    value="Pensar en posibles soluciones"
                    onChange={handleChange}
                  />
                </td>
                <td>Pensar en posibles soluciones</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p2"
                    value="otro"
                    onChange={handleChange}
                  />
                </td>
                <td>Otro</td>
                <td>
                  <textarea
                    disabled={preguntas.disable}
                    name="otro"
                    value={preguntas.otro}
                    onChange={handleChange}
                    rows="5"
                    style={{ height: '100%', width: '100%' }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.resp2}</p>
                <p>{estado.otro}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default ReflexAdsltIdEjer;
