import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const ReflexNiRuEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  const handleChange = (event) => {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const reflexNiRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/reflexion/reflex_ni_ru_ejer`
    );
    reflexNiRuEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Si fueras Pedrito cuál crees que sería la mejor opción para resolver su problema? ',
            respuesta: event.target.value,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.resp = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.resp = event.target.value;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  };

  return (
    <Card body inverse color="success">
      <CardHeader tag="h1">Ejercicio reflexión</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Pedrito era un niño muy inquieto, todo el tiempo estaba brincando y
            corriendo por todas partes, un día en casa de su tita Pedrito tuvo
            la idea de jugar a la pelota mientras su tita echaba tortillas, de
            pronto Pedrito pateó la pelota tan fuerte que rompió una maceta, en
            ese momento Pedrito se asustó y corrió a esconderse pensando en que
            se había metido en problemas.
          </p>
          <p>
            Piensa que cuando tengas un problema como Pedrito, primero intenta
            estar tranquilo después piensa en varias cosas que te puedan ayudar
            a resolverlo y escoge la mejor, recuerda que también puedes pedir
            ayuda de un adulto y juntos encontrar la solución{' '}
          </p>
          <p>
            Contesta la pregunta eligiendo una opción ¿Si fueras Pedrito cuál
            crees que sería la mejor opción para resolver su problema?
          </p>
          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="op"
                  value="Mantenerme escondido, aunque eso haría que mi tita se preocupara por mí"
                  onChange={handleChange}
                />
              </td>
              <td>
                Mantenerme escondido, aunque eso haría que mi tita se preocupara
                por mí
              </td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="op"
                  value="Decir una mentira para que nadie sepa que fui yo, aunque eso me haría sentir más culpable"
                  onChange={handleChange}
                />
              </td>
              <td>
                Decir una mentira para que nadie sepa que fui yo, aunque eso me
                haría sentir más culpable
              </td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="op"
                  value="Decirle a mi tita que fue un accidente, pedirle una disculpa y prometerle comprarle otra maceta cuando pueda"
                  onChange={handleChange}
                />
              </td>
              <td>
                Decirle a mi tita que fue un accidente, pedirle una disculpa y
                prometerle comprarle otra maceta cuando pueda
              </td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default ReflexNiRuEjer;
