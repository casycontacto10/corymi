import React, { useState } from 'react';
import {
  Card,
  CardBody,
  CardText,
  CardHeader,
  Button,
  Alert,
} from 'reactstrap';
import audioURL from '../audios/reflexion/REFLEX_NI_ID.mpeg';

import { database } from '../../../helpers/Firebase';

const ReflexNiIdEjer = () => {
  const [audio] = useState(new Audio(audioURL));
  const [isPlaying, setIsPlaying] = useState(false);
  const playPause = () => {
    if (isPlaying) {
      audio.pause();
    } else {
      audio.play();
    }
    setIsPlaying(!isPlaying);
  };
  const [preguntas, setPreguntas] = useState({ p1: '', p2: '', p3: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    resp2: false,
    resp3: false,
  });
  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
  };

  function setRespuesta(event) {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const reflexAdsltRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/reflexion/reflex_ni_id_ejer`
    );
    reflexAdsltRuEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Te gusto la historia?',
            respuesta: preguntas.p1,
          },
          {
            pregunta: '¿Te parece bien lo que reflexionó Oscar al final?',
            respuesta: preguntas.p2,
          },
          {
            pregunta: '¿Aprendiste algo bueno de la historia?',
            respuesta: preguntas.p3,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.resp1 = false;
        estado.resp2 = false;
        estado.resp3 = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.resp1 = preguntas.p1;
          estado.resp2 = preguntas.p2;
          estado.resp3 = preguntas.p3;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="success">
      <CardHeader tag="h1">Ejercicio reflexión</CardHeader>
      <CardBody>
        <CardText>
          <p>Reproduce el siguiente audio y contesta las preguntas:</p>
          <p>
            <Button onClick={playPause}>
              {isPlaying ? 'Detener' : 'Reproducir'}
            </Button>
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>¿Te gusto la historia? </td>
                <td>
                  <textarea
                    name="p1"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%', marginTop: '1em' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>¿Te parece bien lo que reflexionó Oscar al final?</td>
                <td>
                  <textarea
                    name="p2"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%', marginTop: '1em' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>¿Aprendiste algo bueno de la historia?</td>
                <td>
                  <textarea
                    name="p3"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%', marginTop: '1em' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.resp2}</p>
                <p>{estado.resp3}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default ReflexNiIdEjer;
