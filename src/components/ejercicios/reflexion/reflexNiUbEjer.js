import React, { useState } from 'react';
import { Row, Card, CardBody, CardText, CardHeader, Alert, Button } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';
import Emoji from 'a11y-react-emoji';

import audioURL from '../audios/reflexion/REFLEX_NI_UB.m4a';

import { database } from '../../../helpers/Firebase';

const ReflexNiUbEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  const [audio] = useState(new Audio(audioURL));
  const [isPlaying, setIsPlaying] = useState(false);
  const playPause = () => {
    if (isPlaying) {
      audio.pause();
    } else {
      audio.play();
    }
    setIsPlaying(!isPlaying);
  };

  function setRespuesta(respuesta) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const reflexNiUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/reflexion/reflex_ni_ub_ejer`
    );
    reflexNiUbEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Te gustó el cuento? Selecciona una carita',
            respuesta,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = respuesta;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse color="success">
      <CardHeader tag="h1">Ejercicio reflexión</CardHeader>
      <CardBody>
        <CardText>
          <p>Reproduce el siguiente audio y contesta la pregunta:</p>
          <p>
            <Button onClick={playPause}>
              {isPlaying ? 'Detener' : 'Reproducir'}
            </Button>
          </p>
          <p>¿Te gustó el cuento? Selecciona una carita</p>
          <Row style={{ paddingTop: '1em' }}>
            <Colxx xxs="2">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😃"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('me_encanta')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> Me encanta </p>
              </div>
            </Colxx>
            <Colxx xxs="2">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="🙂"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('me_agrada')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> Me agrada </p>
              </div>
            </Colxx>
            <Colxx xxs="3">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😐"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('mas_o_menos')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p>Mas o menos</p>
              </div>
            </Colxx>
            <Colxx xxs="2">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😠"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('no_me_gusto')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> No me gusto </p>
              </div>
            </Colxx>
            <Colxx xxs="3">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😡"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('no_me_gusto_nada')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> No me gusto nada </p>
              </div>
            </Colxx>
          </Row>

          {estado.exito ? (
            <Alert>
              <p>{estado.resp}</p>
              <p>Registrado correctamente</p>
            </Alert>
          ) : null}
          {estado.error ? (
            <Alert>
              <p>Error al guardar</p>
            </Alert>
          ) : null}
        </CardText>
      </CardBody>
    </Card>
  );
};

export default ReflexNiUbEjer;
