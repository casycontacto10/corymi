import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const ReflexAlto = () => {
  return (
    <Card body inverse color="success">
      <CardHeader tag="h1">Reflexión ante los problemas</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: ALTO</CardTitle>
        <CardText>
          <p>
            Por lo general reflexionas cuidadosamente de las cosas antes de
            tomar una decisión, evalúas lo que hiciste bien y mal, tratando de
            entender de qué se trata cada problema al que te enfrentas, analizas
            las posibles soluciones y las que otras personas te brindan. También
            te esfuerzas por aprender de los problemas que vas enfrentando.
          </p>
          <p>
            Felicitaciones tu puntaje en Reflexión ante los problemas indica que
            para ti es fácil pensar en una situación problemática y analizarla
            para buscar una solución. Sigue así.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default ReflexAlto;
