import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const ReflexAdsltUbEjer = () => {
  const [preguntas, setPreguntas] = useState({ p1: '', p2: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    resp2: false,
  });
  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
  };

  function setRespuesta(event) {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const reflexAdsltUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/reflexion/reflex_adslt_ub_ejer`
    );
    reflexAdsltUbEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Qué piensas de la solución de Alicia?',
            respuesta: preguntas.p1,
          },
          {
            pregunta: '¿Tú, qué habrías hecho?',
            respuesta: preguntas.p2,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.resp2 = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = preguntas.p1;
          estado.resp2 = preguntas.p2;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse color="success">
      <CardHeader tag="h1">Ejercicio reflexión</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Ante una situación problemática, es necesario que antes de tomar
            decisiones, nos tomemos un tiempo para reflexionar acerca de las
            posibles soluciones o alternativas. A veces bastan sólo unos
            minutos, pero en otras ocasiones requerimos más tiempo porque se
            trata de decisiones importantes. Por ello, también es necesario
            reconocer que en ocasiones necesitamos orientación de un profesional
            para tomar la mejor decisión.
          </p>
          <p>Ejemplo:</p>
          <p>
            Alicia vive con su mamá y sus tres hermanos en Oaxaca. Su papá vive
            en Estados Unidos desde hace 10 años, y trata de mandarles dinero
            cada que puede. Sin embargo, desde hace tres meses, no han recibido
            dinero de él porque perdió su trabajo, ya que las lluvias y los
            malos tiempos provocaron el despido de mucho personal en los campos
            agrícolas. Desde ese día, la mamá de Alicia sentía muchos malestares
            en la espalda, y Alicia pensó que puede deberse a la preocupación
            por la situación que vivía su padre. Entonces, se le ocurrió que
            ella podría apoyar a la familia colaborando en sus tiempos libres en
            la atención de la frutería que tienen en la casa. Su mamá, en un
            principio le dijo que no, que se dedicara a la escuela, pero Alicia
            decidió apoyar a su mamá para que no se sintiera tan preocupada por
            realizar las labores de la casa y por tener que trabajar. A partir
            de entonces, su mamá ha dejado de sentir los dolores de espalda que
            tanto la molestaban. Juntas han reflexionado que preocuparse o
            quejarse genera problemas físicos y malestares emocionales como
            llanto y desesperación. Entonces, es mejor buscar alternativas para
            solucionar los problemas que se presentan.
          </p>
          <p>Contesta la siguiente pregunta</p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td> ¿Qué piensas de la solución de Alicia? </td>
                <td>
                  <textarea
                    name="p1"
                    onChange={handleChange}
                    rows="5"
                    style={{ height: '100%', width: '100%' }}
                  />
                </td>
              </tr>
              <tr>
                <td> ¿Tú, qué habrías hecho?</td>
                <td>
                  <textarea
                    name="p2"
                    onChange={handleChange}
                    rows="5"
                    style={{ height: '100%', width: '100%' }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.resp2}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default ReflexAdsltUbEjer;
