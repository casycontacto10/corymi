import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const ReflexBajo = () => {
  return (
    <Card body inverse color="success">
      <CardHeader tag="h1">Reflexión ante los problemas</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: BAJO</CardTitle>
        <CardText>
          <p>
            Tu puntaje en Reflexión ante los problemas indica que para ti es
            difícil pensar en una situación problemática y analizarla para
            buscar una solución.{' '}
          </p>
          <p>
            Rara vez te es posible reflexionar cuidadosamente los problemas
            antes de tomar una decisión, es poco lo que puedes reflexionar sobre
            lo que hiciste bien y mal. Además, es raro que intentes entender
            bien de qué se trata el problema al que te enfrento, pues pocas
            veces analizas las posibles soluciones y las que otras personas te
            brindan. Necesitas esforzarte más para poder aprender de los
            problemas que te toca enfrentar en la vida.
          </p>
          <p>
            A continuación, te pedimos que realices un ejercicio que te ayudará
            a mejorar tu capacidad para reflexionar y buscar soluciones a los
            problemas a los que puedas enfrentarte.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default ReflexBajo;
