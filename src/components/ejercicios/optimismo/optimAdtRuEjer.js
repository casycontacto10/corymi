import React, { useState } from 'react';
import { Row, Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';

import YouTube from 'react-youtube';

import { database } from '../../../helpers/Firebase';

const OptimAdtRuEjer = () => {
  const [respuestas, setRespuestas] = useState({ p1: '', p2: '', p3: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta1: '',
    respuesta2: '',
    respuesta3: '',
  });
  const handleChange = (event) => {
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
  };
  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const optimAdtRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/optimismo/optim_adt_ru_ejer`
    );
    optimAdtRuEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Cómo te hizo sentir el video?',
            respuesta: respuestas.p1,
          },
          {
            pregunta: '¿Se te dificultó repetir alguna frase?',
            respuesta: respuestas.p2,
          },
          {
            pregunta: '¿Qué reflexión lograste con el video?',
            respuesta: respuestas.p3,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.respuesta1 = false;
        estado.respuesta2 = false;
        estado.respuesta3 = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.respuesta1 = respuestas.p1;
          estado.respuesta2 = respuestas.p2;
          estado.respuesta3 = respuestas.p3;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse style={{ backgroundColor: 'black' }}>
      <CardHeader tag="h1">Ejercicio optimismo</CardHeader>
      <CardBody>
        <CardText>
          <p>Reproduce el siguiente video y contesta las preguntas:</p>
          <p>
            <Row>
              <Colxx xxs="12" className="mb-5">
                <YouTube videoId="3IN2XtHtEK4" />
              </Colxx>
            </Row>
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>¿Cómo te hizo sentir el video?</td>
                <td>
                  <textarea
                    name="p1"
                    onChange={handleChange}
                    style={{
                      height: '100%',
                      width: '100%',
                    }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>¿Se te dificultó repetir alguna frase?</td>
                <td>
                  <textarea
                    name="p2"
                    onChange={handleChange}
                    style={{
                      height: '100%',
                      width: '100%',
                    }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>¿Qué reflexión lograste con el video?</td>
                <td>
                  <textarea
                    name="p3"
                    onChange={handleChange}
                    style={{
                      height: '100%',
                      width: '100%',
                    }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta1}</p>
                <p>{estado.respuesta2}</p>
                <p>{estado.respuesta3}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default OptimAdtRuEjer;
