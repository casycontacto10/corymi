import React, { useState } from 'react';
import { Row, Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';

import { database } from '../../../helpers/Firebase';

const OptimNiRuEjer = () => {
  const [emociones, setEmociones] = useState({
    Es_muy_difícil: false,
    Me_va_a_salir_mal: false,
    No_puedo_hacer_nada_bien: false,
    Soy_tonto: false,
    Es_mi_culpa: false,
    Nadie_me_quiere: false,
    Solo_me_pasan_cosas_malas: false,
    No_podre_lograr_nada: false,
    Soy_capaz: false,
    Soy_Inteligente: false,
    Puedo_hacerlo_mejor: false,
    Tengo_personas_que_me_aman: false,
    Todo_va_a_mejorar: false,
    Soy_muy_afortunado: false,
    Puedo_aprender_algo_de_esto: false,
    No_me_voy_a_rendir: false,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    emocion: false,
  });
  const handleChange = (event) => {
    event.preventDefault();
    emociones[event.target.name] = event.target.checked;
    setEmociones(emociones);
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const optimNiRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/optimismo/optim_ni_ru_ejer`
    );
    optimNiRuEjer.update(
      {
        respuestas: [
          {
            pregunta: 'Pensamientos malos',
            respuesta: [
              {
                pregunta: 'Es muy difícil',
                respuesta: emociones.Es_muy_difícil,
              },
              {
                pregunta: 'Me va a salir mal',
                respuesta: emociones.Me_va_a_salir_mal,
              },
              {
                pregunta: 'No puedo hacer nada bien',
                respuesta: emociones.No_puedo_hacer_nada_bien,
              },
              { pregunta: 'Soy tonto', respuesta: emociones.Soy_tonto },
              { pregunta: 'Es mi culpa', respuesta: emociones.Es_mi_culpa },
              {
                pregunta: 'Nadie me quiere',
                respuesta: emociones.Nadie_me_quiere,
              },
              {
                pregunta: 'Solo me pasan cosas malas',
                respuesta: emociones.Solo_me_pasan_cosas_malas,
              },
              {
                pregunta: 'No podré lograr nada',
                respuesta: emociones.No_podre_lograr_nada,
              },
            ],
          },
          {
            pregunta: 'Pensamientos buenos',
            respuesta: [
              { pregunta: 'Soy capaz', respuesta: emociones.Soy_capaz },
              {
                pregunta: 'Soy Inteligente',
                respuesta: emociones.Soy_Inteligente,
              },
              {
                pregunta: 'Puedo hacerlo mejor',
                respuesta: emociones.Puedo_hacerlo_mejor,
              },
              {
                pregunta: 'Tengo personas que me aman',
                respuesta: emociones.Tengo_personas_que_me_aman,
              },
              {
                pregunta: 'Todo va a mejorar',
                respuesta: emociones.Todo_va_a_mejorar,
              },
              {
                pregunta: 'Soy muy afortunado',
                respuesta: emociones.Soy_muy_afortunado,
              },
              {
                pregunta: 'Puedo aprender algo de esto',
                respuesta: emociones.Puedo_aprender_algo_de_esto,
              },
              {
                pregunta: 'No me voy a rendi',
                respuesta: emociones.No_me_voy_a_rendir,
              },
            ],
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.emocion = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.emocion = event.target.name;

          setEstado(estado);
        }
      }
    );
  };
  return (
    <Card body inverse style={{ backgroundColor: 'black' }}>
      <CardHeader tag="h1">Ejercicio optimismo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            El optimismo es una herramienta que nos ayuda cuando estamos en una
            situación difícil, nos hace pensar que todo va a estar bien y que
            podemos encontrar una solución, también nos ayuda en nuestra vida
            diaria para no estar preocupados o tristes, a sentirnos más felices
            y ver las cosas difíciles más fáciles.
          </p>
          <p>
            Para que el optimismo funcione debemos tener en nuestra mente
            pensamientos buenos y eliminar los pensamientos malos, por ejemplo:
          </p>
          Marca los pensamientos malos que alguna vez haz tenido, y los
          pensamientos buenos que te gustaría tener a partir de ahora.
          <Row>
            <Colxx xxs="3" className="mt-5">
              <img
                src="/assets/img/ejercicios/optimismo/optim_ni_ru_ejer1.png"
                alt=""
              />
            </Colxx>
            <Colxx xxs="9" className="mt-5">
              <table>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Es_muy_difícil"
                      checked={emociones.Es_muy_difícil}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Es muy difícil</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Me_va_a_salir_mal"
                      checked={emociones.Me_va_a_salir_mal}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Me va a salir mal</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="No_puedo_hacer_nada_bien"
                      checked={emociones.No_puedo_hacer_nada_bien}
                      onChange={handleChange}
                    />
                  </td>
                  <td>No puedo hacer nada bien</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Soy_tonto"
                      checked={emociones.Soy_tonto}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Soy tonto</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Es_mi_culpa"
                      checked={emociones.Es_mi_culpa}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Es mi culpa</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Nadie_me_quiere"
                      checked={emociones.Nadie_me_quiere}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Nadie me quiere</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Solo_me_pasan_cosas_malas"
                      checked={emociones.Solo_me_pasan_cosas_malas}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Solo me pasan cosas malas</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="No_podre_lograr_nada"
                      checked={emociones.No_podre_lograr_nada}
                      onChange={handleChange}
                    />
                  </td>
                  <td>No podré lograr nada</td>
                </tr>
              </table>
            </Colxx>
          </Row>
          <Row>
            <Colxx xxs="3" className="mt-5">
              <img
                src="/assets/img/ejercicios/optimismo/optim_ni_ru_ejer2.png"
                alt=""
              />
            </Colxx>
            <Colxx xxs="9" className="mt-5">
              <table>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Soy_capaz"
                      checked={emociones.Soy_capaz}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Soy capaz</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Soy_Inteligente"
                      checked={emociones.Soy_Inteligente}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Soy Inteligente</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Puedo_hacerlo_mejor"
                      checked={emociones.Puedo_hacerlo_mejor}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Puedo hacerlo mejor</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Tengo_personas_que_me_aman"
                      checked={emociones.Tengo_personas_que_me_aman}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Tengo personas que me aman</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Todo_va_a_mejorar"
                      checked={emociones.Todo_va_a_mejorar}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Todo va a mejorar</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Soy_muy_afortunado"
                      checked={emociones.Soy_muy_afortunado}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Soy muy afortunado</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="Puedo_aprender_algo_de_esto"
                      checked={emociones.Puedo_aprender_algo_de_esto}
                      onChange={handleChange}
                    />
                  </td>
                  <td>Puedo aprender algo de esto</td>
                </tr>
                <tr>
                  <td>
                    <input
                      type="checkbox"
                      name="No_me_voy_a_rendir"
                      checked={emociones.No_me_voy_a_rendir}
                      onChange={handleChange}
                    />
                  </td>
                  <td>No me voy a rendir</td>
                </tr>
              </table>
            </Colxx>
          </Row>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.emocion}</p>
                <p>Actualizado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert>
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default OptimNiRuEjer;
