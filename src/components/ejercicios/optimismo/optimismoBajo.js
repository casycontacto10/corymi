import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const OptimismoMedio = () => {
  return (
    <Card body inverse  style={{backgroundColor:'black'}}>
      <CardHeader tag="h1">Optimismo</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: BAJO</CardTitle>
        <CardText>
          <p>
          Tu puntaje en Optimismo indica que para ti es difícil ver y juzgar las cosas de manera positiva o favorable.          </p>
          <p> 
          Rara vez te das cuenta del lado favorable de lo que te sucede, aún cuando se trate de situaciones difíciles o desagradables.          </p>
          <p>
          Muy pocas veces reconoces el lado positivo de las personas y cuando las cosas no van bien, solo en contadas ocasiones tu mismo te das ánimo. Por lo general cuando recibes una crítica, no la escuchas, la desechas automáticamente, por lo que no te permites aprender de ella.          </p> 
          <p> 
          A continuación, te pedimos que realices un ejercicio que te ayudará a mejorar tu capacidad para encontrar el lado bueno en las cosas.          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default OptimismoMedio;
