import React, { useState } from 'react';
import { Row, Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';
import Emoji from 'a11y-react-emoji';

import { database } from '../../../helpers/Firebase';

const OptimNiUbEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  function setRespuesta(respuesta) {
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const optimNiUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/optimismo/optim_ni_ub_ejer`
    );
    optimNiUbEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Te gustó la actividad? Selecciona una carita',
            respuesta,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = respuesta;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse style={{ backgroundColor: 'black' }}>
      <CardHeader tag="h1">Ejercicio optimismo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            En ocasiones, después de un día muy malo, es bueno reconocer
            nuestras cosas positivas, nos ayuda a ver que no todo es tan malo.
            Te invito a que realices la siguiente actividad.
          </p>
          <p>
            <img
              src="/assets/img/ejercicios/optimismo/optim_ni_ub_ejer.png"
              alt=""
            />
          </p>
          <p>
            Mírate al espejo y di todas aquellas cosas que te gustan sobre ti o
            cosas bonitas que te pasaron durante el día, por ejemplo:
          </p>
          <p>
            Si mi mamá, papá o profesor(a), dicen que me estoy portando mal y
            mencionan que acciones negativas estoy haciendo, no lo debo tomar
            como un regaño, más bien me lo dicen, porque son cosas que puedo
            mejorar.
          </p>
          <p>
            Aquellas personas que nos aman, siempre tendrán cosas bonitas que
            decirnos. El que ellas nos digan cosas agradables, es un apapacho a
            nuestro corazón. Ahora, puedes hacer lo siguiente:
          </p>
          <p>
            Dile a tú mamá, papá o alguien que quieras mucho, que te diga cosas
            bonitas sobre ti.
          </p>
          <p>¿Te gustó la actividad? Selecciona una carita</p>
          <Row style={{ paddingTop: '1em' }}>
            <Colxx xxs="2">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😃"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('me_encanta')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> Me encanta </p>
              </div>
            </Colxx>
            <Colxx xxs="2">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="🙂"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('me_agrada')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> Me agrada </p>
              </div>
            </Colxx>
            <Colxx xxs="3">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😐"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('mas_o_menos')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p>Mas o menos</p>
              </div>
            </Colxx>
            <Colxx xxs="2">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😠"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('no_me_gusto')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> No me gusto </p>
              </div>
            </Colxx>
            <Colxx xxs="3">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😡"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('no_me_gusto_nada')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> No me gusto nada </p>
              </div>
            </Colxx>
          </Row>

          {estado.exito ? (
            <Alert>
              <p>{estado.resp}</p>
              <p>Registrado correctamente</p>
            </Alert>
          ) : null}
          {estado.error ? (
            <Alert>
              <p>Error al guardar</p>
            </Alert>
          ) : null}
        </CardText>
      </CardBody>
    </Card>
  );
};

export default OptimNiUbEjer;
