import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const OptimAdsltUbEjer = () => {
  const [respuestas, setRespuestas] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  const handleChange = (event) => {
    setRespuestas(event.target.value);
  };
  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const optimAdsltUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/optimismo/optim_adslt_ub_ejer`
    );
    optimAdsltUbEjer.update({
      respuestas: [
        {
          pregunta: '¿Has intentado hacer esta actividad antes?',
          respuesta: respuestas,
        },
      ],
      diagnostico: true,
    },
    (error) => {
      estado.error = false;
      estado.exito = false;
      estado.resp = false;
      setEstado(estado);
      if (error) {
        estado.error = true;
        setEstado(estado);
      } else {
        estado.exito = true;
        estado.resp = respuestas;
        setEstado(estado);
      }
    });
  };

  return (
    <Card body inverse style={{ backgroundColor: 'black' }}>
      <CardHeader tag="h1">Ejercicio optimismo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            En ocasiones, solo pensamos en todas aquellas cosas negativas que
            nos pasan, pero, aunque sea un día muy malo, también pasan pequeñas
            cosas que nos hacen feliz o nos sacan una sonrisa. Recuerda que, en
            ocasiones no podemos controlar lo que nos pasa, pero si elegimos con
            que actitud tomar aquellas cosas. Nada en esta vida es para siempre,
            así que, la tristeza y la alegría son solo momentos.
          </p>
          <p>Por lo anterior, te invito a que:</p>
          <p>
            En la noche, antes de irte acostar, mírate en un espejo, y menciona
            3 cosas buenas que pasaron en tu día, puede ser cosas sencillas,
            como:
          </p>
          <ul>
            <li>Hoy me comí mi golosina favorita</li>
            <li>Vi un meme o video que me hizo reír mucho</li>
          </ul>
          <p>
            Ahora bien, di frente al espejo tres cosas positivas sobre ti, como:
          </p>
          <ul>
            <li>Hoy se me veía estupenda esa playera</li>
            <li>Me agrada mi sonrisa</li>
          </ul>
          <p>
            Esta actividad realízala diariamente. Quizá al principio o en un día
            difícil, solo vengan a tu mente cosas negativas, sobre tu día o
            sobre ti, pero cada día esfuérzate por decir al menos una cosa
            positiva.
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td> ¿Has intentado hacer esta actividad antes?</td>
              </tr>
              <tr>
                 <td>
                    <textarea name="respuestas" onChange={handleChange} rows="4" style={{height:"100%", width:"100%"}} />
                  </td>
              </tr>
              <tr>
                <td>
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default OptimAdsltUbEjer;
