import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const OptimismoMedio = () => {
  return (
    <Card body inverse  style={{backgroundColor:'black'}}>
      <CardHeader tag="h1">Optimismo</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: MEDIO</CardTitle>
        <CardText>
          <p>
          Tu puntaje en Optimismo indica que en ocasiones para ti es difícil ver y juzgar las cosas de manera positiva o favorable.
          </p>
          <p> 
          Algunas veces ves el lado favorable de lo que te sucede, aunque se trate de situaciones difíciles, pero en otras tantas ocasiones te cuesta trabajo hacerlo.
          </p>
          <p>
          De igual manera muestras una habilidad medianamente desarrollada para reconocer el lado positivo de las personas y de las críticas que recibes. Por lo que podrías trabajar en todo ello para mejorar aún más.
          </p> 
          <p> 
          A continuación, te pedimos que realices un ejercicio que te ayudará a mejorar tu capacidad para encontrar el lado bueno en las cosas.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default OptimismoMedio;
