import React, { useState } from 'react';
import { Row, Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';

import YouTube from 'react-youtube';

import { database } from '../../../helpers/Firebase';

const OptimAdtUbEjer = () => {
  const [preguntas, setPreguntas] = useState({
    alegria: false,
    tristeza: false,
    enojo: false,
    melancolia: false,
    verguenza: false,
    pregunta: '',
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta: false,
    emocion: false,
    tem: false,
  });
  const handleChange = (event) => {
    preguntas[event.target.name] =
      event.target.name === 'pregunta'
        ? event.target.value
        : event.target.checked;
    setPreguntas(preguntas);
    if (event.target.name !== 'pregunta') {
      estado.tem = event.target.name;
      setEstado(estado);
    }
  };
  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const optimAdtUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/optimismo/optim_adt_ub_ejer`
    );
    optimAdtUbEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Qué emociones te hizo sentir el video?',
            respuesta: [
              { pregunta: 'alegria', respuesta: preguntas.alegria },
              { pregunta: 'tristeza', respuesta: preguntas.tristeza },
              { pregunta: 'enojo', respuesta: preguntas.enojo },
              { pregunta: 'melancolia', respuesta: preguntas.melancolia },
              { pregunta: 'verguenza', respuesta: preguntas.verguenza },
            ],
          },
          {
            pregunta: '¿Por qué crees que sentiste esas emociones?',
            respuesta: preguntas.pregunta,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.respuesta = false;
        estado.emocion = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.respuesta = preguntas.pregunta;
          estado.exito = true;
          estado.emocion = estado.tem;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse style={{ backgroundColor: 'black' }}>
      <CardHeader tag="h1">Ejercicio optimismo</CardHeader>
      <CardBody>
        <CardText>
          <p>Reproduce el siguiente video y contesta las preguntas:</p>
          <p>
            <Row>
              <Colxx xxs="12" className="mb-5">
                <YouTube videoId="wT8K2w_WvVs" />
              </Colxx>
            </Row>
          </p>
          <p>
            ¿Qué emociones te hizo sentir el video? Puedes elegir más de una.
          </p>
          <table>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="alegria"
                  checked={preguntas.alegria}
                  onChange={handleChange}
                />
              </td>
              <td>Alegría</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="tristeza"
                  checked={preguntas.tristeza}
                  onChange={handleChange}
                />
              </td>
              <td>Tristeza</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="enojo"
                  checked={preguntas.enojo}
                  onChange={handleChange}
                />
              </td>
              <td>Enojo</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="melancolia"
                  checked={preguntas.melancolia}
                  onChange={handleChange}
                />
              </td>
              <td>Melancolía</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="verguenza"
                  checked={preguntas.verguenza}
                  onChange={handleChange}
                />
              </td>
              <td>Vergüenza</td>
            </tr>
          </table>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>¿Por qué crees que sentiste esas emociones?</td>
              </tr>
              <tr>
                <td>
                  <textarea
                    name="pregunta"
                    onChange={handleChange}
                    style={{
                      height: '100%',
                      width: '100%',
                      marginTop: '0.5em',
                    }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta}</p>
                <p>{estado.emocion}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default OptimAdtUbEjer;
