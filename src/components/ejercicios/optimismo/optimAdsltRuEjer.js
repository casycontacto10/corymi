import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const OptimAdsltRuEjer = () => {
  const [pregunta, setPregunta] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  const handleChange = (event) => {
    setPregunta(event.target.value);
  };

  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const optimAdtUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/optimismo/optim_adslt_ru_ejer`
    );
    optimAdtUbEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Además de lo anterior qué otra cosa haces tú para ser más optimista o que más te gustaría comenzar a hacer?',
            respuesta: pregunta,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = pregunta;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse style={{ backgroundColor: 'black' }}>
      <CardHeader tag="h1">Ejercicio optimismo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            El optimismo es una herramienta muy buena que nos sirve cuando
            tenemos un problema, estamos en una situación difícil o nos sentimos
            tristes, nos ayuda a pensar que todo va a estar bien, a ver el lado
            positivo de las cosas, estar más tranquilos mientras todo se
            resuelve y nos ayuda a sentirnos más felices. También podemos usarlo
            en nuestra vida diaria, eso nos puede ayudar a estar menos
            estresados o preocupados.
          </p>
          <p>
            A continuación, te dejo una lista de cosas que puedes comenzar a
            hacer para ser más optimista:
          </p>
          <ol>
            <li>
              Al final de tu día tomate un momento para pensar en todo lo bueno
              que te sucedió y en las cosas por las que estás agradecido
            </li>
            <li>
              Cambia los pensamientos negativos por positivos, por ejemplo:
              <br />
              <br />
              <p>“No puedo” por: “yo soy capaz”</p>
              <p>“Es muy difícil” por: “si me esfuerzo más lo puedo lograr”</p>
              <p>
                “Todo me sale mal” por: “todos nos equivocamos, la próxima vez
                me esforzare más”
              </p>
              <p>
                “¿Qué hice para merecer esto?” por “¿Qué debo aprender de esta
                situación?”
              </p>
            </li>
            <li>
              Cuando estés pasando por un momento difícil recuerda que todo es
              temporal y que ese momento va a terminar tarde o temprano.
            </li>
            <li>
              Intenta encontrarle las cosas positivas a cualquier situación.
            </li>
          </ol>
          <p>Contesta la siguiente pregunta:</p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  ¿Además de lo anterior qué otra cosa haces tú para ser más
                  optimista o que más te gustaría comenzar a hacer?
                </td>
              </tr>
              <tr>
                <td>
                  <textarea
                    name="pregunta"
                    onChange={handleChange}
                    rows="5"
                    style={{
                      height: '100%',
                      width: '100%',
                      marginTop: '0.5em',
                    }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default OptimAdsltRuEjer;
