import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const OptimismoAlto = () => {
  return (
    <Card body inverse style={{ backgroundColor: 'black' }}>
      <CardHeader tag="h1">Optimismo</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: ALTO</CardTitle>
        <CardText>
          <p>
            Felicitaciones, tu puntaje en Optimismo indica que tienes una gran
            facilidad para ver las cosas de manera positiva o favorable. Que tu
            pensamiento es flexible y que puedes encontrar alguna ganancia aún
            en medio de la adversidad. Sigue así.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default OptimismoAlto;
