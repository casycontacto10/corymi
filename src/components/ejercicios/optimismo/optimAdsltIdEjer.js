import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const OptimAdtRuEjer = () => {
  const [preguntas, setPreguntas] = useState({
    p1: '',
    otro: '',
    disable: true,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    otro: false,
  });
  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
    if (event.target.name === 'p1' && event.target.value !== 'otro') {
      preguntas.otro = '';
      preguntas.disable = true;
    }
    if (event.target.value === 'otro') {
      preguntas.disable = false;
    }
  };

  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const autoctrlAdtIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/optimismo/optim_adslt_id_ejer`
    );
    autoctrlAdtIdEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿tú qué piensas cuando vives una situación triste o desagradable?',
            respuesta: preguntas.p1,
            otro_texto: preguntas.otro,
            otro: !preguntas.disable,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.otro = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = preguntas.p1;
          estado.otro = preguntas.otro;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse style={{ backgroundColor: 'black' }}>
      <CardHeader tag="h1">Ejercicio optimismo</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta la pregunta de reflexión:</p>
          <p>
            El próximo domingo los amigos y amigas de Gaby irán de paseo al lago
            que está cerca de su pueblo. Llevarán comida, bebidas y música, y
            estarán ahí hasta que anochezca. Los papás de Gaby no le dieron
            permiso de ir con ellos porque no habrá ningún adulto presente. Ella
            se sintió muy triste pero comprendió que sus papás lo hacían por su
            seguridad.
          </p>
          <p>
            Aunque estaba triste, Gaby pudo ver el lado positivo de la
            situación, ¿tú qué piensas cuando vives una situación triste o
            desagradable?
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Que las cosas pasan por algo"
                    onChange={handleChange}
                  />
                </td>
                <td>Que las cosas pasan por algo</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Que siempre me van a pasar cosas tristes"
                    onChange={handleChange}
                  />
                </td>
                <td>Que siempre me van a pasar cosas tristes</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Que también he tenido momentos agradables y de felicidad"
                    onChange={handleChange}
                  />
                </td>
                <td>
                  Que también he tenido momentos agradables y de felicidad
                </td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="otro"
                    onChange={handleChange}
                  />
                </td>
                <td>Otro</td>
                <td>
                  <textarea
                    disabled={preguntas.disable}
                    type="text"
                    name="otro"
                    value={preguntas.otro}
                    onChange={handleChange}
                    rows="5"
                    style={{ height: '100%', width: '100%' }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.otro}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default OptimAdtRuEjer;
