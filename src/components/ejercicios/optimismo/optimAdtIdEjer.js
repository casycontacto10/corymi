import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const OptimAdtIdEjer = () => {
  const [preguntas, setPreguntas] = useState({
    p1: '',
    otro: '',
    disable: true,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    otro: false,
  });
  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
    if (event.target.name === 'p1' && event.target.value !== 'otro') {
      preguntas.otro = '';
      preguntas.disable = true;
    }
    if (event.target.value === 'otro') {
      preguntas.disable = false;
    }
  };

  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const autoctrlAdtIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/optimismo/optim_adt_id_ejer`
    );
    autoctrlAdtIdEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿tú en qué piensas cuando necesitas darte ánimos en una situación triste o desagradable?',
            respuesta: preguntas.p1,
            otro_texto: preguntas.otro,
            otro: !preguntas.disable,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.otro = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = preguntas.p1;
          estado.otro = preguntas.otro;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse style={{ backgroundColor: 'black' }}>
      <CardHeader tag="h1">Ejercicio optimismo</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta la pregunta de reflexión:</p>
          <p>
            Lalo trabaja toda la semana en un rancho cercano a su comunidad y
            solo puede ver a su familia los fines de semana. Él los extraña
            mucho y algunos días se siente muy triste de no estar con ellos,
            pero sabe que todo su esfuerzo vale la pena para que ellos tengan
            todo lo que necesitan.
          </p>
          <p>
            A pesar de que extraña a su familia, Lalo ve el lado bueno de la
            situación, ¿tú en qué piensas cuando necesitas darte ánimos en una
            situación triste o desagradable?
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Que es algo que no durará para siempre"
                    onChange={handleChange}
                  />
                </td>
                <td>Que es algo que no durará para siempre</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Que hay personas que me apoyan"
                    onChange={handleChange}
                  />
                </td>
                <td>Que hay personas que me apoyan</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Que tengo que salir adelante por mi cuenta"
                    onChange={handleChange}
                  />
                </td>
                <td>Que tengo que salir adelante por mi cuenta</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="otro"
                    onChange={handleChange}
                  />
                </td>
                <td>Otro</td>
                <td>
                  <textarea
                    disabled={preguntas.disable}
                    type="text"
                    name="otro"
                    value={preguntas.otro}
                    onChange={handleChange}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.otro}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default OptimAdtIdEjer;
