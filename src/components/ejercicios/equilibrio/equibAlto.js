import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const EquibAlto = () => {
  return (
    <Card body inverse color="info">
      <CardHeader tag="h1">Recuperación del equilibrio</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: ALTO</CardTitle>
        <CardText>
          <p>
            Felicitaciones tu puntaje en Recuperación del equilibrio indica que
            es fácil para ti regresar a un estado de tranquilidad después de
            haber perdido el control por emociones de tristeza, enojo o
            exaltación. Continúa así. Este es un importante recurso personal
            para toda tu vida.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EquibAlto;
