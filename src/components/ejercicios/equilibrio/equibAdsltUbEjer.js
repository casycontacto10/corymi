import React, { useState } from 'react';
import {  Row, Card, CardBody, CardText, CardHeader, Button, Alert } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';
import audioURL from '../audios/equilibrio/EQUIB_ADSLT_UB.m4a';

import {database} from '../../../helpers/Firebase';

const EquibAdsltUbEjer = () => {
  const [audio] = useState(new Audio(audioURL));
  const [isPlaying, setIsPlaying] = useState(false);
  const [respuestas, setRespuestas] = useState({"aprendi":"","gusto":""});
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    resp2: false,
  });
  const playPause = () => {
    if (isPlaying) {
      audio.pause();
    } else {
      audio.play();
    }
    setIsPlaying(!isPlaying);
  };
  const handleChange = (event) => {
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
  }

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');
  
    const refe = database.ref();
    const equibAdsltUbEjer =  refe.child(`respuestasPersona/${id}/ejercicios/equilibrio/equib_adslt_ub_ejer`)
    equibAdsltUbEjer.update({ 
      respuestas:[ 
        {'pregunta': "¿Qué aprendiste con la historia?", "respuesta": respuestas.aprendi},
        {'pregunta': "¿Te gustó la historia?", "respuesta": respuestas.gusto}
      ],
      diagnostico: true
    },
    (error) => {
      estado.error = false;
      estado.exito = false;
      estado.resp1 = false;
      estado.resp2 = false;

      setEstado(estado);

      if (error) {
        estado.error = true;
        setEstado(estado);
      } else {
        estado.exito = true;
        estado.resp1 = respuestas.aprendi;
        estado.resp2 = respuestas.gusto;
        setEstado(estado);
      }
    });
  
  };
  return (
    <Card body inverse color="info">
      <CardHeader tag="h1">Ejercicio recuperación del equilibrio</CardHeader>
      <CardBody>
        <CardText>
          <p>Reproduce el siguiente audio y contesta las preguntas.</p>
          <p> <Button onClick={playPause}>
              {isPlaying ? 'Detener' : 'Reproducir'}
            </Button>
          </p>

          <form onSubmit={setRespuesta}>
            <Row>
              <Colxx xxs="4">
                <p>¿Qué aprendiste con la historia?</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea 
                  name="aprendi" 
                  onChange={handleChange}
                  rows="4"
                />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="4">
                <p>¿Te gustó la historia?</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea 
                  name="gusto" 
                  onChange={handleChange}
                  rows="4"
                />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="4">
              <input type="submit" />
              </Colxx>
            </Row>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.resp2}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EquibAdsltUbEjer;
