import React, { useState } from 'react';
import { Row, Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';

import { database } from '../../../helpers/Firebase';

const EquibAdtRuEjer = () => {
  const [respuestas, setRespuestas] = useState({ resp: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta: '',
  });
  const handleChange = (event) => {
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
  };
  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const equibAdtRuEje = refe.child(
      `respuestasPersona/${id}/ejercicios/equilibrio/equib_adt_ru_ejer`
    );
    equibAdtRuEje.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Cómo te sentiste antes y después de realizar el ejercicio?',
            respuesta: respuestas.resp,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.respuesta = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.respuesta = respuestas.resp;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  };
  return (
    <Card body inverse color="info">
      <CardHeader tag="h1">Ejercicio recuperación del equilibrio</CardHeader>
      <CardBody>
        <CardText>
          <p>
            ¿Alguna vez te ha ocurrido que pierdes el control ante una situación
            que te hace sentir muy enojado, triste o frustrado y en ocasiones
            eso te hace actuar de una forma que no es buena para ti o para las
            personas que están contigo? por ejemplo algunas personas suelen
            alzar la voz, golpear objetos o insultar a alguien más.
          </p>
          <p>
            Es importante darnos cuenta cuando sentimos que estamos perdiendo el
            control y pensar en una manera de mantener la calma y enfrentar el
            problema.
          </p>

          <p>
            Escribe cuatro cosas que estés sintiendo o tocando como por ejemplo
            la suavidad de tu suéter.
          </p>

          <p>
            A continuación, te presento un ejercicio que puedes llevar a cabo en
            alguna situación en la que sientas necesidad de relajarte
          </p>
          <p>
            Realiza el ejercicio a modo de experimento y contesta la pregunta al
            final:
          </p>
          <ol>
            <li>
              Ve a un lugar en el que puedas estar tranquilo y escoge una
              posición en la que estés cómodo
            </li>
            <li>
              {' '}
              Cierra tus ojos y comienza a respirar profundamente jalando aire
              por la nariz y sacándolo por tu boca
            </li>
            <li>
              Imagina que tu estómago es un globo que se infla al respirar y se
              desinfla al sacar el aire
            </li>
            <li>
              Empieza a tensar tus pies por unos segundos haciendo mucha fuerza
              y luego relájalos
            </li>
            <li>
              Haz lo mismo con tus piernas, enseguida el estómago, continúa con
              las manos, después los brazos y por último los hombros
            </li>
            <li>Recuerda seguir respirando profundamente</li>
            <li>
              Para terminar, relaja todo tu cuerpo, respira un par de veces más
              y abre tus ojos lentamente
            </li>
          </ol>

          <p>¿Cómo te sentiste antes y después de realizar el ejercicio?</p>
          <form onSubmit={setRespuesta}>
            <Row>
              <Colxx xxs="12">
                <textarea
                  name="resp"
                  onChange={handleChange}
                  style={{
                    height: '100%',
                    width: '100%',
                    marginTop: '0.5em',
                  }}
                  rows="5"
                />
              </Colxx>
            </Row>
            <br />
            <Row>
              <Colxx xxs="4">
                <input type="submit" />
              </Colxx>
            </Row>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EquibAdtRuEjer;
