import React, { useState } from 'react';
import { Row, Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';

import { database } from '../../../helpers/Firebase';

const EquibAdsltRuEjer = () => {
  const [respuestas, setRespuestas] = useState({
    objeto1: '',
    objeto2: '',
    objeto3: '',
    objeto4: '',
    objeto5: '',
    sentir1: '',
    sentir2: '',
    sentir3: '',
    sentir4: '',
    sonido1: '',
    sonido2: '',
    sonido3: '',
    oler1: '',
    oler2: '',
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    objeto1: false,
    objeto2: false,
    objeto3: false,
    objeto4: false,
    objeto5: false,
    sentir1: false,
    sentir2: false,
    sentir3: false,
    sentir4: false,
    sonido1: false,
    sonido2: false,
    sonido3: false,
    oler1: false,
    oler2: false,
  });
  const handleChange = (event) => {
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const equibAdsltRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/equilibrio/equib_adslt_ru_ejer`
    );
    equibAdsltRuEjer.update(
      {
        respuestas: [
          {
            pregunta: 'Observa a tu alrededor y escribe cinco objetos que veas',
            respuesta: {
              1: respuestas.objeto1,
              2: respuestas.objeto2,
              3: respuestas.objeto3,
              4: respuestas.objeto4,
              5: respuestas.objeto5,
            },
          },
          {
            pregunta:
              'Escribe cuatro cosas que estés sintiendo o tocando como por ejemplo la suavidad de tu suéter.',
            respuesta: {
              1: respuestas.sentir1,
              2: respuestas.sentir2,
              3: respuestas.sentir3,
              4: respuestas.sentir4,
            },
          },
          {
            pregunta:
              'Escribe tres sonidos que estés escuchando (presta mucha atención)',
            respuesta: {
              1: respuestas.sonido1,
              2: respuestas.sonido2,
              3: respuestas.sonido3,
            },
          },
          {
            pregunta: 'Escribe dos cosas que puedas oler.',
            respuesta: {
              1: respuestas.oler1,
              2: respuestas.oler2,
            },
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.objeto1 = false;
        estado.objeto2 = false;
        estado.objeto3 = false;
        estado.objeto4 = false;
        estado.objeto5 = false;
        estado.sentir1 = false;
        estado.sentir2 = false;
        estado.sentir3 = false;
        estado.sentir4 = false;
        estado.sonido1 = false;
        estado.sonido2 = false;
        estado.sonido3 = false;
        estado.oler1 = false;
        estado.oler2 = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.objeto1 = respuestas.objeto1;
          estado.objeto2 = respuestas.objeto2;
          estado.objeto3 = respuestas.objeto3;
          estado.objeto4 = respuestas.objeto4;
          estado.objeto5 = respuestas.objeto5;
          estado.sentir1 = respuestas.sentir1;
          estado.sentir2 = respuestas.sentir2;
          estado.sentir3 = respuestas.sentir3;
          estado.sentir4 = respuestas.sentir4;
          estado.sonido1 = respuestas.sonido1;
          estado.sonido2 = respuestas.sonido2;
          estado.sonido3 = respuestas.sonido1;
          estado.oler1 = respuestas.oler1;
          estado.oler2 = respuestas.oler2;
          setEstado(estado);
        }
      }
    );
  };

  return (
    <Card body inverse color="info">
      <CardHeader tag="h1">Ejercicio recuperación del equilibrio</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Este ejercicio te puede ayudar mucho cuando te encuentres en algún
            momento en el que te sientas inquieto, triste, inseguro o algún
            pensamiento que te quite de sentirte bien.
          </p>
          <p>Observa a tu alrededor y escribe cinco objetos que veas…</p>
          <form onSubmit={setRespuesta}>
            <Row>
              <Colxx xxs="1">
                <p>1.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="objeto1" onChange={handleChange} />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="1">
                <p>2.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="objeto2" onChange={handleChange} />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="1">
                <p>3.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="objeto3" onChange={handleChange} />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="1">
                <p>4.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="objeto4" onChange={handleChange} />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="1">
                <p>5.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="objeto5" onChange={handleChange} />
              </Colxx>
            </Row>
            <p>
              Escribe cuatro cosas que estés sintiendo o tocando como por
              ejemplo la suavidad de tu suéter.
            </p>
            <Row>
              <Colxx xxs="1">
                <p>1.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="sentir1" onChange={handleChange} />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="1">
                <p>2.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="sentir2" onChange={handleChange} />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="1">
                <p>3.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="sentir3" onChange={handleChange} />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="1">
                <p>4.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="sentir4" onChange={handleChange} />
              </Colxx>
            </Row>
            <p>
              Escribe tres sonidos que estés escuchando (presta mucha atención).
            </p>
            <Row>
              <Colxx xxs="1">
                <p>1.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="sonido1" onChange={handleChange} />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="1">
                <p>2.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="sonido2" onChange={handleChange} />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="1">
                <p>3.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="sonido3" onChange={handleChange} />
              </Colxx>
            </Row>
            <p>Escribe dos cosas que puedas oler.</p>
            <Row>
              <Colxx xxs="1">
                <p>1.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="oler1" onChange={handleChange} />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="1">
                <p>2.-</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea name="oler2" onChange={handleChange} />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="1">
                <input type="submit" />
              </Colxx>
            </Row>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.objeto1}</p>
                <p>{estado.objeto2}</p>
                <p>{estado.objeto4}</p>
                <p>{estado.objeto5}</p>
                <p>{estado.sentir1}</p>
                <p>{estado.sentir2}</p>
                <p>{estado.sentir3}</p>
                <p>{estado.sentir4}</p>
                <p>{estado.sonido1}</p>
                <p>{estado.sonido2}</p>
                <p>{estado.sonido3}</p>
                <p>{estado.oler1}</p>
                <p>{estado.oler2}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EquibAdsltRuEjer;
