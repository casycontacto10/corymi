import React, { useState } from 'react';
import {
  Row,
  Card,
  CardBody,
  CardText,
  CardHeader,
  Button,
  Alert,
} from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';
import audioURL from '../audios/equilibrio/EQUIB_ADULTO_UB.mpeg';

import { database } from '../../../helpers/Firebase';

const EquibAdtUbEjer = () => {
  const [audio] = useState(new Audio(audioURL));
  const [isPlaying, setIsPlaying] = useState(false);
  const [respuestas, setRespuestas] = useState({ servido: '', manera: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta1: '',
    respuesta2: '',
  });
  const playPause = () => {
    if (isPlaying) {
      audio.pause();
    } else {
      audio.play();
    }
    setIsPlaying(!isPlaying);
  };
  const handleChange = (event) => {
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const equibAdtUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/equilibrio/equib_adt_ub_ejer`
    );
    equibAdtUbEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Qué aprendiste con la historia?',
            respuesta: respuestas.servido,
          },
          { pregunta: '¿Te gustó la historia?', respuesta: respuestas.manera },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.respuesta1 = false;
        estado.respuesta2 = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.respuesta1 = respuestas.servido;
          estado.respuesta2 = respuestas.manera;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  };
  return (
    <Card body inverse color="info">
      <CardHeader tag="h1">Ejercicio recuperación del equilibrio</CardHeader>
      <CardBody>
        <CardText>
          <p>Reproduce el siguiente audio y contesta las preguntas.</p>
          <p>
            <Button onClick={playPause}>
              {isPlaying ? 'Detener' : 'Reproducir'}
            </Button>
          </p>

          <form onSubmit={setRespuesta}>
            <Row>
              <Colxx xxs="4">
                <p>¿Te ha servido esté ejercicio?</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea
                  name="servido"
                  onChange={handleChange}
                  style={{
                    height: '100%',
                    width: '50%',
                    marginTop: '0.5em',
                  }}
                  rows="5"
                />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="4">
                <p>¿De qué manera?</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea
                  name="manera"
                  onChange={handleChange}
                  style={{
                    height: '100%',
                    width: '50%',
                    marginTop: '0.5em',
                  }}
                  rows="5"
                />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="4">
                <input type="submit" />
              </Colxx>
            </Row>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta1}</p>
                <p>{estado.respuesta2}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EquibAdtUbEjer;
