import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const EquibBajo = () => {
  return (
    <Card body inverse color="info">
      <CardHeader tag="h1">Recuperación del equilibrio</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: BAJO</CardTitle>
        <CardText>
          <p>
            Cuando estás triste no acostumbras a tratarte a ti mismo con
            amabilidad o a darte mensajes de ánimo, por ejemplo, al recordarte
            que tienes la capacidad de salir adelante, de que necesitas
            continuar esforzándote, de que sí puedes hacer las cosas. También te
            es difícil darte cuenta de cómo te sientes y de lo que necesitas
            para volver a recuperar tu equilibrio.
          </p>
          <p>
            Tu puntaje en Recuperación del equilibrio indica que para ti es
            difícil regresar a un estado de tranquilidad después de haber
            perdido el control por emociones de tristeza, enojo o exaltación.
          </p>
          <p>
            A continuación te pedimos que realices un ejercicio que te ayudará a
            saber cómo recuperar la tranquilidad en diferentes situaciones
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EquibBajo;
