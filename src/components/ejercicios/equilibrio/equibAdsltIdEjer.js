import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const EquibAdsltIdEjer = () => {
  const [respuestas, setRespuestas] = useState({
    pregunta1: '',
    pregunta2: '',
    otro: '',
    disable: true,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    resp2: false,
    otro: false,
  });

  const handleChange = (event) => {
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
    if (event.target.name === 'pregunta2' && event.target.value !== 'otro') {
      respuestas.otro = '';
      respuestas.disable = true;
    }
    if (event.target.value === 'otro') {
      respuestas.disable = false;
    }
  };

  function setRespuesta(event) {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const equibAdsltIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/equilibrio/equib_adslt_id_ejer`
    );
    equibAdsltIdEjer.update({
      respuestas: [
        {
          pregunta:
            '¿Cuál de los consejos crees que ayude más a Marcela a resolver la situación?',
          respuesta: respuestas.pregunta1,
        },
        {
          pregunta: '¿Tú qué haces cuando te pasa algo que te hace enojar?',
          respuesta: respuestas.pregunta2,
          otro_texto: respuestas.otro,
          otro: !respuestas.disable,
        },
      ],
      diagnostico: true,
    },
    
    (error) => {
      estado.error = false;
      estado.exito = false;
      estado.resp1 = false;
      estado.resp2 = false;
      estado.otro = false;
      setEstado(estado);

      if (error) {
        estado.error = true;
        setEstado(estado);
      } else {
        estado.exito = true;
        estado.resp1 = respuestas.pregunta1;
        estado.otro = respuestas.otro;
        estado.resp2 = respuestas.pregunta2;
        setEstado(estado);
      }
    });
  }

  return (
    <Card body inverse color="info">
      <CardHeader tag="h1">Ejercicio recuperación del equilibrio</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta las preguntas de reflexión:</p>
          <p>
            Marcela se enteró que su amiga Paola habló mal de ella y sintió
            mucho coraje. Como estaba muy enojada pensó en ir a reclamarle, pero
            primero platicó con sus amigas para que le dieran un consejo. Su
            amiga Nancy le dijo que, si estuviera en su lugar, ella sí iría a
            buscarla y le daría una cachetada. Fernanda comentó que Marcela
            debería hacer lo mismo y contar cosas malas de Paola. Y María le
            aconsejó que primero debía tranquilizarse y después buscar a Paola
            para pedirle una explicación.
          </p>
          <p>
            ¿Cuál de los consejos crees que ayude más a Marcela a resolver la
            situación?
          </p>
          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="pregunta1"
                  value="El consejo de Nancy"
                  onChange={handleChange}
                />
              </td>
              <td>El consejo de Nancy</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="pregunta1"
                  value="El consejo de Fernanda"
                  onChange={handleChange}
                />
              </td>
              <td>El consejo de Fernanda</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="pregunta1"
                  value="El consejo de María"
                  onChange={handleChange}
                />
              </td>
              <td>El consejo de María</td>
            </tr>
          </table>
          <p>¿Tú qué haces cuando te pasa algo que te hace enojar?</p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="pregunta2"
                    value="Lloro de coraje"
                    onChange={handleChange}
                  />
                </td>
                <td>Lloro de coraje</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="pregunta2"
                    value="Respiro profundamente hasta que me tranquilizo"
                    onChange={handleChange}
                  />
                </td>
                <td>Respiro profundamente hasta que me tranquilizo</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="pregunta2"
                    value="Me distraigo con otra cosa"
                    onChange={handleChange}
                  />
                </td>
                <td>Me distraigo con otra cosa</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="pregunta2"
                    value="otro"
                    onChange={handleChange}
                  />
                </td>
                <td>Otro</td>
                <td>
                  <textarea
                    disabled={respuestas.disable}
                    type="text"
                    name="otro"
                    value={respuestas.otro}
                    onChange={handleChange}
                    rows="5"
                    style={{ height: '100%', width: '100%' }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.resp2}</p>
                <p>{estado.otro}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EquibAdsltIdEjer;
