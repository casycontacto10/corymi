import React, { useState } from 'react';
import { Row, Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';

import YouTube from 'react-youtube';

import { database } from '../../../helpers/Firebase';

const EquibNiIdEjer = () => {
  const [respuestas, setRespuestas] = useState({ antes: '', despues: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    antes: false,
    despues: false,
  });
  function handleChange(event) {
    const id = localStorage.getItem('recursoId');
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
    const refe = database.ref();
    const equibNiUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/equilibrio/equib_ni_id_ejer`
    );
    equibNiUbEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Qué tan estresado/a me sentía antes del ejercicio?',
            respuesta: respuestas.antes,
          },
          {
            pregunta: '¿Qué tan estresado/a me sentía después del ejercicio?',
            respuesta: respuestas.despues,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.antes = false;
        estado.despues = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.antes = respuestas.antes;
          estado.despues = respuestas.despues;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse color="info">
      <CardHeader tag="h1">Ejercicio recuperación del equilibrio</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Reproduce el siguiente video y selecciona una opción en cada
            pregunta.
          </p>
          <p>
            <Row>
              <Colxx xxs="12" className="mb-5">
                <YouTube videoId="XyYlZ4XentA" />
              </Colxx>
            </Row>
          </p>
          <p>¿Qué tan estresado/a me sentía antes del ejercicio?</p>
          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="antes"
                  value="Demasiado estresado/a"
                  onChange={handleChange}
                />
              </td>
              <td>Demasiado estresado/a</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="antes"
                  value="Muy estresado"
                  onChange={handleChange}
                />
              </td>
              <td>Muy estresado</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="antes"
                  value="Un poco estresado"
                  onChange={handleChange}
                />
              </td>
              <td>Un poco estresado</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="antes"
                  value="Relajado"
                  onChange={handleChange}
                />
              </td>
              <td>Relajado</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="antes"
                  value="Completamente relajado"
                  onChange={handleChange}
                />
              </td>
              <td>Completamente relajado</td>
            </tr>
          </table>
          <br />
          <p>¿Qué tan estresado/a me sentía después del ejercicio?</p>
          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="despues"
                  value="Demasiado estresado/a"
                  onChange={handleChange}
                />
              </td>
              <td>Demasiado estresado/a</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="despues"
                  value="Muy estresado"
                  onChange={handleChange}
                />
              </td>
              <td>Muy estresado</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="despues"
                  value="Un poco estresado"
                  onChange={handleChange}
                />
              </td>
              <td>Un poco estresado</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="despues"
                  value="Relajado"
                  onChange={handleChange}
                />
              </td>
              <td>Relajado</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="despues"
                  value="Completamente relajado"
                  onChange={handleChange}
                />
              </td>
              <td>Completamente relajado</td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.antes}</p>
                <p>{estado.despues}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EquibNiIdEjer;
