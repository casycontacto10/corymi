import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const EquibAdtIdEjer = () => {
  const [preguntas, setPreguntas] = useState({
    p1: '',
    otro: '',
    disable: true,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    otro: false,
  });
  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
    if (event.target.name === 'p1' && event.target.value !== 'otro') {
      preguntas.otro = '';
      preguntas.disable = true;
    }
    if (event.target.value === 'otro') {
      preguntas.disable = false;
    }
  };

  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const equibAdtIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/equilibrio/equib_adt_id_ejer`
    );
    equibAdtIdEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿A ti qué te ayuda a recuperarte cuando te sientes muy triste?',
            respuesta: preguntas.p1,
            otro_texto: preguntas.otro,
            otro: !preguntas.disable,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.otro = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = preguntas.p1;
          estado.otro = preguntas.otro;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse color="info">
      <CardHeader tag="h1">Ejercicio recuperación del equilibrio</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta la pregunta de reflexión:</p>
          <p>
            Todas las mañanas Martín ordeña a sus dos vacas y va al mercado a
            vender la leche para poder comprar alimentos para su familia. Hoy,
            cuando regresó del mercado, se dio cuenta de que alguien se había
            robado sus vacas y se sintió muy triste y desesperado porque sin
            ellas ya no tendría dinero. Lloró durante muchas horas porque pensó
            que su familia iba a estar muy decepcionada de él por no haber
            cuidado a las vacas pero después recordó que cuando han tenido otros
            problemas, lo han apoyado y han salido adelante.
          </p>
          <p>
            Pensar en el apoyo de su familia, ayudó a Martín a recuperar la
            tranquilidad.
          </p>
          <p>¿A ti qué te ayuda a recuperarte cuando te sientes muy triste?</p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Hablar con alguien sobre lo que siento"
                    onChange={handleChange}
                  />
                </td>
                <td>Hablar con alguien sobre lo que siento</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Recibir el apoyo de mi familia o amigos"
                    onChange={handleChange}
                  />
                </td>
                <td>Recibir el apoyo de mi familia o amigos</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Pensar en posibles soluciones"
                    onChange={handleChange}
                  />
                </td>
                <td>Pensar en posibles soluciones</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="otro"
                    onChange={handleChange}
                  />
                </td>
                <td>Otro </td>
                <td>
                  <textarea
                    disabled={preguntas.disable}
                    name="otro"
                    value={preguntas.otro}
                    onChange={handleChange}
                    rows="5"
                    style={{ height: '100%', width: '100%' }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.otro}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EquibAdtIdEjer;
