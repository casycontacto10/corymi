import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const EquibMedio = () => {
  return (
    <Card body inverse color="info">
      <CardHeader tag="h1">Recuperación del equilibrio</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: MEDIO</CardTitle>
        <CardText>
          <p>
            En ocasiones cuando estas triste tienes la capacidad de levantar tu
            estado de ánimo diciéndote cosas lindas o amables a ti mismo, o
            puedes hacer un segundo o tercer intento de algo que no salió como
            tu esperabas. El darte cuenta de cómo te sientes, algunas veces te
            ayuda a recuperarte de los momentos difíciles.
          </p>
          <p>
            Tu puntaje en Recuperación del equilibrio indica que en ocasiones
            para ti es difícil regresar a un estado de tranquilidad después de
            haber perdido el control por emociones de tristeza, enojo o
            exaltación.
          </p>
          <p>
            A continuación, te pedimos que realices un ejercicio que te ayudará
            a saber cómo recuperar la tranquilidad en diferentes situaciones.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EquibMedio;
