import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const EnojoNiIdEjer = () => {
  const [respuestas, setRespuestas] = useState({ respuesta: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  const handleChange = (event) => {
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const enojoAdtRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/enojo/enojo_ni_id_ejer`
    );
    enojoAdtRuEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Qué haces tú para controlar el enojo?',
            respuesta: respuestas.respuesta,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.resp = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.resp = respuestas.respuesta;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  };

  return (
    <Card body inverse color="danger">
      <CardHeader tag="h1">Ejercicio manejo enojo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Lee el siguiente relato y contesta la pregunta de reflexión Es el
            cumpleaños de Laura y su mamá le organizó una fiesta, le hizo pozole
            que es su comida favorita, todos sus amiguitos se sentaron la mesa y
            una de sus tías estaba sirviendo el pozole preparados con lechuga,
            rábanos y cebolla, cuando le dieron su pozole a Laura se dio cuenta
            que le habían puesto cebolla, a Laura no le gusta la cebolla y se ha
            enojado mucho
          </p>
          <p>Rosa, su amiga le dijo:</p>
          <p>
            -Cuando yo estoy enojada por algún problema trato de cerrar mis ojos
            y respirar profundo para calmarme, también me funciona alejarme y
            estar un momento a solas para después pensar en alguna solución
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>¿Qué haces tú para controlar el enojo?</td>
              </tr>
              <tr>
                <td>
                  <textarea
                    type="text"
                    name="respuesta"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%', marginTop: '1em' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EnojoNiIdEjer;
