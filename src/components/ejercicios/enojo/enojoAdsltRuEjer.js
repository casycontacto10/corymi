import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const EnojoAdsltRuEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  function handleChange(event) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const enojoAdsltUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/enojo/enojo_adslt_ru_ejer`
    );
    enojoAdsltUbEjer.update({
      respuestas: [
        {
          pregunta: '¿Qué nos enseña la historia?',
          respuesta: event.target.value,
        },
      ],
      diagnostico: true,
    },
    (error) => {
      estado.error = false;
      estado.exito = false;
      estado.resp = false;
      setEstado(estado);

      if (error) {
        estado.error = true;
        setEstado(estado);
      } else {
        estado.exito = true;
        estado.resp = event.target.value;
        setEstado(estado);
      }
    });
  }
  return (
    <Card body inverse color="danger">
      <CardHeader tag="h1">Ejercicio manejo enojo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Esta es la historia de un muchachito que tenía muy mal carácter. Su
            padre le dio una bolsa de clavos y le dijo que cada vez que perdiera
            la paciencia, debería clavar un clavo detrás de la puerta. El primer
            día, el muchacho clavó 37 clavos detrás de la puerta. Las semanas
            que siguieron, a medida que él aprendía a controlar su genio,
            clavaba cada vez menos clavos detrás de la puerta. Descubrió que era
            más fácil controlar su carácter durante todo el día. Después de
            informar a su padre, éste le sugirió que retirara un clavo cada día
            que lograra controlar su carácter. Los días pasaron y el joven pudo
            finalmente anunciar a su padre que no quedaban más clavos para
            retirar de la puerta. Su padre lo tomó de la mano y lo llevó hasta
            la puerta. Le dijo: “Has trabajado duro, hijo mío, pero mira todos
            esos hoyos en la puerta. Nunca más será la misma. Cada vez que tú
            pierdes la paciencia, dejas cicatrices exactamente como las que aquí
            ves”. Contesta la pregunta eligiendo una opción ¿Qué nos enseña la
            historia?
          </p>

          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="opc"
                  value="Que tenemos que comprarnos una caja de clavos"
                  onChange={handleChange}
                />
              </td>
              <td>Que tenemos que comprarnos una caja de clavos</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="opc"
                  value="Tú puedes insultar a alguien y retirar lo dicho, pero dejará una cicatriz que será difícil de quitar"
                  onChange={handleChange}
                />
              </td>
              <td>
                Tú puedes insultar a alguien y retirar lo dicho, pero dejará una
                cicatriz que será difícil de quitar
              </td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="opc"
                  value="Que los papás siempre tienen la razón"
                  onChange={handleChange}
                />
              </td>
              <td>Que los papás siempre tienen la razón</td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EnojoAdsltRuEjer;
