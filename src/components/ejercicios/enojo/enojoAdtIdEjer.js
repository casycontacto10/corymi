import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const EnojoAdtIdEjer = () => {
  const [preguntas, setPreguntas] = useState({
    p1: '',
    otro: '',
    disable: true,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    otro: false,
  });

  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
    if (event.target.name === 'p1' && event.target.value !== 'otro') {
      preguntas.otro = '';
      preguntas.disable = true;
    }
    if (event.target.value === 'otro') {
      preguntas.disable = false;
    }
  };
  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const enojoAdtIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/enojo/enojo_adt_id_ejer`
    );
    enojoAdtIdEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Tú qué haces para tranquilizarte cuando estás muy enojado o enojada?',
            respuesta: preguntas.p1,
            otro_texto: preguntas.otro,
            otro: !preguntas.disable,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.otro = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = preguntas.p1;
          estado.otro = preguntas.otro;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="danger">
      <CardHeader tag="h1">Ejercicio manejo enojo</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta la pregunta de reflexión:</p>
          <p>
            Héctor encontró a unos niños jugando en su parcela y pisando su
            cosecha. No era la primera vez que eso ocurría y se molestó tanto
            que pensó en correrlos a palazos. Mientras buscaba un palo, pensó
            que si les hacía daño tendría más problemas así que mejor decidió
            hablar con los papás de los niños para que les explicaran por qué no
            debían jugar ahí.
          </p>
          <p>
            Aunque estaba muy enojado, Héctor controló su enojo para evitar un
            problema mayor.
          </p>
          <p>
            ¿Tú qué haces para tranquilizarte cuando estás muy enojado o
            enojada?
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Me quedo solo hasta calmarme"
                    onChange={handleChange}
                  />
                </td>
                <td>Me quedo solo hasta calmarme</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Salgo a tomar aire para calmarme"
                    onChange={handleChange}
                  />
                </td>
                <td>Salgo a tomar aire para calmarme</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Gritar hasta que se me pasa el enojo"
                    onChange={handleChange}
                  />
                </td>
                <td>Gritar hasta que se me pasa el enojo</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="otro"
                    onChange={handleChange}
                  />
                </td>
                <td>Otro</td>
                <td>
                  <textarea
                    disabled={preguntas.disable}
                    name="otro"
                    value={preguntas.otro}
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.otro}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EnojoAdtIdEjer;
