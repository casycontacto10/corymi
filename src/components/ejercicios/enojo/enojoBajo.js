import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const EnojoBajo = () => {
  return (
    <Card body inverse color="danger">
      <CardHeader tag="h1">Dificultades en el manejo del enojo</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: BAJO</CardTitle>
        <CardText>
          <p>
            Felicitaciones, tu puntaje en el manejo del enojo indica que la
            forma en la que reaccionas ante una situación de frustración,
            inconformidad o desagrado es la correcta. Sigue así.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EnojoBajo;
