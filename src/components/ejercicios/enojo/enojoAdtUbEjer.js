import React, { useState } from 'react';
import {
  Row,
  Card,
  CardBody,
  CardText,
  CardHeader,
  Button,
  Alert,
} from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';
import audioURL from '../audios/enojo/ENOJO_ADULTO_UB.mpeg';

import { database } from '../../../helpers/Firebase';

const EnojoAdtUbEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta: '',
  });
  const [audio] = useState(new Audio(audioURL));
  const [isPlaying, setIsPlaying] = useState(false);
  const playPause = () => {
    if (isPlaying) {
      audio.pause();
    } else {
      audio.play();
    }
    setIsPlaying(!isPlaying);
  };
  const [pregunta, setPregunta] = useState('');

  const handleChange = (event) => {
    setPregunta(event.target.value);
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const enojoAdtUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/enojo/enojo_adt_ub_ejer`
    );
    enojoAdtUbEjer.update(
      {
        respuestas: [
          { pregunta: '¿Cómo te resultó el ejercicio?', respuesta: pregunta },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.respuesta = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.respuesta = pregunta;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  };

  return (
    <Card body inverse color="danger">
      <CardHeader tag="h1">Ejercicio manejo enojo</CardHeader>
      <CardBody>
        <CardText>
          <p>Reproduce el siguiente audio y contesta la pregunta</p>
          <p>
            <Button color="info" onClick={playPause}>
              {isPlaying ? 'Detener' : 'Reproducir'}
            </Button>
          </p>

          <form onSubmit={setRespuesta}>
            <Row>
              <Colxx xxs="4">
                <p>¿Cómo te resultó el ejercicio?</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea
                  name="pregunta"
                  onChange={handleChange}
                  style={{
                    height: '100%',
                    width: '100%',
                  }}
                  rows="5"
                />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="4">
                <input type="submit" />
              </Colxx>
            </Row>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EnojoAdtUbEjer;
