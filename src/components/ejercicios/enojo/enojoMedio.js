import React from 'react';
import { Card, CardTitle, CardBody, CardText, CardHeader } from 'reactstrap';

const EnojoMedio = () => {
  return (
    <Card body inverse color="danger">
      <CardHeader tag="h1">Dificultades en el manejo del enojo </CardHeader>
      <CardBody>
        <CardText>
          <CardTitle tag="h5">DIAGNÓSTICO: MEDIO</CardTitle>
          <p>
            La mayoría de las veces manejas adecuadamente las situaciones que te
            molestan o incomodan, sin embargo, hay ocasiones en las que pierdes
            el control y actúas impulsivamente, lo que te puede llevar a ofender
            a los demás, o a sentirte mal física o emocionalmente.
          </p>
          <p>
            Tu puntaje en el recurso psicológico Manejo del enojo indica que
            algunas veces la forma en la que reaccionas ante una situación de
            frustración, inconformidad o desagrado no es buena para ti o para
            las personas que están a tu alrededor.
          </p>
          <p>
            A continuación, te pedimos que realices un ejercicio que te ayudará
            a identificar este tipo de situaciones y cómo manejarlas.
          </p>
        </CardText>
      </CardBody>
    </Card>
    
  );
};

export default EnojoMedio;
