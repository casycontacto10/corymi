import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert, Row } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';
import Emoji from 'a11y-react-emoji';
import { database } from '../../../helpers/Firebase';

const EnojoNiUbEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  function setRespuesta(respuesta) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const enojoNiUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/enojo/enojo_ni_ub_ejer`
    );
    enojoNiUbEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Te gustó la actividad? Selecciona una carita',
            respuestas: respuesta,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = respuesta;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Ejercicio manejo enojo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Para entender nuestras emociones, antes debemos aprender a
            identificarlas. Para ello, te pedimos que tengas a la mano una hoja
            y colores. Ahora necesito que dibujes 6 caritas que representen las
            emociones básicas, piensa en aquellas cosas que te hagan sentir esas
            emociones, y escríbelas en un espacio libre de tu hoja.
          </p>
          <p>Ejemplo de las emociones básicas</p>

          <p>
            <img
              src="/assets/img/ejercicios/enojo/enojo_ni_ub_ejer.png"
              alt=""
            />
          </p>

          <p>
            Ya que identificaste tus emociones, es más fácil entenderlas,
            entonces pensarás ¿Qué podemos hacer cuando estemos sintiendo enojo,
            miedo y tristeza?
          </p>

          <ul>
            <li>Respira profundamente, hasta que te sientas tranquilo(a)</li>
            <li>
              Cuando estés tranquilo(a), habla con alguien para que entienda
              como te sientes y puedan llegar acuerdos o apoyarte.
            </li>
          </ul>
          <p>Recuerda que…</p>
          <p>
            El saber decir que nos hace sentir bien o mal nos ayuda a sentirnos
            con más paz, y mejor con nosotros mismos (Cossin, Rubinsteir,
            Politis, 2017).
          </p>
          <p>¿Te gustó la actividad? Selecciona una carita</p>
          <Row style={{ paddingTop: '1em' }}>
            <Colxx xxs="2">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😃"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('me_encanta')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> Me encanta </p>
              </div>
            </Colxx>
            <Colxx xxs="2">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="🙂"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('me_agrada')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> Me agrada </p>
              </div>
            </Colxx>
            <Colxx xxs="3">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😐"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('mas_o_menos')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p>Mas o menos</p>
              </div>
            </Colxx>
            <Colxx xxs="2">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😠"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('no_me_gusto')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> No me gusto </p>
              </div>
            </Colxx>
            <Colxx xxs="3">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😡"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('no_me_gusto_nada')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> No me gusto nada </p>
              </div>
            </Colxx>
          </Row>
          {estado.exito ? (
            <Alert>
              <p>{estado.resp}</p>
              <p>Registrado correctamente</p>
            </Alert>
          ) : null}
          {estado.error ? (
            <Alert>
              <p>Error al guardar</p>
            </Alert>
          ) : null}
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EnojoNiUbEjer;
