/* eslint-disable no-alert */
/* eslint-disable func-names */
import React, { useState } from 'react';
import {
  Card,
  CardBody,
  CardText,
  CardHeader,
  Button,
  Alert,
} from 'reactstrap';

import { storage, database } from '../../../helpers/Firebase';

const EnojoNiRuEjer = () => {
  const [image, setImage] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  const upload = () => {
    const id = localStorage.getItem('recursoId');
    estado.error = false;
    estado.exito = false;
    estado.resp = false;
    setEstado(estado);
    if (!image) {
      estado.error = true;
      estado.resp = 'no hay archivo para guardar';
      setEstado(estado);
    } else {
      storage
        .ref(`/${id}/enojo/enojo_ni_ru_ejer/${image.name}`)
        .put(image)
        .on(
          'state_changed',
          function () {
            estado.error = true;
            estado.resp = 'subiendo archivo';
            setEstado(estado);
          },
          function () {
            estado.error = true;
            estado.resp = 'error al subir archivo';
            setEstado(estado);
          },
          function () {
            const refe = database.ref();
            const enojoNiRuEjer = refe.child(
              `respuestasPersona/${id}/ejercicios/enojo/enojo_ni_ru_ejer`
            );
            enojoNiRuEjer.update(
              {
                archivo: `/${id}/enojo/enojo_ni_ru_ejer/${image.name}`,
                diagnostico: true,
              },
              (error) => {
                estado.error = false;
                estado.exito = false;
                estado.resp = false;
                setEstado(estado);
                if (error) {
                  estado.error = true;
                  estado.resp = 'error al guardar';
                  setEstado(estado);
                } else {
                  estado.exito = true;
                  estado.resp = `${image.name} guardado correctamente`;
                  setEstado(estado);
                }
              }
            );
          }
        );
    }
  };
  return (
    <Card body inverse color="danger">
      <CardHeader tag="h1">Ejercicio manejo enojo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Todos los días, después de hacer sus deberes, a Luis le gusta mucho
            ir al campo junto con sus amigos a jugar, un día a Luis propuso a
            sus amigos jugar futbol, pero a los demás no les pareció buena idea
            y prefirieron jugar a las escondidas, en ese momento Luis se enojó
            tanto que les dijo a todos que ya no quería ser su amigo y no
            volvería a jugar con ellos jamás, comenzó a llorar y se fue
            corriendo a su casa. Cuando nos enojamos mucho a veces nos cuesta
            mucho trabajo controlarnos, eso puede llevarnos a algunos de
            nosotros a hacer cosas como gritarles a los demás, decirles cosas
            hirientes, pegar, romper cosas y luego sentirnos muy arrepentidos o
            apenados.
          </p>
          <p>
            A continuación,hay una lista de cosas que puede hacer cuando te
            sientas muy enojado y sientas que no te puedes controlar:
          </p>

          <ol>
            <li>
              Respira profundamente varias veces, toma aire por la nariz como si
              olieras una flor y sácalo por la boca como si soplaras una vela
              hasta que te sientas más tranquilo
            </li>
            <li>Patea una pelota</li>
            <li>
              Aléjate del lugar donde comenzaste a sentirte enojado y ve a otro
              donde puedas estar un momento solo
            </li>
            <li>
              Pídele a un adulto que te acompañe a dar un paseo, mientras puedes
              contarle cómo te sientes
            </li>
            <li>
              Escribe en una hoja que es lo que te hizo sentirte enojado y por
              qué, después puedes romperla
            </li>
          </ol>
          <p>
            Haz un dibujo como el de la imagen (puedes escoger otra figura) para
            que puedas recordar tu lista y pégala en algún lugar de tu casa.
          </p>

          <p>
            <input
              type="file"
              onChange={(e) => {
                setImage(e.target.files[0]);
              }}
            />
          </p>
          <p>
            <Button onClick={upload} color="info">
              Subir
            </Button>
          </p>
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="warning">
                <p>{estado.resp}</p>
              </Alert>
            ) : null}
          </p>
          <p>Sube una foto de tu dibujo</p>
          <p>
            <img
              alt=""
              src="/assets/img/ejercicios/enojo/enojo_ni_ru_ejer.png"
            />
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EnojoNiRuEjer;
