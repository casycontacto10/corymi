/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const EnojoAdsltUbEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  function handleChange(event) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const enojoAdsltUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/enojo/enojo_adslt_ub_ejer`
    );
    enojoAdsltUbEjer.update(
      {
        respuestas: [
          {
            pregunta:
              'A continuación, elije una opción de lo que crees que sucedió en la historia:',
            respuesta: event.target.value,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = event.target.value;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="danger">
      <CardHeader tag="h1">Ejercicio manejo enojo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Se siente enojo cuando algo no sale como lo planeamos, cuando vemos
            una injusticia o cuando alguien nos lastima con sus palabras o sus
            acciones. Sin embargo, cuando estamos muy enojados podemos decir o
            hacer cosas que lastiman a las personas que están a nuestro al
            rededor. Para evitarlo, primero debemos reconocer que estamos
            enojados, decir qué es lo que nos provoca el enojo, o bien podemos
            alejarnos, salir al jardín o sentarnos afuera de la casa, cerrar los
            ojos y respirar. Por ejemplo: Eduardo tiene 14 años, vive en Zamora
            y está cursando el último año de secundaria. En la última semana de
            clases, justo antes de su graduación, se sintió muy triste porque su
            mamá le dijo que no era posible regresar de Estados Unidos para
            estar con él ese día tan importante, entonces él enojado le colgó la
            llamada y se ha negado a establecer contacto nuevamente con ella.
            Pablo quien es su mejor amigo, se dio cuenta de lo que estaba
            pasando y platicó con él acerca de ello, le explicó que el que su
            mamá no asistiera no quería decir que no lo quisiera o que no le
            importara, por el contrario, ya que si ella seguía trabajando era
            para que él pudiera estudiar y aspirar a un futuro mejor. Entonces,
            en ese momento Eduardo recordó que aunque su mamá estaba lejos,
            nunca se había desatendido de él, porque cada sábado hablaban por
            teléfono, le mandaba cartas, además, a sus abuelos nunca les faltaba
            el dinero para la comida y la escuela, porque ella era muy
            responsable.
          </p>

          <p>
            A continuación, elije una opción de lo que crees que sucedió en la
            historia:
          </p>

          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="opc"
                  value="Eduardo se enojó y no asistió a la graduación"
                  onChange={handleChange}
                />
              </td>
              <td>Eduardo se enojó y no asistió a la graduación</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="opc"
                  value="Eduardo se peleó con Pablo y sus abuelos"
                  onChange={handleChange}
                />
              </td>
              <td>Eduardo se peleó con Pablo y sus abuelos</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="opc"
                  value="Eduardo reflexionó, habló con su mamá y le pidió disculpas. Asistió a su graduación y le envío muchas fotos, para que ella se sintiera muy orgullosa de él"
                  onChange={handleChange}
                />
              </td>
              <td>
                Eduardo reflexionó, habló con su mamá y le pidió disculpas.
                Asistió a su graduación y le envío muchas fotos, para que ella
                se sintiera muy orgullosa de él
              </td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EnojoAdsltUbEjer;
