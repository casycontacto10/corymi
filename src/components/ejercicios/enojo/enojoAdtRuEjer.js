/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const EnojoAdtRuEjer = () => {
  const [respuestas, setRespuestas] = useState({ p1: '', p2: '', p3: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta1: '',
    respuesta2: '',
    respuesta3: '',
  });
  const handleChange = (event) => {
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const enojoAdtRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/enojo/enojo_adt_ru_ejer`
    );
    enojoAdtRuEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Qué puedo hacer a partir de ahora?',
            respuesta: respuestas.p1,
          },
          {
            pregunta: '¿Cómo puedo aprovechar lo que está sucediendo?',
            respuesta: respuestas.p2,
          },
          {
            pregunta: '¿Para que necesito salir de esto?',
            respuesta: respuestas.p3,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.respuesta1 = false;
        estado.respuesta2 = false;
        estado.respuesta3 = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.respuesta1 = respuestas.p1;
          estado.respuesta2 = respuestas.p2;
          estado.respuesta3 = respuestas.p3;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  };

  return (
    <Card body inverse color="danger">
      <CardHeader tag="h1">Ejercicio manejo enojo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Las personas enojadas tienden a sacar conclusiones y actuar en
            consecuencia y algunas de esas conclusiones pueden ser bastante
            extremas. Lo primero que se debe hacer si está en una discusión
            acalorada es tranquilizarse y pensar. Para este tipo de situaciones
            es útil saber qué es lo que está pasando y lo que podrías estar
            experimentando. En una hoja escribe alguna situación que se te venga
            a la mente, en la que hayas estado en una discusión o problema y que
            te haya hecho sentir muy enojado, una vez que lo has hecho explora
            que soluciones que podría tener. Contesta las siguientes preguntas:
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>¿Qué puedo hacer a partir de ahora?</td>
                <td>
                  <textarea
                    type="text"
                    name="p1"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>¿Cómo puedo aprovechar lo que está sucediendo?</td>
                <td>
                  <textarea
                    type="text"
                    name="p2"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>¿Para que necesito salir de esto?</td>
                <td>
                  <textarea
                    type="text"
                    name="p3"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta1}</p>
                <p>{estado.respuesta2}</p>
                <p>{estado.respuesta3}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EnojoAdtRuEjer;
