import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const EnojoAdsltIdEjer = () => {
  const [preguntas, setPreguntas] = useState({
    p1: '',
    otro: '',
    disable: true,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    otro: false,
  });
  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
    if (event.target.name === 'p1' && event.target.value !== 'otro') {
      preguntas.otro = '';
      preguntas.disable = true;
    }
    if (event.target.value === 'otro') {
      preguntas.disable = false;
    }
  };
  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const enojoAdsltIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/enojo/enojo_adslt_id_ejer`
    );
    enojoAdsltIdEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Tú cómo actúas cuando estás muy enojado o enojada?',
            respuesta: preguntas.p1,
            otro_texto: preguntas.otro,
            otro: !preguntas.disable,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.otro = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = preguntas.p1;
          estado.otro = preguntas.otro;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="danger">
      <CardHeader tag="h1">Ejercicio manejo enojo</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta la pregunta de reflexión:</p>
          <p>
            Jaime estaba jugando con sus compañeros de la escuela y uno de ellos
            le pegó con el codo sin darse cuenta, le dolió tanto que quiso
            pegarle él también. Pero pensó en que sería mejor gritar, aventar
            piedras al suelo fuerte y dar patadas al aire porque si le pegaba,
            eso traería más problemas porque su compañero no lo hizo con
            intención, solo fue un accidente. Cuando se tranquilizó le pidió que
            tuviera más cuidado.
          </p>
          <p>
            Jaime controló su enojo gritando, aventando piedras y dando patadas
            al aire en lugar de pegarle a su compañero,
          </p>
          <p>¿Tú cómo actúas cuando estás muy enojado o enojada?</p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Golpeo o aviento cosas"
                    onChange={handleChange}
                  />
                </td>
                <td>Golpeo o aviento cosas</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Respiro profundamente para calmarme"
                    onChange={handleChange}
                  />
                </td>
                <td>Respiro profundamente para calmarme</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Hablo con respeto sobre lo que me hizo enojar"
                    onChange={handleChange}
                  />
                </td>
                <td>Hablo con respeto sobre lo que me hizo enojar</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="otro"
                    onChange={handleChange}
                  />
                </td>
                <td>Otro</td>
                <td>
                  <textarea
                    disabled={preguntas.disable}
                    type="text"
                    name="otro"
                    value={preguntas.otro}
                    onChange={handleChange}
                    rows="5"
                    style={{ height: '100%', width: '100%' }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.otro}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EnojoAdsltIdEjer;
