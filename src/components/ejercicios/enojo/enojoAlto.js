import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const EnojoAlto = () => {
  return (
    <Card body inverse color="danger">
      <CardHeader tag="h1">Dificultades en el manejo del enojo</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: ALTO</CardTitle>
        <CardText>
          <p>
            Con frecuencia te sientes muy enojado y actúas de manera impulsiva,
            lo que te lleva a aventar objetos o a ofender a los demás.
          </p>
          <p>
            Tu puntaje en el Manejo del enojo indica que la forma en la que
            reaccionas ante una situación de frustración, inconformidad o
            desagrado no es buena para ti o para las personas que están a tu
            alrededor.
          </p>
          <p>
            A continuación, te pedimos que realices un ejercicio que te ayudará
            a identificar este tipo de situaciones y cómo manejarlas de una
            mejor forma.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default EnojoAlto;
