import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const AutoctrlAdtIdEjer = () => {
  const [preguntas, setPreguntas] = useState({
    p1: '',
    p2: '',
    otro: '',
    disable: true,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta1: '',
    respuesta2: '',
    respuestaO: '',
  });

  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
    if (event.target.name === 'p2' && event.target.value !== 'otro') {
      preguntas.otro = '';
      preguntas.disable = true;
    }
    if (event.target.value === 'otro') {
      preguntas.disable = false;
    }
  };

  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const autoctrlAdtIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/autocontrol/autoctrl_adt_id_ejer`
    );
    autoctrlAdtIdEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Estuvo bien que Juan le gritara a Pedro por desobedecerlo?',
            respuesta: preguntas.p1,
          },
          {
            pregunta: '¿Qué te ayudaría a tranquilizarte para no gritar?',
            respuesta: preguntas.p2,
            otro_texto: preguntas.otro,
            otro: !preguntas.disable,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.respuesta1 = preguntas.p1;
        estado.respuesta2 = preguntas.p2;
        estado.respuestaO = preguntas.otro;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse color="secondary">
      <CardHeader tag="h1">Ejercicio autocontrol</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta las preguntas de reflexión:</p>
          <p>
            Juan le ordenó a su hijo Pedro que le diera de comer a los borregos.
            Pedro se distrajo jugando y no obedeció a su papá. Cuando Juan se
            dio cuenta de que lo había desobedecido, se enojó mucho y le gritó
            tanto que lo hizo llorar. Cuando se tranquilizó, Juan pensó que no
            era correcto gritarle a Pedro y que la próxima vez que tuviera que
            regañarlo, podía explicarle lo que había hecho mal sin gritarle.
          </p>
          <p>¿Estuvo bien que Juan le gritara a Pedro por desobedecerlo?</p>

          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="si"
                  onChange={handleChange}
                />
              </td>
              <td>Si</td>
            </tr>

            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="no"
                  onChange={handleChange}
                />
              </td>
              <td>No</td>
            </tr>
          </table>
          <p>¿Qué te ayudaría a tranquilizarte para no gritar?</p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p2"
                    value="Respirar profundamente"
                    onChange={handleChange}
                  />
                </td>
                <td>Respirar profundamente </td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p2"
                    value="Pensar si puedo llamar la atención de otra forma"
                    onChange={handleChange}
                  />
                </td>
                <td>Pensar si puedo llamar la atención de otra forma</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p2"
                    value="Esperar un tiempo para calmarme"
                    onChange={handleChange}
                  />
                </td>
                <td>Esperar un tiempo para calmarme</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p2"
                    value="otro"
                    onChange={handleChange}
                  />
                </td>
                <td>Otro </td>
                <td>
                  <textarea
                    disabled={preguntas.disable}
                    name="otro"
                    value={preguntas.otro}
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta1}</p>
                <p>{estado.respuesta2}</p>
                <p>{estado.respuestaO}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default AutoctrlAdtIdEjer;
