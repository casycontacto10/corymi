import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const AutoctrlBajo = () => {
  return (
    <Card body inverse color="secondary">
      <CardHeader tag="h1">Autocontrol</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: BAJO</CardTitle>
        <CardText>
          <p>
            Cuando te enojas, te cuesta mucho trabajo permanecer tranquilo y
            seguir siendo amable a pesar de que intentes no alterarte. Además de
            que te lleva mucho tiempo el poder volver a calmarte.
          </p>
          <p>
            Tu puntaje en Autocontrol indica que tu capacidad para expresar
            emociones y contener tus impulsos es baja, lo cual no es bueno para
            ti o para las personas que están a tu alrededor.
          </p>
          <p>
            A continuación te pedimos que realices un ejercicio que te ayudará a
            mejorar tu capacidad de Autocontrol en diferentes situaciones.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default AutoctrlBajo;
