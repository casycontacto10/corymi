import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const AutoctrlMedio = () => {
  return (
    <Card body inverse color="secondary">
      <CardHeader tag="h1">Autocontrol</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: MEDIO</CardTitle>
        <CardText>
          <p>
            Hay ocasiones que cuando hay problemas te cuesta trabajo mantenerte
            en calma, mostrarte amable y no alterarte.
          </p>
          <p>
            Tu puntaje en Autocontrol indica que algunas veces tu capacidad para
            expresar emociones y contener tus impulsos es baja, lo cual no es
            bueno para ti o para las personas que están a tu alrededor.
          </p>
          <p>
            A continuación te pedimos que realices un ejercicio que te ayudará a
            mejorar tu capacidad de Autocontrol en diferentes situaciones
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default AutoctrlMedio;
