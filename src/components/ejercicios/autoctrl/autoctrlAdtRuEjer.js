import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const AutoctrlAdtRuEjer = () => {
  const [fields, setFields] = useState({
    alegria: false,
    enojo: false,
    tristeza: false,
    nostalgia: false,
    miedo: false,
    asco: false,
    angustia: false,
    ansiedad: false,
    anoranza: false,
    carino: false,
    soledad: false,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    emocion: false,
  });
  const handleChange = (event) => {
    event.preventDefault();
    fields[event.target.name] = event.target.checked;
    setFields(fields);
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const autoctrlAdtRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/autocontrol/autoctrl_adt_ru_ejer`
    );
    autoctrlAdtRuEjer.update(
      {
        respuestas: [
          { pregunta: 'alegria', respuesta: fields.alegria },
          { pregunta: 'enojo', respuesta: fields.enojo },
          { pregunta: 'tristeza', respuesta: fields.tristeza },
          { pregunta: 'nostalgia', respuesta: fields.nostalgia },
          { pregunta: 'miedo', respuesta: fields.miedo },
          { pregunta: 'asco', respuesta: fields.asco },
          { pregunta: 'angustia', respuesta: fields.angustia },
          { pregunta: 'ansiedad', respuesta: fields.ansiedad },
          { pregunta: 'anoranza', respuesta: fields.anoranza },
          { pregunta: 'carino', respuesta: fields.carino },
          { pregunta: 'soledad', respuesta: fields.soledad },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.emocion = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.emocion = event.target.name;

          setEstado(estado);
        }
      }
    );
  };

  return (
    <Card body inverse color="secondary">
      <CardHeader tag="h1">Ejercicio autocontrol</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Todas las personas, hombres y mujeres, tenemos emociones. Algunas
            son más fáciles de controlar que otras, pero siempre es importante
            que podamos identificarlas, por ejemplo después del fallecimiento de
            una persona querida y estimada. Todas las emociones las sentimos por
            algo, no está mal sentirlas, pero para poder manejarlas,tenemos que
            primero identificarlas, y aceptar que las sentimos.
          </p>
          <p>
            A continuación te voy a pedir, que cierres los ojos un momento, y
            recuerdes a tu ser querido. Pueden aparecer muchos recuerdos, o sólo
            uno. No importa. Lo importante es identificar qué es lo que siente,
            que emociones aparecen con el recuerdo que se le presenta.
          </p>
          <p>
            A continuación hay un listado de emociones que se pueden sentir
            durante el duelo. Por favor marca las emociones que has
            experimentado al recordar a tu ser querido.
          </p>
          <table>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="alegria"
                  onChange={handleChange}
                  checked={fields.alegria}
                />
              </td>
              <td>Alegría</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="enojo"
                  onChange={handleChange}
                  checked={fields.enojo}
                />
              </td>
              <td> Enojo</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="tristeza"
                  onChange={handleChange}
                  checked={fields.tristeza}
                />
              </td>
              <td>Tristeza</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="nostalgia"
                  onChange={handleChange}
                  checked={fields.nostalgia}
                />
              </td>
              <td>Nostalgia</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="miedo"
                  onChange={handleChange}
                  checked={fields.miedo}
                />
              </td>
              <td>Miedo</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="asco"
                  onChange={handleChange}
                  checked={fields.asco}
                />
              </td>
              <td>Asco</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="angustia"
                  onChange={handleChange}
                  checked={fields.angustia}
                />
              </td>
              <td>Angustia</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="ansiedad"
                  onChange={handleChange}
                  checked={fields.ansiedad}
                />
              </td>
              <td>Ansiedad</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="anoranza"
                  onChange={handleChange}
                  checked={fields.anoranza}
                />
              </td>
              <td>Añoranza</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="carino"
                  onChange={handleChange}
                  checked={fields.carino}
                />
              </td>
              <td>Cariño</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="soledad"
                  onChange={handleChange}
                  checked={fields.soledad}
                />
              </td>
              <td>Soledad</td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.emocion}</p>
                <p>Actualizado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert>
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
          <p>
            Si aparecen emociones muy incomodas en los recuerdos, no se
            preocupe, obsérvelas, déjelas pasar. No trate de suprimirlas. Esa
            emoción pasará. Respire hondo, exhale, y repita (tres veces).
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default AutoctrlAdtRuEjer;
