import React, { useState } from 'react';
import { Row, Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';

import { database } from '../../../helpers/Firebase';

const AutoctrlNiRuEjer = () => {
  const [resp, setResp] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta: '',
  });
  const handleChange = (event) => {
    setResp(event.target.value);
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const autoctrlNiRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/autocontrol/autoctrl_ni_ru_ejer`
    );
    autoctrlNiRuEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿cuál es la que más te ha gustado y la que podrías aplicar tú cuando sientas que podrías actuar de alguna forma en la que normalmente no lo harías?',
            respuesta: resp,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.respuesta = resp;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  };
  return (
    <Card body inverse color="secondary">
      <CardHeader tag="h1">Ejercicio autocontrol</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Cuando una situación no resulta como nosotros lo esperamos es
            posible que puedas llegar a sentirte enojado, triste, furioso,
            incluso con ganas de actuar en formas en las que normalmente no lo
            harías. Para evitar que algo que no queremos que suceda, podemos
            aplicar algunas de las siguientes ideas para no actuar en formas en
            las que normalmente no lo haríamos.{' '}
          </p>
          <ol>
            <li>Respirar muy profundo </li>
            <li>Pensar en algo que me gusta mucho</li>
            <li>Irme a otro sitio en el que me sieta más cómodo</li>
            <li>Pensar que yo podría sentirme mal si estuviera en su lugar</li>
          </ol>
          <p>Contesta la siguiente pregunta</p>
          <p>De las ideas presentadas:</p>
          <form onSubmit={setRespuesta}>
            <Row>
              <Colxx xxs="4">
                <p>
                  ¿cuál es la que más te ha gustado y la que podrías aplicar tú
                  cuando sientas que podrías actuar de alguna forma en la que
                  normalmente no lo harías?
                </p>
              </Colxx>
              <Colxx xxs="8">
                <textarea
                  type="text"
                  name="resp"
                  onChange={handleChange}
                  style={{ height: '100%', width: '100%' }}
                />
              </Colxx>
            </Row>

            <Row>
              <Colxx xxs="4">
                <input type="submit" />
              </Colxx>
            </Row>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default AutoctrlNiRuEjer;
