import React, { useState } from 'react';
import { Row, Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';
import Emoji from 'a11y-react-emoji';

import { database } from '../../../helpers/Firebase';

const AutoctrlNiUbEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  function setRespuesta(respuesta) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const autoctrlNiUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/autocontrol/autoctrl_ni_ub_ejer`
    );
    autoctrlNiUbEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Cómo te sentiste? Selecciona una carita',
            respuesta,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = respuesta;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="secondary">
      <CardHeader tag="h1">Ejercicio autocontrol</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Existen diferentes motivos por los que te puedes sentir nervioso o
            triste. Por ejemplo: Si estás muy nervioso por un examen, porque te
            van a poner una vacuna o no te has podido comunicar con algún
            familiar de Estados Unidos. Recuerda que esas emociones forman parte
            de la vida diaria y que a todos nos pasa. Sin embargo, existen
            diferentes maneras con las que te puedes sentir mejor. Por ejemplo:
            Cierra los ojos y piensa cosas bonitas para ti, puede ser en tu
            mascota, en que estás jugando con tus amigos, o que ves tu película
            favorita. Mientras estás pensando en esas cosas, respira
            profundamente.
          </p>
          <p>¿Cómo te sentiste? Selecciona una carita</p>
          <Row style={{ paddingTop: '1em' }}>
            <Colxx xxs="2">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😃"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('me_encanta')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> Me encanta </p>
              </div>
            </Colxx>
            <Colxx xxs="2">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="🙂"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('me_agrada')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> Me agrada </p>
              </div>
            </Colxx>
            <Colxx xxs="3">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😐"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('mas_o_menos')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p>Mas o menos</p>
              </div>
            </Colxx>
            <Colxx xxs="2">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😠"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('no_me_gusto')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> No me gusto </p>
              </div>
            </Colxx>
            <Colxx xxs="3">
              <div style={{ textAlign: 'center' }}>
                <Emoji
                  symbol="😡"
                  style={{ 'font-size': '50px' }}
                  onClick={() => setRespuesta('no_me_gusto_nada')}
                />
              </div>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: '0.5em',
                  'font-size': '20px',
                  width: '100%',
                }}
              >
                <p> No me gusto nada </p>
              </div>
            </Colxx>
          </Row>

          {estado.exito ? (
            <Alert>
              <p>{estado.resp}</p>
              <p>Registrado correctamente</p>
            </Alert>
          ) : null}
          {estado.error ? (
            <Alert>
              <p>Error al guardar</p>
            </Alert>
          ) : null}
        </CardText>
      </CardBody>
    </Card>
  );
};

export default AutoctrlNiUbEjer;
