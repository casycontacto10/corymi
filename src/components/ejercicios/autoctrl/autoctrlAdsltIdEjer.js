import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const AutoctrlAdsltIdEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta: '',
  });
  function setRespuesta(resultado) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const autoctrlAdsltIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/autocontrol/autoctrl_adslt_id_ejer`
    );
    autoctrlAdsltIdEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Crees que fue correcto lo que hizo Maricruz?',
            respuesta: resultado,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.respuesta = resultado;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="secondary">
      <CardHeader tag="h1">Ejercicio autocontrol</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta las preguntas de reflexión:</p>
          <p>
            Maricruz esperaba con mucha ilusión que Luis la invitara al baile de
            la fiesta patronal, pero él decidió invitar a Julia, su hermana
            mayor. Maricruz se sintió muy enojada y pensó en decirles a sus
            papás alguna mentira sobre Julia para que no le dieran permiso para
            ir al baile. Cuando estaba a punto de hablar con su mamá, pensó que
            si descubrían que mentía a quien le quitarían el permiso sería a
            ella y decidió no hacerlo.
          </p>
          <p>¿Crees que fue correcto lo que hizo Maricruz?</p>

          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="r"
                  onClick={() => setRespuesta('Si')}
                />
              </td>
              <td>Si</td>
            </tr>

            <tr>
              <td>
                <input
                  type="radio"
                  name="r"
                  onClick={() => setRespuesta('No')}
                />
              </td>
              <td>No</td>
            </tr>
          </table>
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                Error al guardar respuesta: {estado.respuesta}.
              </Alert>
            ) : null}
          </p>
          <p>
            Antes de hacer algo o de tomar una decisión importante, considera
            los siguientes consejos:
          </p>
          <ol>
            <li>Respira profundamente</li>
            <li>Piensa en las consecuencias de las acciones</li>
            <li>
              Habla con alguien acerca de cómo te sientes para que puedas
              reflexionar lo que te sucede
            </li>
          </ol>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default AutoctrlAdsltIdEjer;
