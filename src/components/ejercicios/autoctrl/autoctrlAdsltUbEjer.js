import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const AutoctrlAdsltUbEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  function setRespuesta(respuesta, idRespuesta) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const autoctrlAdsltUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/autocontrol/autoctrl_adslt_ub_ejer`
    );
    autoctrlAdsltUbEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Qué crees que le sugirió la maestra a Cristian?',
            respuesta,
            id: idRespuesta,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = respuesta;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="secondary">
      <CardHeader tag="h1">Ejercicio autocontrol</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Identificar y manejar nuestras emociones no siempre es una tarea
            fácil, para ello se requiere conocer y practicar estrategias que nos
            ayuden a estar tranquilos para resolver de mejor manera las
            situaciones a las que nos enfrentamos. Por ejemplo, cuando se
            presenta una situación nueva sentimos muchos nervios (ansiedad)
            porque es algo desconocido. Para controlarnos podemos platicarle a
            alguien aquello que nos hace sentir esa emoción, o bien, podemos
            caminar, escuchar música, sonreír, saltar, dibujar o cantar mientras
            tomamos un baño.
          </p>
          <p>Ejemplo:</p>
          <p>
            Cristian tiene 16 años y le gusta jugar Futbol. Cada mañana acude a
            la escuela y por la tarde ayuda a su tío en el taller de mecánica
            para apoyar a sus papás con los gastos de la casa. Cristian es muy
            estudioso pero en matemáticas no siempre saca buenas calificaciones,
            por lo cual se siente muy asustado, ya que teme reprobar el
            semestre.
          </p>
          <p>
            Un día, mientras escuchaba a su maestra de Psicología hablar sobre
            el estrés, pensó que podría acercarse a ella y pedirle un consejo.
            La maestra le explicó que estar asustado le impedía concentrarse y
            resolver las actividades de clase, era como si el miedo bloqueara su
            capacidad para encontrar soluciones y recordar todo lo aprendido
          </p>
          <p>A partir de esta reflexión, elige una respuesta:</p>
          <p>¿Qué crees que le sugirió la maestra a Cristian?</p>

          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="r"
                  onClick={() =>
                    setRespuesta(
                      'Le recomendó que cada vez que estuviera en la clase de matemáticas, respirara profundo y se concentrara, dejando de lado las ideas de reprobar.',
                      'a'
                    )
                  }
                />
              </td>
              <td>
                Le recomendó que cada vez que estuviera en la clase de
                matemáticas, respirara profundo y se concentrara, dejando de
                lado las ideas de reprobar.
              </td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="r"
                  onClick={() =>
                    setRespuesta(
                      'Le dijo que antes del examen, cerrara sus ojos y pensara en cosas  positivas, como su familia, amigos y el futbol, y que después comenzara a realizar los ejercicios del examen.',
                      'b'
                    )
                  }
                />
              </td>
              <td>
                Le dijo que antes del examen, cerrara sus ojos y pensara en
                cosas positivas, como su familia, amigos y el futbol, y que
                después comenzara a realizar los ejercicios del examen.
              </td>
            </tr>
            <tr>
              <td>
                <input
                  name="r"
                  type="radio"
                  onClick={() =>
                    setRespuesta('Que ya no fuera a la escuela.', 'c')
                  }
                />
              </td>
              <td>Que ya no fuera a la escuela.</td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default AutoctrlAdsltUbEjer;
