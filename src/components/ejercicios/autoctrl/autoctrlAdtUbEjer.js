import React, { useState } from 'react';
import {
  Row,
  Card,
  CardBody,
  CardText,
  CardHeader,
  Button,
  Alert,
} from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';
import audioURL from '../audios/autoctrl/AUTOCTRL_ADULTO_UB.mpeg';

import { database } from '../../../helpers/Firebase';

const AutoctrlAdtUbEjer = () => {
  const [audio] = useState(new Audio(audioURL));
  const [isPlaying, setIsPlaying] = useState(false);
  const playPause = () => {
    if (isPlaying) {
      audio.pause();
    } else {
      audio.play();
    }
    setIsPlaying(!isPlaying);
  };
  const [ejercicio, setEjercicio] = useState('');
  const [ayuda, setAyuda] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta1: '',
    respuesta2: '',
  });

  const handleChange = (event) => {
    if (event.target.name === 'ejercicio') {
      setEjercicio(event.target.value);
    }
    if (event.target.name === 'ayuda') {
      setAyuda(event.target.value);
    }
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const autoctrlAdtUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/autocontrol/autoctrl_adt_ub_ejer`
    );
    autoctrlAdtUbEjer.update(
      {
        respuestas: [
          { pregunta: '¿Cómo te fue con el ejercicio?', respuesta: ejercicio },
          { pregunta: '¿En qué te ha sido de ayuda?', respuesta: ayuda },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.respuesta1 = false;
        estado.respuesta2 = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.respuesta1 = ejercicio;
          estado.respuesta2 = ayuda;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  };

  return (
    <Card body inverse color="secondary">
      <CardHeader tag="h1">Ejercicio autocontrol</CardHeader>
      <CardBody>
        <CardText>
          <p>Reproduce el siguiente audio y contesta las preguntas</p>
          <p>
            <Button onClick={playPause}>
              {isPlaying ? 'Detener' : 'Reproducir'}
            </Button>
          </p>
          <form onSubmit={setRespuesta}>
            <Row>
              <Colxx xxs="4">
                <p>¿Cómo te fue con el ejercicio? </p>
              </Colxx>
              <Colxx xxs="8">
                <textarea
                  name="ejercicio"
                  onChange={handleChange}
                  style={{
                    height: '100%',
                    width: '50%',
                    marginTop: '0.5em',
                  }}
                  rows="5"
                />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="4">
                <p>¿En qué te ha sido de ayuda?</p>
              </Colxx>
              <Colxx xxs="8">
                <textarea
                  type="text"
                  name="ayuda"
                  onChange={handleChange}
                  style={{
                    height: '100%',
                    width: '50%',
                    marginTop: '0.5em',
                  }}
                  rows="5"
                />
              </Colxx>
            </Row>
            <Row>
              <Colxx xxs="4">
                <input type="submit" />
              </Colxx>
            </Row>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta1}</p>
                <p>{estado.respuesta2}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default AutoctrlAdtUbEjer;
