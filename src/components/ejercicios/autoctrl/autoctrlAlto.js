import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const AutoctrlAlto = () => {
  return (
    <Card body inverse color="secondary">
      <CardHeader tag="h1">Autocontrol</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: ALTO</CardTitle>
        <CardText>
          <p>
            Felicitaciones, tu puntaje en Autocontrol indica que tu capacidad
            para expresar emociones y contener tus impulsos es alta, lo cual es
            bueno ya que te ayuda a manejar con cortesía, tranquilidad y calma
            diferentes situaciones difíciles. Sigue así.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default AutoctrlAlto;
