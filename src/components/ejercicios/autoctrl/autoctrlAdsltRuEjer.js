import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Table, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const AutoctrlAdsltRuEjer = () => {
  const [fields, setFields] = useState({ rojo: '', verde: '', amarillo: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    amarillo: false,
    rojo: false,
    verde: false,
  });
  const handleChange = (event) => {
    fields[event.target.name] = event.target.value;
    setFields(fields);
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const autoctrlNiRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/autocontrol/autoctrl_adslt_ru_ejer`
    );
    autoctrlNiRuEjer.update(
      {
        respuestas: [
          {
            pregunta: 'Rojo: detente y piensa ¿Qué no te ha gustado?',
            respuesta: fields.rojo,
          },
          {
            pregunta:
              'Amarillo: Reflexiona ¿Qué es lo que puedo hacer, funcionará?',
            respuesta: fields.amarillo,
          },
          {
            pregunta: 'Verde: Actúa Elegir una de las alternativas',
            respuesta: fields.verde,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.amarillo = false;
        estado.verde = false;
        estado.rojo = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.verde = fields.verde;
          estado.amarillo = fields.amarillo;
          estado.rojo = fields.rojo;
          setEstado(estado);
        }
      }
    );
  };
  return (
    <Card body inverse color="secondary">
      <CardHeader tag="h1">Ejercicio autocontrol</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Cuando una situación no resulta como nosotros lo esperamos es
            posible que puedas llegar a sentirte enojado, triste, furioso,
            incluso con ganas de actuar en formas en las que normalmente no lo
            harías. Te cuento que para evitar que algo que no queremos suceda,
            existe una técnica que se llama semáforo, con ella podemos darnos
            una idea de cómo actuar en los casos en que requerimos controlarnos.
          </p>
          <p>
            Te la explico, un semáforo inicia con una luz roja a la que le sigue
            una luz amarilla y finalmente una luz verde. Cada una de estas luces
            te indica una forma de actuar en cada momento:
          </p>
          <ol>
            <li>
              Rojo: Identificas que algo no te ha gustado, tú inmediatamente
              respira lenta y profundamente, formula el problema y expresa cómo
              te sientes.
            </li>
            <li>
              Amarillo: ¿Qué es lo que puedo hacer, funcionara? ¿Qué es lo mejor
              que puedo hacer?
            </li>
            <li>Verde: Elige y ejecuta la mejor de las alternativas</li>
          </ol>
          <p>
            Piensa en una situación que te haya hecho enfadar mucho y escribe
            como la podrías haber resuelto mediante la técnica del semáforo
          </p>
          <p>Contesta las preguntas en el siguiente cuadro:</p>
          <p>
            <form onSubmit={setRespuesta}>
              <Table bordered>
                <thead>
                  <tr>
                    <th colSpan="4"> Situación</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td rowSpan="2">
                      <img
                        src="/assets/img/ejercicios/autocontrol/autoctrl_adslt_ru_ejer.png"
                        alt=""
                      />
                    </td>
                    <td>
                      <p>Rojo: detente y piensa ¿Qué no te ha gustado?</p>

                      <textarea
                        type="text"
                        name="rojo"
                        onChange={handleChange}
                        style={{ height: '100%', width: '100%' }}
                        rows="5"
                      />
                    </td>
                    <td>
                      <p>
                        Amarillo: Reflexiona ¿Qué es lo que puedo hacer,
                        funcionará?
                      </p>

                      <textarea
                        type="text"
                        name="amarillo"
                        onChange={handleChange}
                        style={{ height: '100%', width: '100%' }}
                        rows="5"
                      />
                    </td>
                    <td>
                      <p>Verde: Actúa Elegir una de las alternativas</p>
                      <textarea
                        type="text"
                        name="verde"
                        onChange={handleChange}
                        style={{ height: '100%', width: '100%' }}
                        rows="5"
                      />
                    </td>
                  </tr>
                  <tr>
                    <td colSpan="4">
                      <input type="submit" />
                    </td>
                  </tr>
                </tbody>
              </Table>
            </form>
          </p>

          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.rojo}</p>
                <p>{estado.amarillo}</p>
                <p>{estado.verde}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default AutoctrlAdsltRuEjer;
