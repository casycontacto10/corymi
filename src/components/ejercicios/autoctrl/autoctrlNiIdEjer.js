import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const AutoctrlNiIdEjer = () => {
  const [fields, setFields] = useState({ animalito: '', sentiste: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta1: '',
    respuesta2: '',
  });
  const handleChange = (event) => {
    fields[event.target.name] = event.target.value;
    setFields(fields);
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const autoctrlNiIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/autocontrol/autoctrl_ni_id_ejer`
    );
    autoctrlNiIdEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Cuál animalito elegiste?',
            respuesta: fields.animalito,
          },
          {
            pregunta: '¿Cómo te sentiste realizando la actividad?',
            respuesta: fields.sentiste,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.respuesta1 = fields.animalito;
        estado.respuesta2 = fields.sentiste;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  };

  return (
    <Card body inverse color="secondary">
      <CardHeader tag="h1">Ejercicio autocontrol</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Cuando estoy jugando en la calle con mis amigos, me molesta que mi
            mamá diga que se terminó el tiempo de jugar que tengo que regresar
            casa. Así que me voy molesto, cuando llego lo que hago es que me voy
            a mi cuarto y comienzo a contar hasta 10 e Inhalo y exhalo hasta que
            me siento mejor. Otro ejercicio que puedo realizar para
            tranquilizarme es realizando un dibujo.
          </p>
          <p>
            De las siguientes opciones, selecciona 2 y dibújalos en una hoja.
          </p>
          <ul>
            <li>Vaca</li>
            <li>Perro</li>
            <li>Gato</li>
            <li>Borrego</li>
          </ul>
          <p>Contesta las siguientes preguntas:</p>

          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <p>¿Cuál animalito elegiste?</p>{' '}
                </td>
                <td>
                  <textarea
                    type="text"
                    name="animalito"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%', marginTop: '1em' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <p>¿Cómo te sentiste realizando la actividad?</p>
                </td>
                <td>
                  <textarea
                    type="text"
                    name="sentiste"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%', marginTop: '1em' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta1}</p>
                <p>{estado.respuesta2}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert>
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default AutoctrlNiIdEjer;
