import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const SolApoyoAdtRuEjer = () => {
  const [respuestas, setRespuestas] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta: '',
  });
  const handleChange = (event) => {
    setRespuestas(event.target.value);
  };
  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const solApoyoAdtRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/solAyuda/sol_apoyo_adt_ru_ejer`
    );
    solApoyoAdtRuEjer.update(
      {
        respuestas: [
          {
            pregunta:
              'Escribe una situación en la que no te hayas sentido capaz de pedir ayuda y por que',
            respuesta: respuestas,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.respuesta = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.respuesta = respuestas;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse style={{ backgroundColor: '#e69500' }}>
      <CardHeader tag="h1">Ejercicio dificultades para pedir ayuda</CardHeader>

      <CardBody>
        <CardText>
          <p>
            Carmen es una joven que tuvo que salir de su pueblo para trabajar en
            la ciudad y poder apoyar a sus papás, encontró trabajo haciendo
            limpieza en un local, al poco tiempo se sintió incómoda, ya que uno
            de sus compañeros comenzó a tener malas intenciones con ella, la
            acosaba la mayoría del tiempo y una noche la siguió hasta la casa
            que rentaba para intentar obligarla a salir con él.
          </p>
          <p>
            Carmen se asustó mucho ya que estaba sola en la ciudad y pensó que
            no tenía a quien pedirle ayuda, tampoco lo comentó con su jefe
            porque tenía mucho miedo de que no le creyeran. Si estás pasando por
            una situación similar a la de Carmen o tienes algún otro tipo de
            problema es importante recordar los siguiente:
          </p>

          <ul>
            <li>Piensa que siempre habrá alguien dispuesto a ayudarte </li>
            <li>
              Es más importante tu bienestar mental y físico que no pedir ayuda
              por pena o miedo
            </li>
            <li>Pedir ayuda no te hace una persona débil o incapaz</li>
            <li>
              Pedir ayuda te hace reconocer que no eres perfecto y no tienes la
              obligación de poder con todo
            </li>
          </ul>

          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  Escribe una situación en la que no te hayas sentido capaz de
                  pedir ayuda y por que.
                </td>
              </tr>
              <tr>
                <td>
                  <textarea
                    name="respuestas"
                    onChange={handleChange}
                    style={{
                      height: '100%',
                      width: '100%',
                      marginTop: '0.5em',
                    }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default SolApoyoAdtRuEjer;
