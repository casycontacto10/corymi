import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const SolApoyoAdsltUbEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  function handleChange(event) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const solApoyoNiUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/solAyuda/sol_apoyo_adslt_ub_ejer`
    );
    solApoyoNiUbEjer.update(
      {
        respuestas: [
          {
            pregunta:
              'En tu casa han tenido algunas dificultades económicas, y el dinero que tenías para comprar ese material, se lo diste a tus padres para que compraran el medicamento… ¿Qué harías?',
            respuesta: event.target.value,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = event.target.value;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse style={{ backgroundColor: '#e69500' }}>
      <CardHeader tag="h1">Ejercicio dificultades para pedir ayuda</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Hay situaciones, que escapan de nuestro control, y existen personas
            que sin recibir nada a cambio, están dispuestos apoyarnos, solo es
            necesario quitarnos nuestros miedos y vergüenzas, y ser capaces de
            hablar de aquellas cosas que nos están pasando.
          </p>
          <p>
            A continuación, se muestra una problemática, selecciona con la que
            más te identifiques:
          </p>

          <p>
            El otro día, tuviste una discusión fuerte con tu mejor amigo o
            amiga, ya que, se te olvido llevar un material que necesitaban para
            hacer un proyecto importante de la escuela. Pero en realidad, no se
            te olvido, sino que un día antes tu abuelita se enfermó y necesitaba
            un medicamento de manera urgente.
          </p>
          <p>
            En tu casa han tenido algunas dificultades económicas, y el dinero
            que tenías para comprar ese material, se lo diste a tus padres para
            que compraran el medicamento… ¿Qué harías?
          </p>
          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="opc"
                  value="Hablar con tu profesora y comentarle tu situación"
                  onChange={handleChange}
                />
              </td>
              <td>Hablar con tu profesora y comentarle tu situación</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="opc"
                  value="Hablar con tu mejor amigo(a) y contarle la verdad, para que entre ambos busquen una solución"
                  onChange={handleChange}
                />
              </td>
              <td>
                Hablar con tu mejor amigo(a) y contarle la verdad, para que
                entre ambos busquen una solución
              </td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="opc"
                  value="Quedarte callado(a), y solo hablar con la profesora y decirle que es tu culpa no tener el material completo"
                  onChange={handleChange}
                />
              </td>
              <td>
                Quedarte callado(a), y solo hablar con la profesora y decirle
                que es tu culpa no tener el material completo
              </td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default SolApoyoAdsltUbEjer;
