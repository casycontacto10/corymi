import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const SolApoyoNiUbEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  function handleChange(event) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const solApoyoNiUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/solAyuda/sol_apoyo_ni_ub_ejer`
    );
    solApoyoNiUbEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Qué hubieras hecho en el caso de Juan?',
            respuesta: event.target.value,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = event.target.value;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse style={{ backgroundColor: '#e69500' }}>
      <CardHeader tag="h1">Ejercicio dificultades para pedir ayuda</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Un día Juan iba caminando por el parque, él estaba muy entretenido
            jugando en su celular. Sin darse cuenta, piso un charco de lodo, por
            lo que resbaló y cayó al suelo. Muy adolorido, comenzó a llorar con
            gran fuerza.
          </p>
          <p>
            En ese momento, pasaban un grupo de compañeros del colegio, se
            acercaron apresuradamente, para intentar ayudar a Juan. Pero él, al
            ver que se acercaban, sintió muchísima vergüenza, así que, se
            levantó rápidamente, como si nada pasara y comenzó a caminar junto
            con ellos. Pero al levantarse, sintió gran dolor en su tobillo. A
            medida que caminaba, más le dolía, sus amigos notaron que caminaba
            con gran dificultad, pero Juan seguía diciendo que todo estaba bien.
            Al llegar a su casa, su madre lo regaño, ya que llego lleno de lodo,
            y decidió no decirle nada a ella. En la madrugada, Juan comenzó a
            tener fiebre y mucho dolor en su tobillo, su mamá, preocupada, lo
            llevo al hospital. En el hospital le comentaron que tenía un hueso
            roto.
          </p>

          <p>Contesta la siguiente pregunta eligiendo una opción</p>
          <p>¿Qué hubieras hecho en el caso de Juan?</p>
          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="opc"
                  value="Te quedarías callado"
                  onChange={handleChange}
                />
              </td>
              <td>Te quedarías callado</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="opc"
                  value="Pedir ayuda a tus amigos"
                  onChange={handleChange}
                />
              </td>
              <td>Pedir ayuda a tus amigos</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="opc"
                  value="Pedir ayuda a tu mamá"
                  onChange={handleChange}
                />
              </td>
              <td>Pedir ayuda a tu mamá</td>
            </tr>
          </table>
          <br />
          {estado.exito ? (
            <Alert>
              <p>{estado.resp}</p>
              <p>Registrado correctamente</p>
            </Alert>
          ) : null}
          {estado.error ? (
            <Alert color="warning">
              <p>Error al guardar</p>
            </Alert>
          ) : null}
          <br />
          <p>Reflexión final…</p>
          <p>
            Como te puedes dar cuenta, hay momentos en la vida que se salen
            fuera de tú control, pero tienes personas lindas a tu alrededor que
            están dispuestas ayudarte cuando lo necesitas, solo es cuestión que
            te sacudas la pena.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default SolApoyoNiUbEjer;
