import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const SolApoyoNiRuEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  const [respuestas, setRespuestas] = useState('');
  const handleChange = (event) => {
    setRespuestas(event.target.value);
  };
  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const solApoyoNiRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/solAyuda/sol_apoyo_ni_ru_ejer`
    );
    solApoyoNiRuEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿a quién le pedirías ayuda y por qué a esa persona?',
            respuesta: respuestas,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.resp = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.resp = respuestas;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse style={{ backgroundColor: '#e69500' }}>
      <CardHeader tag="h1">Ejercicio dificultades para pedir ayuda</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Anita es una hormiga muy trabajadora, vive con todas sus hermanas en
            un hormiguero en medio de un campo verde muy hermoso, todos los días
            Anita sale muy temprano con las demás hormigas a buscar hojitas
            verdes haciendo una fila muy grande, un día, al regresar al
            hormiguero comenzó a correr un aire muy fuerte y Anita se aferró
            tanto a su hojita que el viento la arrastró, sintió mucho miedo en
            ese momento por lo que comenzó a gritar con todas sus fuerzas
            pidiendo ayuda, hasta que una de sus hermanas la escuchó,
            rápidamente aviso a las demás y entre todas juntas tomadas de las
            manos alcanzaron a Anita y lograron rescatarla. Anita se sintió muy
            aliviada y muy contenta ya que gracias a la ayuda de sus hermanas
            regreso sana y salva.
          </p>
          <p>
            Cuando estamos en una situación peligrosa o difícil es importante
            pedir ayuda, aunque sintamos pena o miedo, si pedimos ayuda siempre
            habrá alguien dispuesto a tendernos la mano.
          </p>
          <p>Contesta la siguiente pregunta:</p>

          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  Si estuvieras en una situación de peligro como Anita ¿a quién
                  le pedirías ayuda y por qué a esa persona?
                </td>
              </tr>
              <tr>
                <td>
                  <textarea
                    name="respuestas"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%', marginTop: '1em' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default SolApoyoNiRuEjer;
