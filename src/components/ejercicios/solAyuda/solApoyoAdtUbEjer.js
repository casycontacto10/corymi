import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const SolApoyoAdtUbEjer = () => {
  const [respuestas, setRespuestas] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta: false,
  });
  const handleChange = (event) => {
    setRespuestas(event.target.value);
  };
  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const rApoyoNiIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/solAyuda/sol_apoyo_adt_ub_ejer`
    );
    rApoyoNiIdEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Tú qué opinas de buscar apoyo psicológico?',
            respuesta: respuestas,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.respuesta = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.respuesta = respuestas;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse style={{ backgroundColor: '#e69500' }}>
      <CardHeader tag="h1">Ejercicio dificultades para pedir ayuda</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Tener personas de confianza es necesario porque cuando lo
            necesitamos pueden ayudarnos a afrontar situaciones difíciles a
            través de su compañía, ayuda física, emocional y/o psicológica.
          </p>
          <p>Ejemplo:</p>

          <p>
            Katia tiene 3 hijos y desde hace dos años siente que la
            responsabilidad de cuidarlos se ha incrementado mucho, pues su
            esposo Miguel se fue a trabajar a los campos de cultivo que están en
            Baja California, México. Katia señala sentirse muy triste porque,
            aunque no viven en distintos países, Miguel sólo los visita una vez
            al mes.
          </p>
          <p>
            Katia sufría constantes dolores de cabeza, ya que los niños solían
            portarse mal en respuesta a la ausencia de su padre. Sin embargo,
            desde hace tres meses distintas unidades de salud están acudiendo a
            la comunidad y entre los servicios que se ofrecen se encuentra el
            apoyo psicológico. Al respecto, Katia decidió acudir y llevar a los
            niños en busca de ayuda también. Ahora, ella señala que existe una
            significativa mejoría en su estado de ánimo y en el de los niños, ya
            que han dejado de juzgar y culpar a Miguel, valorando su valentía de
            salir del pueblo en busca de un mejor empleo que les ayude a salir
            adelante.
          </p>
          <p>
            Recuerda que todos necesitamos a alguien para platicar acerca de lo
            que nos sucede, alguien que nos escuche y oriente cuando sea
            necesario, y la labor de un psicólogo es justo esa.
          </p>
          <p>Contesta la siguiente pregunta:</p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>¿Tú qué opinas de buscar apoyo psicológico?</td>
              </tr>
              <tr>
                <td>
                  <textarea
                    name="respuestas"
                    onChange={handleChange}
                    style={{
                      height: '100%',
                      width: '100%',
                      marginTop: '0.5em',
                    }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta}</p>
                <p>Actualizado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert>
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default SolApoyoAdtUbEjer;
