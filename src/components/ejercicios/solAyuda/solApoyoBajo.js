import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const SolApoyoBajo = () => {
  return (
    <Card body inverse style={{ backgroundColor: '#e69500' }}>
      <CardHeader tag="h1">Dificultades para pedir ayuda</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: ALTO</CardTitle>
        <CardText>
          <p>
            Felicitaciones, tu puntaje en Capacidad para pedir ayuda indica que,
            ante una situación difícil, tienes la facilidad para pedir apoyo a
            las personas de confianza que se encuentran a tu alrededor en Sigue
            así.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default SolApoyoBajo;
