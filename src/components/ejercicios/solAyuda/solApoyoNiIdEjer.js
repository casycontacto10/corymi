import React, { useState } from 'react';
import {
  Card,
  CardBody,
  CardText,
  CardHeader,
  Button,
  Alert,
} from 'reactstrap';
import audioURL from '../audios/solApoyo/SOL_APOYO_NI_ID.mpeg';

import { database } from '../../../helpers/Firebase';

const SolApoyoNiIdEjer = () => {
  const [audio] = useState(new Audio(audioURL));
  const [isPlaying, setIsPlaying] = useState(false);
  const playPause = () => {
    if (isPlaying) {
      audio.pause();
    } else {
      audio.play();
    }
    setIsPlaying(!isPlaying);
  };
  const [respuestas, setRespuestas] = useState({ p1: '', p2: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    resp2: false,
  });
  const handleChange = (event) => {
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
  };
  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const solApoyoNiIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/solAyuda/sol_apoyo_ni_id_ejer`
    );
    solApoyoNiIdEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Te gusto el ejercicio?',
            respuesta: respuestas.p1,
          },
          {
            pregunta:
              '¿Te servirá la próxima vez que para recordar que es bueno pedir ayuda?',
            respuesta: respuestas.p2,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.resp1 = false;
        estado.resp2 = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.resp1 = respuestas.p1;
          estado.resp2 = respuestas.p2;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse style={{ backgroundColor: '#e69500' }}>
      <CardHeader tag="h1">Ejercicio reflexión</CardHeader>
      <CardBody>
        <CardText>
          <p>Reproduce el siguiente audio y contesta las preguntas:</p>
          <p>
            <Button onClick={playPause}>
              {isPlaying ? 'Detener' : 'Reproducir'}
            </Button>
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>¿Te gusto el ejercicio? </td>
                <td>
                  <textarea
                    name="p1"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%', marginTop: '1em' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  ¿Te servirá la próxima vez que para recordar que es bueno
                  pedir ayuda?
                </td>
                <td>
                  <textarea
                    name="p2"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%', marginTop: '1em' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.resp2}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default SolApoyoNiIdEjer;
