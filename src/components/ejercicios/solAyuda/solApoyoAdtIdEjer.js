import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const SolApoyoAdtIdEjer = () => {
  const [preguntas, setPreguntas] = useState({
    p1: '',
    p2: '',
    otro: '',
    disable: true,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta1: '',
    respuesta2: '',
    respuestaO: '',
  });
  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
    if (event.target.name === 'p2' && event.target.value !== 'otro') {
      preguntas.otro = '';
      preguntas.disable = true;
    }
    if (event.target.value === 'otro') {
      preguntas.disable = false;
    }
  };

  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const rApoyoAdtIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/solAyuda/sol_apoyo_adt_id_ejer`
    );
    rApoyoAdtIdEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Cómo podría convencer Rosa a sus hermanas para que le ayuden?',
            respuesta: preguntas.p1,
          },
          {
            pregunta: '¿Tú cómo pides ayuda cuando la necesitas?',
            respuesta: preguntas.p2,
            otro_texto: preguntas.otro,
            otro: !preguntas.disable,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.respuesta1 = false;
        estado.respuesta2 = false;
        estado.respuestaO = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.respuesta1 = preguntas.p1;
          estado.respuesta2 = preguntas.p2;
          estado.respuestaO = preguntas.otro;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse style={{ backgroundColor: '#e69500' }}>
      <CardHeader tag="h1">Ejercicio dificultades para pedir ayuda</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta las preguntas de reflexión:</p>
          <p>
            En ocasiones, puede ser difícil pedirle ayuda a nuestra familia o
            amigos porque no sabemos cómo hacerlo o en otros momentos no han
            podido ayudarnos. Ese es el caso de Rosa, ella es madre soltera y no
            ha conseguido trabajo porque no tiene quien le ayude a cuidar a su
            bebé y en ningún trabajo aceptan que lo lleve con ella.
          </p>
          <p>
            En otras ocasiones, le ha pedido a sus hermanas que le ayuden a
            cuidarlo pero siempre le dicen que están muy ocupadas, así que ya no
            les pide el favor, aunque sabe que en este momento realmente
            necesita de su ayuda.
          </p>
          <p>¿Cómo podría convencer Rosa a sus hermanas para que le ayuden?</p>

          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="Explicándoles que para ella es importante trabajar"
                  onChange={handleChange}
                />
              </td>
              <td>Explicándoles que para ella es importante trabajar</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="Haciendo un favor que ellas le pidan"
                  onChange={handleChange}
                />
              </td>
              <td>Haciendo un favor que ellas le pidan</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="Diciéndoles que solo será por un corto tiempo"
                  onChange={handleChange}
                />
              </td>
              <td>Diciéndoles que solo será por un corto tiempo</td>
            </tr>
          </table>
          <br />
          <p>¿Tú cómo pides ayuda cuando la necesitas?</p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p2"
                    value="Busco a una persona de confianza"
                    onChange={handleChange}
                  />
                </td>
                <td>Busco a una persona de confianza</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p2"
                    value="No pido ayuda"
                    onChange={handleChange}
                  />
                </td>
                <td>No pido ayuda</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p2"
                    value="Le pido ayuda a quien yo he ayudado antes"
                    onChange={handleChange}
                  />
                </td>
                <td>Le pido ayuda a quien yo he ayudado antes</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p2"
                    value="otro"
                    onChange={handleChange}
                  />
                </td>
                <td>Otro</td>
                <td>
                  <textarea
                    disabled={preguntas.disable}
                    name="otro"
                    value={preguntas.otro}
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta1}</p>
                <p>{estado.respuesta2}</p>
                <p>{estado.respuestaO}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
          <p>
            Recuerda que no debemos suponer que los demás no tienen tiempo para
            ayudarnos, a veces basta con explicarles lo que nos sucede, para que
            nos ayuden.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default SolApoyoAdtIdEjer;
