import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const SolApoyoAlto = () => {
  return (
    <Card body inverse style={{ backgroundColor: '#e69500' }}>
      <CardHeader tag="h1">Dificultades para pedir ayuda</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: ALTO</CardTitle>
        <CardText>
          <p>
            Tu puntaje en Capacidad para pedir apoyo indica que para ti es
            difícil solicitar ayuda a las personas de confianza que se
            encuentran a tu alrededor, aún y cuando tengas un grupo de personas
            alrededor tuyo que pudieran brindarte ayuda. Y es que te cuesta
            trabajo acercarte a ellas para contarles lo que te pasa y pedir su
            apoyo. Quizá no estás acostumbrado a ello, o te da pena expresarlo,
            o no quieres mostrar aquello que para ti puede ser señal de
            debilidad o de dolor.
          </p>
          <p>
            A continuación, te presentamos un ejercicio que te ayudará a que sea
            más sencillo para ti pedir ayuda cuando lo necesites.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default SolApoyoAlto;
