import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Table, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const SolApoyoAdsltRuEjer = () => {
  const [respuestas, setRespuestas] = useState({
    p1: '',
    p2: '',
    p3: '',
    p4: '',
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    p1: false,
    p2: false,
    p3: false,
    p4: false,
  });
  const handleChange = (event) => {
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
  };
  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const solApoyoAdsltRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/solAyuda/sol_apoyo_adslt_ru_ejer`
    );
    solApoyoAdsltRuEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Qué quiero pedir?',
            respuesta: respuestas.p1,
          },
          {
            pregunta: '¿Qué quiero conseguir?',
            respuesta: respuestas.p2,
          },
          {
            pregunta: '¿A quién se lo pude pedir?',
            respuesta: respuestas.p3,
          },
          {
            pregunta: 'Inventa un posible dialogo de la petición que pensaste',
            respuesta: respuestas.p4,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.p1 = false;
        estado.p2 = false;
        estado.p3 = false;
        estado.p4 = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.p1 = respuestas.p1;
          estado.p2 = respuestas.p2;
          estado.p3 = respuestas.p3;
          estado.p4 = respuestas.p4;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse style={{ backgroundColor: '#e69500' }}>
      <CardHeader tag="h1">Ejercicio dificultades para pedir ayuda</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Te cuento que hacer peticiones nos es una habilidad que todas las
            personas tengan, no, es algo que se trabaja y se adquiere. La
            habilidad de pedir ayuda no resulta fácil para todos es por ello que
            aquí te presento algunos pasos que puedes considerar al momento de
            pedir ayuda o apoyo de alguien y que te resulte más fácil hacerlo.
          </p>
          <p>Cuando realizamos una petición:</p>
          <ol>
            <li>
              Es conveniente analizar la situación ¿Qué ha sucedido que necesito
              ayuda?
            </li>
            <li>
              Tener un objetivo en mente sobre lo que vamos a pedir ¿qué es lo
              que necesitamos?
            </li>
            <li>
              Ser claros, concretos y directos, es decir pensar previamente en
              lo que queremos pedir
            </li>
            <li>
              No hacer prejuicios acerca de nuestra petición - Todas las
              peticiones de ayuda son válidas si para ti es importante
            </li>
            <li>
              Y la más importante no tengas miedo de pedir ayuda, al principio
              sentiremos que quizás no vale la pena, o que no le importa a los
              demás, quizás que no vale la pena o que no seremos escuchados,
              pero cuando lo intentas te darás cuenta lo fácil que puede ser
            </li>
          </ol>
          <p>
            Para ponerlo en práctica, te invito a que pienses en una situación
            en la quizás habrías querido pedir ayuda y que llenes la tabla que a
            continuación se te presenta:
          </p>
          <form onSubmit={setRespuesta}>
            <Table bordered>
              <thead>
                <tr>
                  <td>¿Qué quiero pedir?</td>
                  <td>¿Qué quiero conseguir?</td>
                  <td>¿A quién se lo pude pedir?</td>
                  <td>
                    Inventa un posible dialogo de la petición que pensaste
                  </td>
                </tr>
                <tr>
                  <td>
                    <textarea
                      name="p1"
                      onChange={handleChange}
                      style={{ height: '100%', width: '100%' }}
                      rows="5"
                    />
                  </td>
                  <td>
                    <textarea
                      name="p2"
                      onChange={handleChange}
                      style={{ height: '100%', width: '100%' }}
                      rows="5"
                    />
                  </td>
                  <td>
                    <textarea
                      name="p3"
                      onChange={handleChange}
                      style={{ height: '100%', width: '100%' }}
                      rows="5"
                    />
                  </td>
                  <td>
                    <textarea
                      name="p4"
                      onChange={handleChange}
                      style={{ height: '100%', width: '100%' }}
                      rows="5"
                    />
                  </td>
                </tr>
                <tr>
                  <td colSpan="4">
                    <input type="submit" />
                  </td>
                </tr>
              </thead>
            </Table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.p1}</p>
                <p>{estado.p2}</p>
                <p>{estado.p3}</p>
                <p>{estado.p4}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default SolApoyoAdsltRuEjer;
