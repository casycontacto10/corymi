import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const SolApoyoAdsltIdEjer = () => {
  const [respuestas, setRespuestas] = useState({ p1: '', p2: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    resp2: false,
  });
  function handleChange(event) {
    const id = localStorage.getItem('recursoId');
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
    const refe = database.ref();
    const solApoyoAdsltIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/solAyuda/sol_apoyo_adslt_id_ejer`
    );
    solApoyoAdsltIdEjer.update(
      {
        respuestas: [
          {
            pregunta:
              'Si estuvieras en la situación de Ramón, ¿cómo pedirías ayuda?',
            respuesta: respuestas.p1,
          },
          {
            pregunta:
              'Cuando tú has pedido ayuda en alguna situación, ¿la has recibido?',
            respuesta: respuestas.p2,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.resp2 = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = respuestas.p1;
          estado.resp2 = respuestas.p2;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse style={{ backgroundColor: '#e69500' }}>
      <CardHeader tag="h1">Ejercicio dificultades para pedir ayuda</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta las preguntas de reflexión:</p>
          <p>
            Los compañeros de salón de Ramón se burlan de él porque tartamudea.
            Su mamá se ha dado cuenta de que está muy serio y de que ya no
            quiere ir a la escuela, pero Ramón no le cuenta lo que está pasando
            porque piensa que lo van a regañar.
          </p>
          <p>Si estuvieras en la situación de Ramón, ¿cómo pedirías ayuda?</p>

          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="Le diría la verdad a mis papá"
                  onChange={handleChange}
                />
              </td>
              <td>Le diría la verdad a mis papá</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="Le pediría apoyo a un amigo o amiga"
                  onChange={handleChange}
                />
              </td>
              <td>Le pediría apoyo a un amigo o amiga</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p1"
                  value="Hablaría con mi maestra o maestro"
                  onChange={handleChange}
                />
              </td>
              <td>Hablaría con mi maestra o maestro</td>
            </tr>
          </table>
          <br />
          <p>
            Cuando tú has pedido ayuda en alguna situación, ¿la has recibido?
          </p>

          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p2"
                  value="Sí"
                  onChange={handleChange}
                />
              </td>
              <td>Sí</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p2"
                  value="Algunas veces"
                  onChange={handleChange}
                />
              </td>
              <td>Algunas veces</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="p2"
                  value="No"
                  onChange={handleChange}
                />
              </td>
              <td>No</td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.resp2}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
          <br />
          <p>
            No olvides que es necesario siempre contar lo que nos sucede a
            personas de nuestra confianza, como la familia y los amigos, porque
            ellos pueden ayudarnos cuando lo necesitamos.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default SolApoyoAdsltIdEjer;
