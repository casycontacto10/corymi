import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const TristezaAlto = () => {
  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Dificultades en el manejo de la tristeza</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: ALTO</CardTitle>
        <CardText>
          <p>
            Tu puntaje en el manejo de la tristeza indica que la forma en la que
            reaccionas a situaciones que te son difíciles o dolorosas como
            pérdidas, soledad o nostalgia no es buena para ti.
          </p>
          <p>
            Casi siempre te sientes sin ánimo o esperanza porque has llegado a
            pensar que hagas lo que hagas nada tiene solución, por lo que te
            desanimas muy fácilmente y te es muy difícil volver a sentirte bien
            cuando no salen las cosas como esperabas, lo que hace que tu
            tristeza aumente. Por eso necesitas encontrar qué hacer para
            sentirte bien.
          </p>
          <p>
            A continuación, te pedimos que realices un ejercicio que te ayudará
            a identificar este tipo de situaciones y cómo manejarlas de una
            mejor forma.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default TristezaAlto;
