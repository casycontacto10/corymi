import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const TristezaBajo = () => {
  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Resultado recurso manejo de tristeza</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: BAJO</CardTitle>
        <CardText>
          <p>
            Felicitaciones, tu puntaje en el manejo de la tristeza, indica que
            la forma en la que reaccionas a situaciones difíciles o dolorosas,
            como pérdidas, soledad o nostalgia es la correcta para ti. Sigue
            así.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default TristezaBajo;
