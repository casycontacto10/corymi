import React, { useState } from 'react';
import { Row, Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';
import YouTube from 'react-youtube';

import { database } from '../../../helpers/Firebase';

const TristezaNiUbEjer = () => {
  const [respuestas, setRespuestas] = useState({
    p1: '',
    p2: '',
    p3: '',
    p4: '',
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    resp2: false,
    resp3: false,
    resp4: false,
  });
  const handleChange = (event) => {
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const tristezaNiUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/tristeza/tristeza_ni_ub_ejer`
    );
    tristezaNiUbEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Qué le dirías a Bruno para que se siente un poco mejor ante la muerte de su abuelo?',
            respuesta: respuestas.p1,
          },
          {
            pregunta:
              '¿Con quiénes puede platicar Bruno respecto a cómo se siente?',
            respuesta: respuestas.p2,
          },
          {
            pregunta: '¿Con quiénes puedes platicar cuando te sientes triste?',
            respuesta: respuestas.p3,
          },
          {
            pregunta: '¿Qué puedes hacer cuando te sientes triste?',
            respuesta: respuestas.p4,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.resp2 = false;
        estado.resp3 = false;
        estado.resp4 = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = respuestas.p1;
          estado.resp2 = respuestas.p2;
          estado.resp3 = respuestas.p3;
          estado.resp4 = respuestas.p4;
          setEstado(estado);
        }
      }
    );
  };
  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Ejercicio manejo tristeza</CardHeader>
      <CardBody>
        <CardText>
          <p>Reproduce el siguiente video y contesta las preguntas:</p>
          <p>
            <Row>
              <Colxx xxs="12" className="mb-5">
                <YouTube videoId="9VKG-XbkdGM" />
              </Colxx>
            </Row>
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  ¿Qué le dirías a Bruno para que se siente un poco mejor ante
                  la muerte de su abuelo?
                </td>
                <td>
                  <textarea
                    type="text"
                    name="p1"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  ¿Con quiénes puede platicar Bruno respecto a cómo se siente?
                </td>
                <td>
                  <textarea
                    type="text"
                    name="p2"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>¿Con quiénes puedes platicar cuando te sientes triste?</td>
                <td>
                  <textarea
                    type="text"
                    name="p3"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>¿Qué puedes hacer cuando te sientes triste?</td>
                <td>
                  <textarea
                    type="text"
                    name="p4"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          {estado.exito ? (
            <Alert>
              <p>{estado.resp1}</p>
              <p>{estado.resp2}</p>
              <p>{estado.resp3}</p>
              <p>{estado.resp4}</p>
              <p>Registrado correctamente</p>
            </Alert>
          ) : null}
          {estado.error ? (
            <Alert>
              <p>Error al guardar</p>
            </Alert>
          ) : null}
        </CardText>
      </CardBody>
    </Card>
  );
};

export default TristezaNiUbEjer;
