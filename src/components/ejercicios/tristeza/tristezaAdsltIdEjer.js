import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const TristezaAdsltIdEjer = () => {
  const [preguntas, setPreguntas] = useState({
    p1: '',
    otro: '',
    disable: true,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    otro: false,
  });

  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
    if (event.target.name === 'p1' && event.target.value !== 'otro') {
      preguntas.otro = '';
      preguntas.disable = true;
    }
    if (event.target.value === 'otro') {
      preguntas.disable = false;
    }
  };

  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const autoctrlAdtIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/tristeza/tristeza_adslt_id_ejer`
    );
    autoctrlAdtIdEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿a ti qué te ayuda a sentirte mejor cuando estás triste?',
            respuesta: preguntas.p1,
            otro_texto: preguntas.otro,
            otro: !preguntas.disable,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.otro = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = preguntas.p1;
          estado.otro = preguntas.otro;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Ejercicio manejo tristeza</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente cuento y contesta la pregunta de reflexión:</p>
          <p>
            Las luciérnagas que embellecen a los árboles Mi abuelo me contaba
            que antes, cuando llegaban las lluvias de junio, las luciérnagas
            venían junto con ellas.
          </p>
          <p>
            Las luciérnagas alegraban a los árboles y entonces los corazones de
            las personas brillaban también. Esto era durante el día de San
            Pedro.
          </p>
          <p>
            El campanero tocaba fuerte las campanas de la iglesia mientras las
            luciérnagas, guiadas por las antorchas, llegaban a los árboles, y
            con sus lucecitas los alegraban para que dejaran la tristeza y
            dieran sus frutos.
          </p>
          <p>
            Las personas en el pueblo seguían cantando hasta que las antorchas
            se acabaran o las luciérnagas dejaran de brillar.
          </p>
          <p>
            Entonces, la noche se quedaba oscura, pero los árboles ya estaban
            sanados.
          </p>
          <p>
            Ahora, la ausencia de los cantos y los ocotes han dejado tristes a
            los árboles, pero en nosotros está volver a encender las antorchas,
            cantar a San Pedro y brillar en nuestro corazón junto con las
            luciérnagas para alegrar a los árboles.
          </p>
          <p>
            En el cuento, los árboles y las personas que estaban tristes se
            alegraban con la llegada de las luciérnagas.
          </p>
          <p>
            En el cuento, los árboles y las personas que estaban tristes se
            alegraban con la llegada de las luciérnagas, ¿a ti qué te ayuda a
            sentirte mejor cuando estás triste?
          </p>

          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Estar con mi familia"
                    onChange={handleChange}
                  />
                </td>
                <td>Estar con mi familia</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Salir con mis amigos y amigas"
                    onChange={handleChange}
                  />
                </td>
                <td>Salir con mis amigos y amigas</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Hacer una actividad que disfrute"
                    onChange={handleChange}
                  />
                </td>
                <td>Hacer una actividad que disfrute</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="otro"
                    onChange={handleChange}
                  />
                </td>
                <td> Otro</td>
                <td>
                  <textarea
                    disabled={preguntas.disable}
                    type="text"
                    name="otro"
                    value={preguntas.otro}
                    onChange={handleChange}
                    rows="5"
                    style={{ height: '100%', width: '100%' }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.otro}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default TristezaAdsltIdEjer;
