/* eslint-disable no-alert */
/* eslint-disable func-names */
import React, { useState } from 'react';
import {
  Card,
  CardBody,
  CardText,
  CardHeader,
  Button,
  Alert,
} from 'reactstrap';

import { storage, database } from '../../../helpers/Firebase';

const TristezaNiRuEjer = () => {
  const [image, setImage] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });

  const upload = () => {
    const id = localStorage.getItem('recursoId');
    estado.error = false;
    estado.exito = false;
    estado.resp = false;
    setEstado(estado);
    if (!image) {
      estado.error = true;
      estado.resp = 'no hay archivo para guardar';
      setEstado(estado);
    } else {
      storage
        .ref(`/${id}/tristeza/tristeza_ni_ru_ejer/${image.name}`)
        .put(image)
        .on(
          'state_changed',
          function () {
            estado.error = true;
            estado.resp = 'subiendo archivo';
            setEstado(estado);
          },
          function () {
            estado.error = true;
            estado.resp = 'error al subir archivo';
            setEstado(estado);
          },
          function () {
            const refe = database.ref();
            const tristezaNiRuEjer = refe.child(
              `respuestasPersona/${id}/ejercicios/tristeza/tristeza_ni_ru_ejer`
            );
            tristezaNiRuEjer.update(
              {
                archivo: `/${id}/tristeza/tristeza_ni_ru_ejer/${image.name}`,
                diagnostico: true,
              },
              (error) => {
                estado.error = false;
                estado.exito = false;
                estado.resp = false;
                setEstado(estado);
                if (error) {
                  estado.error = true;
                  estado.resp = 'error al guardar';
                  setEstado(estado);
                } else {
                  estado.exito = true;
                  estado.resp = `${image.name} guardado correctamente`;
                  setEstado(estado);
                }
              }
            );
          }
        );
    }
  };
  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Ejercicio manejo tristeza</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Paty es una patita que vive con su mamá y su papá en un estanque muy
            bonito junto a otras familias de patitos, un día Paty se acercó a
            los demás patitos para jugar con ellos, estos patitos no quisieron
            jugar con Paty ya que su plumaje era diferente al de ellos, Paty se
            sintió profundamente triste y regresó llorando a su nido a contarle
            a sus papás, ellos le explicaron que los demás patitos no entendían
            que ella era especial y hermosa aunque tuviera un plumaje más
            colorido y le dijeron que podía hacer varias cosas para dejar de
            sentirse triste.
          </p>
          <p>
            Realiza un dibujo como el de la imagen con las cosas que puedes
            hacer cuando estés triste y pégala en algún lugar de tu casa.
          </p>
          <p>
            <input
              type="file"
              onChange={(e) => {
                setImage(e.target.files[0]);
              }}
            />
          </p>
          <p>
            <Button onClick={upload} color="info">
              Subir
            </Button>
          </p>
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="warning">
                <p>{estado.resp}</p>
              </Alert>
            ) : null}
          </p>
          <p>
            <img
              src="/assets/img/ejercicios/tristeza/tristeza_ni_ru_ejer.png"
              alt=""
            />
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default TristezaNiRuEjer;
