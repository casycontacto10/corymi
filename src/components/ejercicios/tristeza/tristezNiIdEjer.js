import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const TristezaNiIdEjer = () => {
  const [respuestas, setRespuestas] = useState('');

  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });

  const handleChange = (event) => {
    setRespuestas(event.target.value);
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const tristezaNiIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/tristeza/tristeza_ni_id_ejer`
    );
    tristezaNiIdEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Qué harías tu para manejar la tristeza?',
            respuesta: respuestas,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.resp = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.resp = respuestas;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  };
  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Ejercicio manejo tristeza</CardHeader>
      <CardBody>
        <CardText>
          <p>
            En la familia de Miguel se dedican a la pesca, es la tradición que
            desde muy pequeños todos aprenden a pescar, un día Miguel y su papá
            salieron de pesca, Miguel tomo la red y la lanzó muy fuerte para
            poder atrapar muchos peces, pasaron las horas y ya tenían que
            regresar a su casa, Miguel se sintió muy triste ya que no pudo
            pescar nada. Al llegar a casa su familia platicó con Miguel acerca
            de cómo se sentía, le explicaron que a veces pasan cosas que nos
            hacen sentir mal pero que no podemos cambiar como en el caso de la
            pesca y que para sentirse mejor podía hablar de cómo se sentía,
            realizar un dibujo, escuchar su música favorita, salir a dar un
            paseo o llorar todo lo que necesitara.
          </p>
          <p> Contesta la siguiente pregunta: </p>

          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  Imaginemos que somos Miguel, ¿Qué harías tu para manejar la
                  tristeza?
                </td>
              </tr>
              <tr>
                <td>
                  <textarea
                    name="respuestas"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%', marginTop: '1em' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default TristezaNiIdEjer;
