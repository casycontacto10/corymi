import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const TristezaAdsltUbEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  function setRespuesta(respuesta) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const tristezaAdsltUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/tristeza/tristeza_adslt_ub_ejer`
    );
    tristezaAdsltUbEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Cómo crees que se sintió Jimena al volver a practicar sus clases de piano?',
            respuesta,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = respuesta;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Ejercicio manejo tristeza</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Sentimos tristeza cuando alguien que queremos mucho se va a vivir a
            otro lugar, cuando perdemos algo que nos gusta o cuando extrañamos
            salir de casa. Muchas veces queremos llorar, lo cual es bueno porque
            nos ayuda a pensar y a reflexionar sobre lo que está a nuestro
            alrededor. Sin embargo, no podemos estar tristes siempre porque
            corremos el riesgo de olvidar lo que aún tenemos. Por esa razón, es
            importante reflexionar sobre lo sentimos y cómo lo vamos a afrontar.
            :
          </p>
          <p>Ejemplo:</p>
          <p>
            Jimena tiene 15 años y antes de la pandemia de COVID-19 asistía cada
            jueves a clase de música en la orquesta de la escuela. Jimena era
            muy feliz porque al tocar el piano su tensión y estrés de la escuela
            disminuían. Sin embargo, cuando se limitaron las actividades de
            convivencia debido a la pandemia, Jimena y sus compañeros dejaron de
            verse, y eso la entristeció muchísimo, porque ella no tenía un piano
            en casa y eso impedía que siguiera practicando. Entonces, en casa
            sus papás notaron que estaba muy triste, pero no tenían dinero para
            comprar un piano. Después de algunos meses, al papá de Jimena se le
            ocurrió hablar con el profesor de música de la escuela para pedirle
            prestado el piano. Al principio el profesor lo dudó, porque temía
            que dañaran el instrumento, pero Jimena se comprometió a cuidarlo.
            Entonces el profesor accedió y Jimena volvió a tocar el piano.
          </p>
          <p>
            ¿Cómo crees que se sintió Jimena al volver a practicar sus clases de
            piano? Elije una opción
          </p>

          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="r"
                  onClick={() =>
                    setRespuesta(
                      'Contenta porque además de tocar el piano, pudo manejar su estrés'
                    )
                  }
                />
              </td>
              <td>
                Contenta porque además de tocar el piano, pudo manejar su estrés
              </td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="r"
                  onClick={() => setRespuesta('Enojada con el profesor')}
                />
              </td>
              <td>Enojada con el profesor</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="r"
                  onClick={() => setRespuesta('Triste')}
                />
              </td>
              <td>Triste</td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default TristezaAdsltUbEjer;
