import React, { useState } from 'react';
import {
  Card,
  CardBody,
  CardText,
  CardHeader,
  Button,
  Alert,
} from 'reactstrap';
import audioURL from '../audios/tristeza/TRISTEZA_ADULTO_RU.mpeg';

import { database } from '../../../helpers/Firebase';

const TristezaAdtRuEjer = () => {
  const [audio] = useState(new Audio(audioURL));
  const [isPlaying, setIsPlaying] = useState(false);
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta: '',
  });
  const playPause = () => {
    if (isPlaying) {
      audio.pause();
    } else {
      audio.play();
    }
    setIsPlaying(!isPlaying);
  };
  const [respuestas, setRespuestas] = useState('');

  const handleChange = (event) => {
    setRespuestas(event.target.value);
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const tristezaAdtRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/tristeza/tristeza_adt_ru_ejer`
    );
    tristezaAdtRuEjer.update(
      {
        respuestas: [
          {
            pregunta:
              'Cuéntame ¿cómo te has sentido realizando este ejercicio?',
            respuesta: respuestas,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.respuesta = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.respuesta = respuestas;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  };
  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Ejercicio manejo tristeza</CardHeader>
      <CardBody>
        <CardText>
          <p>Reproduce el siguiente audio y contesta la pregunta</p>
          <p>
            <Button onClick={playPause}>
              {isPlaying ? 'Detener' : 'Reproducir'}
            </Button>
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  Cuéntame ¿cómo te has sentido realizando este ejercicio?
                </td>
              </tr>
              <tr>
                <td>
                  <textarea
                    name="respuestas"
                    onChange={handleChange}
                    style={{
                      height: '100%',
                      width: '100%',
                      marginTop: '0.5em',
                    }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default TristezaAdtRuEjer;
