import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const TristezaMedio = () => {
  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Dificultades en el manejo de la tristeza</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: MEDIO</CardTitle>
        <CardText>
          <p>
            Tu puntaje en el manejo de la tristeza indica que en ocasiones la
            forma en la que reaccionas a situaciones difíciles o dolorosas, como
            pérdidas, soledad o nostalgia, necesita cambiar para que logres
            sentirte mejor.{' '}
          </p>
          <p>
            Tu estado de ánimo por lo general no es ni bueno, ni malo. A veces
            te sientes motivado y con ganas de hacer las cosas y en otras no,
            sobre todo cuando no salen las cosas como tú esperabas, o cuando
            recibes noticias difíciles.{' '}
          </p>
          <p>
            A continuación, te pedimos que realices un ejercicio que te ayudará
            a identificar este tipo de situaciones y cómo manejarlas de una
            mejor forma.{' '}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default TristezaMedio;
