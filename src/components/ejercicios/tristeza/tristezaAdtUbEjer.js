import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const TristezaAdtUbEjer = () => {
  const [respuestas, setRespuestas] = useState({ p1: '' });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta: '',
  });
  const handleChange = (event) => {
    respuestas[event.target.name] = event.target.value;
    setRespuestas(respuestas);
  };

  const setRespuesta = (event) => {
    event.preventDefault();
    const id = localStorage.getItem('recursoId');

    const refe = database.ref();
    const tristezaAdtUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/tristeza/tristeza_adt_ub_ejer`
    );
    tristezaAdtUbEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿Cómo te sentiste después de realizar el ejercicio?',
            respuesta: respuestas.p1,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.respuesta = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.respuesta = respuestas.p1;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  };

  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Ejercicio manejo tristeza</CardHeader>
      <CardBody>
        <CardText>
          <p>
            -Sentimos tristeza cuando alguien que queremos mucho se va a vivir a
            otro lugar, cuando perdemos algo que nos gusta o cuando extrañamos
            salir de casa. Muchas veces queremos llorar, lo cual es bueno porque
            nos ayuda a pensar y a reflexionar sobre lo que está a nuestro
            alrededor. Sin embargo, no podemos estar tristes siempre porque
            corremos el riesgo de olvidar lo que aún tenemos. Por esa razón, es
            importante reflexionar sobre lo sentimos y cómo lo vamos a afrontar.
          </p>
          <p>
            A continuación, te presento un ejercicio para manejar la tristeza:
          </p>
          <p>
            Toma una hoja de papel, un lápiz y siéntate en un lugar cómodo.
            Piensa en lo que te hace sentir triste y escribe todo lo que
            sientes, luego toma un descanso, y respira. Ahora, lee lo que
            escribiste y dale la vuelta a la hoja para escribir soluciones o
            alternativas para resolver la situación que provocó esa emoción.
          </p>
          <p>Contesta la siguiente pregunta</p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>¿Cómo te sentiste después de realizar el ejercicio?</td>
              </tr>
              <tr>
                <td>
                  <textarea
                    type="text"
                    name="p1"
                    onChange={handleChange}
                    style={{
                      height: '100%',
                      width: '100%',
                      marginTop: '0.5em',
                    }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default TristezaAdtUbEjer;
