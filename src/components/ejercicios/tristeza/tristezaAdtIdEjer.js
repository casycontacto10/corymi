import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const TristezaAdtIdEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  function handleChange(event) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const tristezaAdtIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/tristeza/tristeza_adt_id_ejer`
    );
    tristezaAdtIdEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Qué le recomendarías a Diana y Jorge para que no se sientan tan desanimados?',
            respuesta: event.target.value,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = event.target.value;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Ejercicio manejo tristeza</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta las preguntas de reflexión:</p>
          <p>
            Diana y Jorge están muy tristes porque sus hijos y nietos ya casi no
            los visitan porque tuvieron que irse a otra comunidad a buscar
            trabajo. Antes de que se fueran, a Jorge le gustaba sentarse por las
            tardes en su mecedora y platicar con los vecinos que pasaban afuera
            de su casa. Diana está preocupada porque Jorge ya no sale y duerme
            casi todo el día.
          </p>
          <p>
            ¿Qué le recomendarías a Diana y Jorge para que no se sientan tan
            desanimados?
          </p>

          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="resp"
                  value="Que vayan ellos a visitar a sus hijos y nietos"
                  onChange={handleChange}
                />
              </td>
              <td>Que vayan ellos a visitar a sus hijos y nietos</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="resp"
                  value="Que hagan una actividad que les guste a los dos"
                  onChange={handleChange}
                />
              </td>
              <td>Que hagan una actividad que les guste a los dos</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="resp"
                  value="Que convivan más con sus vecinos"
                  onChange={handleChange}
                />
              </td>
              <td>Que convivan más con sus vecinos</td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
          <br />
          <p>
            Cuando tú te sientas triste porque no puedes ver a tus familiares
            puedes:
          </p>
          <ol>
            <li>Llamarlos por teléfono o mensaje</li>
            <li>Llorar y decir lo que sientes</li>
            <li>Hablar con tus amigos o amigas y contarles cómo te sientes</li>
          </ol>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default TristezaAdtIdEjer;
