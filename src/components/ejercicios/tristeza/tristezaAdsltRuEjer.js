import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const TristezaAdsltRuEjer = () => {
  const [respuestas, setRespuestas] = useState({
    r1: { texto: 'Salir a tomar aire fresco y caminar', estado: false },
    r2: { texto: 'Escuchar tu canción favorita', estado: false },
    r3: {
      texto:
        'Pedirle a alguien de confianza que te escuche y contarle cómo te sientes',
      estado: false,
    },
    r4: {
      texto:
        'Buscar un lugar en el que te sientas cómodo y llorar todo lo que quieras',
      estado: false,
    },
    r5: { texto: 'Tocar algún instrumento', estado: false },
    r6: {
      texto: 'Hacer un dibujo o una pintura de algún paisaje',
      estado: false,
    },
    r7: {
      texto: 'Escribir en una hoja lo que te hace sentir triste',
      estado: false,
    },
    r8: {
      texto: 'Comer tu platillo favorito y ayudar a prepararlo',
      estado: false,
    },
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  function setRespuesta(event) {
    event.preventDefault();
    respuestas[event.target.name].estado = event.target.checked;
    setRespuestas(respuestas);
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const tristezaAdsltRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/tristeza/tristeza_adslt_ru_ejer`
    );
    tristezaAdsltRuEjer.update(
      {
        respuestas: [respuestas],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = event.target.value;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse color="primary">
      <CardHeader tag="h1">Ejercicio manejo tristeza</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Cuando María llegó de la escuela sus padres la llamaron para hablar
            con ella de algo muy importante, le tenían una mala noticia, le
            dijeron que su papá tendría que emprender un viaje a los Estados
            Unidos ya que el trabajo en su comunidad se había acabado y no
            tenían otra opción, esa misma noche María tuvo que despedir a su
            papá con lágrimas en los ojos y un sentimiento de profunda tristeza.
            En ocasiones podemos encontrarnos con experiencias muy difíciles
            como la de María y sentirnos tristes, sin esperanza, solos y no
            sabemos qué hacer para aliviar el dolor, hay algunas actividades que
            pueden ayudarnos, distrayendo nuestra mente de la situación en la
            que estamos y que nos pueden llevar a tranquilizarnos y pensar de
            manera positiva.
          </p>
          <p>
            Analiza la lista de actividades que se presentan a continuación y
            selecciona cuál de ellas te gustaría llevar a cabo en una situación
            como la de María, o cuales podrías recomendarle a alguien que sabes
            que se siente muy triste
          </p>

          <table>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="r1"
                  value="Salir a tomar aire fresco y caminar"
                  onChange={setRespuesta}
                  checked={respuestas.r1.estado}
                />
              </td>
              <td>Salir a tomar aire fresco y caminar</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="r2"
                  value="Escuchar tu canción favorita"
                  onChange={setRespuesta}
                  checked={respuestas.r2.estado}
                />
              </td>
              <td>Escuchar tu canción favorita</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="r3"
                  value="Pedirle a alguien de confianza que te escuche y contarle cómo te sientes"
                  onChange={setRespuesta}
                  checked={respuestas.r3.estado}
                />
              </td>
              <td>
                Pedirle a alguien de confianza que te escuche y contarle cómo te
                sientes
              </td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="r4"
                  value="Buscar un lugar en el que te sientas cómodo y llorar todo lo que quieras"
                  onChange={setRespuesta}
                  checked={respuestas.r4.estado}
                />
              </td>
              <td>
                Buscar un lugar en el que te sientas cómodo y llorar todo lo que
                quieras
              </td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="r5"
                  value="Tocar algún instrumento"
                  onChange={setRespuesta}
                  checked={respuestas.r5.estado}
                />
              </td>
              <td>Tocar algún instrumento</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="r6"
                  value="Hacer un dibujo o una pintura de algún paisaje"
                  onChange={setRespuesta}
                  checked={respuestas.r6.estado}
                />
              </td>
              <td>Hacer un dibujo o una pintura de algún paisaje</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="r7"
                  value="Escribir en una hoja lo que te hace sentir triste"
                  onClick={setRespuesta}
                  checked={respuestas.r7.estado}
                />
              </td>
              <td>Escribir en una hoja lo que te hace sentir triste</td>
            </tr>
            <tr>
              <td>
                <input
                  type="checkbox"
                  name="r8"
                  value={respuestas.r8.texto}
                  onClick={setRespuesta}
                  checked={respuestas.r8.estado}
                />
              </td>
              <td>Comer tu platillo favorito y ayudar a prepararlo</td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Actualizado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert>
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default TristezaAdsltRuEjer;
