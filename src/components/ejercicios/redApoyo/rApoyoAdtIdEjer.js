import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const RApoyoAdtIdEjer = () => {
  const [preguntas, setPreguntas] = useState({
    p1: '',
    otro: '',
    disable: true,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    otro: false,
  });
  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
    if (event.target.name === 'p1' && event.target.value !== 'otro') {
      preguntas.otro = '';
      preguntas.disable = true;
    }
    if (event.target.value === 'otro') {
      preguntas.disable = false;
    }
  };

  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const rApoyoAdtIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/redApoyo/r_apoyo_adt_id_ejer`
    );
    rApoyoAdtIdEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿qué personas cercanas pueden ayudarte a resolverlo?',
            respuesta: preguntas.p1,
            otro_texto: preguntas.otro,
            otro: !preguntas.disable,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.otro = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = preguntas.p1;
          estado.otro = preguntas.otro;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse style={{ backgroundColor: '#FF647F' }}>
      <CardHeader tag="h1">Ejercicio red apoyo</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente relato y contesta la pregunta de reflexión:</p>
          <p>
            La temporada de lluvias ha sido muy intensa y la comunidad donde
            vive Ana ha sufrido severas inundaciones. Ella y algunos de sus
            familiares se han visto afectados porque sus casas se inundaron y
            perdieron muchas de sus pertenencias. Ellos están muy tristes porque
            no tienen dónde resguardarse pero algunas personas de la comunidad
            que no tuvieron daños en sus casas les han brindado ayuda y Ana y su
            familia están muy agradecidos con ellos.
          </p>
          <p>
            Cuando tú tienes algún problema, ¿qué personas cercanas pueden
            ayudarte a resolverlo?
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Mi familia"
                    onChange={handleChange}
                  />
                </td>
                <td>Mi familia</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Mis amigos o amigas"
                    onChange={handleChange}
                  />
                </td>
                <td>Mis amigos o amigas</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="Compañeros o compañeras de trabajo"
                    onChange={handleChange}
                  />
                </td>
                <td>Compañeros o compañeras de trabajo</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="otro"
                    onChange={handleChange}
                  />
                </td>
                <td>Otro</td>
                <td>
                  <textarea
                    disabled={preguntas.disable}
                    type="text"
                    name="otro"
                    value={preguntas.otro}
                    onChange={handleChange}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.otro}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default RApoyoAdtIdEjer;
