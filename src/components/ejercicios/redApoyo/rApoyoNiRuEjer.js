/* eslint-disable no-alert */
/* eslint-disable func-names */
import React, { useState } from 'react';
import {
  Card,
  CardBody,
  CardText,
  CardHeader,
  Button,
  Alert,
} from 'reactstrap';

import { storage, database } from '../../../helpers/Firebase';

const RApoyoNiRuEjer = () => {
  const [image, setImage] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  const upload = () => {
    const id = localStorage.getItem('recursoId');
    estado.error = false;
    estado.exito = false;
    estado.resp = false;
    if (!image) {
      estado.error = true;
      estado.resp = 'no hay archivo para guardar';
      setEstado(estado);
    } else {
      storage
        .ref(`/${id}/redApoyo/r_apoyo_ni_ru_ejer/${image.name}`)
        .put(image)
        .on(
          'state_changed',
          function () {
            estado.error = true;
            estado.resp = 'subiendo archivo';
            setEstado(estado);
          },
          function () {
            estado.error = true;
            estado.resp = 'error al subir archivo';
            setEstado(estado);
          },
          function () {
            const refe = database.ref();
            const rApoyoNiRuEjer = refe.child(
              `respuestasPersona/${id}/ejercicios/redApoyo/r_apoyo_ni_ru_ejer`
            );
            rApoyoNiRuEjer.update(
              {
                archivo: `/${id}/redApoyo/r_apoyo_ni_ru_ejer/${image.name}`,
                diagnostico: true,
              },
              (error) => {
                estado.error = false;
                estado.exito = false;
                estado.resp = false;
                setEstado(estado);
                if (error) {
                  estado.error = true;
                  estado.resp = 'error al guardar';
                  setEstado(estado);
                } else {
                  estado.exito = true;
                  estado.resp = `${image.name} guardado correctamente`;
                  setEstado(estado);
                }
              }
            );
          }
        );
    }
  };
  return (
    <Card body inverse style={{ backgroundColor: '#FF647F' }}>
      <CardHeader tag="h1">Ejercicio red apoyo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Cuando estamos en una situación difícil es importante recordar que a
            nuestro alrededor siempre hay alguien que nos escucha, nos apoya y
            nos puede ayudar.
          </p>
          <p>En una hoja en blanco dibuja:</p>
          <ol>
            <li>
              Los miembros de tu familia, piensa que ellos son las personas en
              las que más puedes confiar y las que están siempre para brindarte
              seguridad y protegerte
            </li>
            <li>
              Tus mejores amigos, piensa que estos también te pueden apoyar,
              puedes contarles cuando estás triste o cuando estás muy feliz
            </li>
            <li>
              Las personas que no viven contigo pero que te inspiran confianza y
              que sabes que puedes pedirles ayuda alguna vez
            </li>
          </ol>
          <p>
            <input
              type="file"
              onChange={(e) => {
                setImage(e.target.files[0]);
              }}
            />
          </p>
          <p>
            <Button onClick={upload} color="info">
              Subir
            </Button>
          </p>
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="warning">
                <p>{estado.resp}</p>
              </Alert>
            ) : null}
          </p>
          <p> Sube una foto de tus dibujos </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default RApoyoNiRuEjer;
