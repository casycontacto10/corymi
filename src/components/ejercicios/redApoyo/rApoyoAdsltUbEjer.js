import React, { useState } from 'react';
import {
  Card,
  CardBody,
  CardText,
  CardHeader,
  Button,
  Alert,
} from 'reactstrap';
import audioURL from '../audios/redApoyo/R_APOYO_ADSLT_UB.m4a';

import { database } from '../../../helpers/Firebase';

const RApoyoAdsltUbEjer = () => {
  const [preguntas, setPreguntas] = useState({
    p1: '',
    p2: '',
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    resp2: false,
  });
  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
  };

  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const rApoyoAdsltUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/redApoyo/r_apoyo_adslt_ub_ejer`
    );
    rApoyoAdsltUbEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿A quiénes imaginaste mientras narraba la historia?',
            respuesta: preguntas.p1,
          },
          {
            pregunta: '¿Te han apoyado en otros momentos de tu vida?',
            respuesta: preguntas.p2,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.resp2 = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = preguntas.p1;
          estado.resp2 = preguntas.p2;
          setEstado(estado);
        }
      }
    );
  }

  const [audio] = useState(new Audio(audioURL));
  const [isPlaying, setIsPlaying] = useState(false);
  const playPause = () => {
    if (isPlaying) {
      audio.pause();
    } else {
      audio.play();
    }
    setIsPlaying(!isPlaying);
  };
  return (
    <Card body inverse style={{ backgroundColor: '#FF647F' }}>
      <CardHeader tag="h1">Ejercicio red apoyo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Para esta actividad narraré una breve historia, en la cual haré unas
            breves pausas, en ellas quiero que imagines algunas personas. Así
            que cierra tus ojos y deja volar tu imaginación.
          </p>
          <p>
            <Button onClick={playPause}>
              {isPlaying ? 'Detener' : 'Reproducir'}
            </Button>
          </p>
          <p>Contesta las siguientes preguntas</p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>¿A quiénes imaginaste mientras narraba la historia?</td>
                <td>
                  <textarea
                    name="p1"
                    onChange={handleChange}
                    rows="5"
                    style={{ height: '100%', width: '100%' }}
                  />
                </td>
              </tr>
              <tr>
                <td>¿Te han apoyado en otros momentos de tu vida?</td>
                <td>
                  <textarea
                    name="p2"
                    onChange={handleChange}
                    rows="5"
                    style={{ height: '100%', width: '100%' }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.resp2}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
          <p>
            Ahora bien, esas personas que recordaste en algunos momentos de la
            narración, son parte de tu red de apoyo, así que tenlas presentes
            cada que te sientas solo o sola, y/o necesites algo.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default RApoyoAdsltUbEjer;
