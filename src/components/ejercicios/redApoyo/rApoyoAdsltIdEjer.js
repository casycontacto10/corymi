import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const RApoyoAdsltIdEjer = () => {
  const [preguntas, setPreguntas] = useState({
    p1: '',
    otro: '',
    disable: true,
  });
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp1: false,
    otro: false,
  });
  const handleChange = (event) => {
    preguntas[event.target.name] = event.target.value;
    setPreguntas(preguntas);
    if (event.target.name === 'p1' && event.target.value !== 'otro') {
      preguntas.otro = '';
      preguntas.disable = true;
    }
    if (event.target.value === 'otro') {
      preguntas.disable = false;
    }
  };

  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const rApoyoAdsltIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/redApoyo/r_apoyo_adslt_id_ejer`
    );
    rApoyoAdsltIdEjer.update(
      {
        respuestas: [
          {
            pregunta: '¿a quiénes les pides ayuda para resolverlo?',
            respuesta: preguntas.p1,
            otro_texto: preguntas.otro,
            otro: !preguntas.disable,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp1 = false;
        estado.otro = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp1 = preguntas.p1;
          estado.otro = preguntas.otro;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse style={{ backgroundColor: '#FF647F' }}>
      <CardHeader tag="h1">Ejercicio red apoyo</CardHeader>
      <CardBody>
        <CardText>
          <p>Lee el siguiente cuento y contesta la pregunta de reflexión:</p>
          <p>El tigre y el grillo</p>
          <p>
            Un día, mientras el tigre caminaba por el monte se espantó al
            escuchar un rechinido. Con miedo, comenzó a investigar y descubrió
            que el ruido provenía de una hoja bajo la que descansaba un grillo.
          </p>
          <p>
            Confiado de su tamaño y su poder, el tigre retó al grillo a una
            pelea, quien tranquilo aceptó. Cada uno llamó a sus amigos. El tigre
            trajo consigo al león, al coyote, al jabalí y al lobo; mientras que
            el grillo llamó a las avispas, los mosquitos y los abejorros. El
            tigre y sus amigos comenzaron confiados, sin embargo ante el ataque
            de sus oponentes salieron corriendo al ser picoteados por el grillo
            y sus amigos. Así fue como aquellos animales grandes fueron vencidos
            por el grillo y sus pequeños amigos. Así lo cuentan los tojolabales.
          </p>
          <p>
            En el cuento, parecía que el grillo perdería la pelea ante el tigre
            pero supo elegir muy bien a los amigos que le ayudaron a ganar.
          </p>
          <p>
            En el cuento, parecía que el grillo perdería la pelea ante el tigre
            pero supo elegir muy bien a los amigos que le ayudaron a ganar.
            Cuando tú tienes algún problema, ¿a quiénes les pides ayuda para
            resolverlo?
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="A mis papás u otro familiar"
                    onChange={handleChange}
                  />
                </td>
                <td>A mis papás u otro familiar</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="A nadie"
                    onChange={handleChange}
                  />
                </td>
                <td>A nadie</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="A mis amigos"
                    onChange={handleChange}
                  />
                </td>
                <td>A mis amigos</td>
              </tr>
              <tr>
                <td>
                  <input
                    type="radio"
                    name="p1"
                    value="otro"
                    onChange={handleChange}
                  />
                </td>
                <td>Otro </td>
                <td>
                  <textarea
                    disabled={preguntas.disable}
                    name="otro"
                    value={preguntas.otro}
                    onChange={handleChange}
                    rows="5"
                    style={{ height: '100%', width: '100%' }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp1}</p>
                <p>{estado.otro}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default RApoyoAdsltIdEjer;
