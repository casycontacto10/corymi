import React, { useState } from 'react';
import {
  Card,
  CardBody,
  CardText,
  CardHeader,
  Button,
  Alert,
} from 'reactstrap';
import audioURL from '../audios/redApoyo/R_APOYO_NI_ID.mpeg';

import { database } from '../../../helpers/Firebase';

const RApoyoNiIdEjer = () => {
  const [audio] = useState(new Audio(audioURL));
  const [isPlaying, setIsPlaying] = useState(false);
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  const playPause = () => {
    if (isPlaying) {
      audio.pause();
    } else {
      audio.play();
    }
    setIsPlaying(!isPlaying);
  };
  const [respuestas, setRespuestas] = useState('');
  const handleChange = (event) => {
    setRespuestas(event.target.value);
  };
  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const rApoyoNiIdEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/redApoyo/r_apoyo_ni_id_ejer`
    );
    rApoyoNiIdEjer.update(
      {
        respuestas: [
          {
            pregunta: 'Ahora escribe la palabra que tú formaste:',
            respuesta: respuestas,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.resp = false;
        estado.error = false;
        estado.exito = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.resp = respuestas;
          estado.exito = true;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse style={{ backgroundColor: '#FF647F' }}>
      <CardHeader tag="h1">Ejercicio red apoyo</CardHeader>
      <CardBody>
        <CardText>
          <p>Reproduce el siguiente audio y contesta las preguntas:</p>
          <p>
            <Button onClick={playPause}>
              {isPlaying ? 'Detener' : 'Reproducir'}
            </Button>
          </p>
          <p>
            ¿Qué palabra formaste con todos los seres queridos que te apoyan? Yo
            por ejemplo forme la palabra: HAPMO
          </p>
          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>Ahora escribe la palabra que tú formaste.</td>
              </tr>
              <tr>
                <td>
                  <textarea
                    name="respuestas"
                    onChange={handleChange}
                    style={{ height: '100%', width: '100%', marginTop: '1em' }}
                    rows="5"
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default RApoyoNiIdEjer;
