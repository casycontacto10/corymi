import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const RApoyoAdsltRuEjer = () => {
  const [respuestas, setRespuestas] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  const handleChange = (event) => {
    setRespuestas(event.target.value);
  };
  function setRespuesta(event) {
    event.preventDefault();

    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const rApoyoAdsltRuEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/redApoyo/r_apoyo_adslt_ru_ejer`
    );
    rApoyoAdsltRuEjer.update(
      {
        respuestas: [
          {
            pregunta:
              'A continuación escribe quienes fueron las personas en las que pensaste y por qué las elegiste a ellas',
            respuesta: respuestas,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.resp = false;
        setEstado(estado);

        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.resp = respuestas;
          setEstado(estado);
        }
      }
    );
  }
  return (
    <Card body inverse style={{ backgroundColor: '#FF647F' }}>
      <CardHeader tag="h1">Ejercicio red apoyo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Mariana es una joven de 15 años últimamente se ha sentido triste, ha
            dormido más horas de lo habitual, come menos y ha dejado de hacer
            sus actividades favoritas, no entiende que es lo que le pasa, solo
            que tiene muchas ganas de llorar siempre y se siente muy sola.
          </p>
          <p>
            Imagina que estás pasando por una situación como la de Mariana, pero
            no sabes a quien contárselo o a quien acudir para que te ayude,
            reflexiona quiénes son las personas más cercanas a ti en quienes
            puedes confiar para contarles tu problema y que te puedan ayudar.
          </p>

          <form onSubmit={setRespuesta}>
            <table>
              <tr>
                <td>
                  A continuación escribe quienes fueron las personas en las que
                  pensaste y por qué las elegiste a ellas.
                </td>
              </tr>
              <tr>
                <td>
                  <textarea
                    name="respuestas"
                    onChange={handleChange}
                    rows="5"
                    style={{
                      height: '100%',
                      width: '100%',
                      marginTop: '0.5em',
                    }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="4">
                  <input type="submit" />
                </td>
              </tr>
            </table>
          </form>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.resp}</p>
                <p>Registrado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert color="danger">
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default RApoyoAdsltRuEjer;
