/* eslint-disable func-names */
/* eslint-disable no-alert */
import React, { useState } from 'react';
import {
  Card,
  CardBody,
  CardText,
  CardHeader,
  Button,
  Alert,
} from 'reactstrap';

import { storage, database } from '../../../helpers/Firebase';

const RApoyoNiUbEjer = () => {
  const [image, setImage] = useState('');
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    resp: false,
  });
  const upload = () => {
    const id = localStorage.getItem('recursoId');
    estado.error = false;
    estado.exito = false;
    estado.resp = false;
    setEstado(estado);
    if (!image) {
      estado.error = true;
      estado.resp = 'no hay archivo para guardar';
      setEstado(estado);
    } else {
      storage
        .ref(`/${id}/redApoyo/r_apoyo_ni_ub_ejer/${image.name}`)
        .put(image)
        .on(
          'state_changed',
          function () {
            estado.error = true;
            estado.resp = 'subiendo archivo';
            setEstado(estado);
          },
          function () {
            estado.error = true;
            estado.resp = 'error al subir archivo';
            setEstado(estado);
          },
          function () {
            const refe = database.ref();
            const rApoyoNiUbEjer = refe.child(
              `respuestasPersona/${id}/ejercicios/redApoyo/r_apoyo_ni_ub_ejer`
            );
            rApoyoNiUbEjer.update(
              {
                archivo: `/${id}/redApoyo/r_apoyo_ni_ub_ejer/${image.name}`,
                diagnostico: true,
              },
              (error) => {
                estado.error = false;
                estado.exito = false;
                estado.resp = false;
                setEstado(estado);
                if (error) {
                  estado.error = true;
                  estado.resp = 'error al guardar';
                  setEstado(estado);
                } else {
                  estado.exito = true;
                  estado.resp = `${image.name} guardado correctamente`;
                  setEstado(estado);
                }
              }
            );
          }
        );
    }
  };
  return (
    <Card body inverse style={{ backgroundColor: '#FF647F' }}>
      <CardHeader tag="h1">Ejercicio red apoyo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Para esta actividad te voy a pedir que tengas a la mano una hoja y
            colores. A continuación, hay algunas breves historias, al final de
            cada una de ellas están unas preguntas. Cuando termines de leerlas,
            quiero que dibujes a las personas que vengan a tu mente…
          </p>

          <p>Recuerda que puede ser más que una persona.</p>
          <p>
            Imagina que te peleaste con tu mejor amigo en la escuela, ya que te
            dijo que estabas haciendo las cosas mal en una actividad, eso te
            hace sentir molesto. ¿A quién le contaste? (Dibújala).
          </p>
          <p>
            Ahora, imagina que vas a la papelería o a la tiendita de la esquina
            tu solo(a), un desconocido te está molestando, y te sientes
            asustado. En el camino te encuentras a alguien que tú quieres mucho
            y te hace sentir seguro.
          </p>
          <p>¿A quién o quienes te imaginaste? (Dibujala).</p>
          <p>Reflexión…</p>
          <p>
            Quiero que veas esas personas que dibujaste. Ellas son parte de tu
            red de apoyo, esto quiere decir, que cuentas con ellas cada que se
            te presente alguna dificultad. Esas personas, son muy especiales en
            tu vida, ya que, son un rayo de luz en tus días difíciles.
          </p>
          <p>
            <input
              type="file"
              onChange={(e) => {
                setImage(e.target.files[0]);
              }}
            />
          </p>
          <p>
            <Button onClick={upload} color="info">
              Subir
            </Button>
          </p>
          <p> Sube una foto de tus dibujos </p>
          {estado.exito ? (
            <Alert>
              <p>{estado.resp}</p>
              <p>Registrado correctamente</p>
            </Alert>
          ) : null}
          {estado.error ? (
            <Alert color="warning">
              <p>{estado.resp}</p>
            </Alert>
          ) : null}
        </CardText>
      </CardBody>
    </Card>
  );
};

export default RApoyoNiUbEjer;
