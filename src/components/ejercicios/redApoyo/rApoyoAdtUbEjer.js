import React, { useState } from 'react';
import { Card, CardBody, CardText, CardHeader, Alert } from 'reactstrap';

import { database } from '../../../helpers/Firebase';

const RApoyoAdtUbEjer = () => {
  const [estado, setEstado] = useState({
    exito: false,
    error: false,
    respuesta: false,
  });
  function handleChange(event) {
    const id = localStorage.getItem('recursoId');
    const refe = database.ref();
    const rApoyoAdtUbEjer = refe.child(
      `respuestasPersona/${id}/ejercicios/redApoyo/r_apoyo_adt_ub_ejer`
    );
    rApoyoAdtUbEjer.update(
      {
        respuestas: [
          {
            pregunta:
              '¿Qué habilidades le ayudaron a Roberto a construir su red de apoyo?',
            respuesta: event.target.value,
          },
        ],
        diagnostico: true,
      },
      (error) => {
        estado.error = false;
        estado.exito = false;
        estado.respuesta = false;
        setEstado(estado);
        if (error) {
          estado.error = true;
          setEstado(estado);
        } else {
          estado.exito = true;
          estado.respuesta = event.target.value;
          setEstado(estado);
        }
      }
    );
  }

  return (
    <Card body inverse style={{ backgroundColor: '#FF647F' }}>
      <CardHeader tag="h1">Ejercicio red apoyo</CardHeader>
      <CardBody>
        <CardText>
          <p>
            Contar con personas de confianza es necesario para encontrar refugio
            o consuelo en situaciones problemáticas. Recuerda que ayudar, tener
            disponibilidad para escuchar y ser amable con los demás, son
            características que la mayoría de las personas apreciamos y
            valoramos para formar amistad y redes de apoyo.
          </p>
          <p>A continuación, te doy un ejemplo:</p>
          <p>
            -Roberto es maestro de primaria en una comunidad rural del estado de
            Michoacán y vive solo porque él y su familia son originarios de
            Tabasco, pero la falta de empleo en su lugar de origen lo llevó a
            buscar trabajo en otro lugar, y fue así que lo encontró en
            Michoacán. Roberto extrañaba mucho a su familia, y recuerda que los
            primeros meses lejos de casa fueron difíciles, tanto que quería
            regresarse a su comunidad, pero el trato amable y preocupación que
            mostraba ante el aprendizaje de los niños y niñas lo llevaron a ser
            muy apreciado no sólo por sus alumnos sino también por sus familias.
            Ahora, goza del afecto de sus compañeros de trabajo y de la
            comunidad ya que cada que pueden lo invitan a sus reuniones
            familiares, en las cuales ha hecho buenos amigos, y si bien está
            lejos de su familia, ahora ha construido una red de apoyo cada vez
            más fuerte y más grande.
          </p>
          <p>Contesta la siguiente pregunta eligiendo una opción:</p>
          <p>
            ¿Qué habilidades le ayudaron a Roberto a construir su red de apoyo?
          </p>
          <table>
            <tr>
              <td>
                <input
                  type="radio"
                  name="resp"
                  value="Solidaridad por los demás"
                  onChange={handleChange}
                />
              </td>
              <td>Solidaridad por los demás</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="resp"
                  value="Buen trato a las personas que lo rodean"
                  onChange={handleChange}
                />
              </td>
              <td>Buen trato a las personas que lo rodean</td>
            </tr>
            <tr>
              <td>
                <input
                  type="radio"
                  name="resp"
                  value="Quejarse de vivir lejos"
                  onChange={handleChange}
                />
              </td>
              <td>Quejarse de vivir lejos</td>
            </tr>
          </table>
          <br />
          <p>
            {estado.exito ? (
              <Alert>
                <p>{estado.respuesta}</p>
                <p>Actualizado correctamente</p>
              </Alert>
            ) : null}
            {estado.error ? (
              <Alert>
                <p>Error al guardar</p>
              </Alert>
            ) : null}
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default RApoyoAdtUbEjer;
