import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const RApoyoMedio = () => {
  return (
    <Card body inverse style={{ backgroundColor: '#FF647F' }}>
      <CardHeader tag="h1">Red de apoyo</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: MEDIO</CardTitle>
        <CardText>
          <p>
            Te falta mejorar la calidad de la relación con el círculo de
            personas que te rodean para que crezca la confianza y apoyo que
            pueden darse entre sí.
          </p>
          <p>
            A continuación, te pedimos que realices un ejercicio que te ayudará
            a identificar a las personas que pueden ayudarte en una situación
            difícil.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default RApoyoMedio;
