import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const RApoyoBajo = () => {
  return (
    <Card body inverse style={{ backgroundColor: '#FF647F' }}>
      <CardHeader tag="h1">Red de apoyo</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: BAJO</CardTitle>
        <CardText>
          <p>
            Tu puntaje en Red de apoyo indica que para ti es difícil encontrar
            personas de confianza en quienes buscar refugio y consuelo cuando
            tienes situaciones problemáticas.{' '}
          </p>
          <p>
            Es difícil que cuentes con una persona de confianza a quien contarle
            lo que te pasa y con quien puedas desahogarte en situaciones
            difíciles, o que pueda darte un consejo. Necesitas ampliar el
            círculo de personas que te rodean y la calidad de la relación de
            confianza que tienes con cada una de ellas.
          </p>
          <p>
            A continuación, te pedimos que realices un ejercicio que te ayudará
            a identificar a las personas que pueden ayudarte en una situación
            difícil.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default RApoyoBajo;
