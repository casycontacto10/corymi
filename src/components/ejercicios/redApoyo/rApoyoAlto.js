import React from 'react';
import { Card, CardBody, CardText, CardHeader, CardTitle } from 'reactstrap';

const RApoyoAlto = () => {
  return (
    <Card body inverse style={{ backgroundColor: '#FF647F' }}>
      <CardHeader tag="h1">Red de apoyo</CardHeader>
      <CardBody>
        <CardTitle tag="h5">DIAGNÓSTICO: ALTO</CardTitle>
        <CardText>
          <p>
            Felicitaciones tu puntaje en Red de apoyo indica que cuentas con
            personas a tu alrededor en las que puedes confiar y buscar refugio
            en una situación problemática.
          </p>
          <p>
            Por lo general tienes a tu alrededor personas con las que sabes que
            cuentas en caso de que necesites buscar ayuda, con quienes puedes
            recurrir a platicar de lo que te pasa. Tienes al menos con una
            persona de tu confianza a quien puedes pedirle un consejo.
          </p>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default RApoyoAlto;
