/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-key */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/display-name */
import React, { useState } from 'react';
import { NotificationManager } from 'components/common/react-notifications';
import { CSVLink } from 'react-csv';
import { Row,Button } from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';
import TableContainer from 'components/adminResultados/tableContainer';
import { useHistory } from 'react-router-dom';
import { database, auth } from '../../helpers/Firebase';

const sumaPuntuacion = (respuestasArray) => {
  if (!respuestasArray) return 'no contesto';
  return (
    respuestasArray.reduce(
      (accumulator, currentValue) => accumulator + Number(currentValue.value),
      0
    ) / respuestasArray.length
  );
};

const diagnostico = (recursosArray, suma) => {
  const sumaTem =
    (suma < 2.2 && suma > 2) || (suma < 3.2 && suma > 3)
      ? Math.trunc(suma)
      : suma;

  return recursosArray
    .filter((d) => sumaTem >= d.min && sumaTem <= d.max)
    .map((r) => {
      return r.diagnostico;
    });
};

const TableResultados = () => {
  const [resultados, setResultados] = useState([]);
  const history = useHistory();

  React.useMemo(() => {
    const refe = database.ref();
    refe
      .child(`respuestasPersona/`)
      .get()
      .then((snapshot) => {
        refe
          .child('recursos')
          .get()
          .then((snapshotRecursos) => {
            const newResultado = [];
            const respuestaPersona = snapshot.val();
            const recursos = snapshotRecursos.val();
            // const arrayTable = [];
            Object.keys(respuestaPersona).forEach((key) => {
              const temp = [];
              temp.push({
                id: key,
                iniciales: respuestaPersona[key].preguntasGenerales[0]
                  ? respuestaPersona[key].preguntasGenerales[0].value
                  : 'sin respuesta',
                enojoPuntuacion: false,
                enojoClasificacion: false,
                autocontrolPuntuacion: false,
                autocontrolClasificacion: false,
                equilibrioPuntuacion: false,
                equilibrioClasificacion: false,
                optimismoPuntuacion: false,
                optimismoClasificacion: false,
                redApoyoPuntuacion: false,
                redApoyoClasificacion: false,
                reflexionPuntuacion: false,
                reflexionClasificacion: false,
                solAyudaPuntuacion: false,
                solAyudaClasificacion: false,
                tristezaPuntuacion: false,
                tristezaClasificacion: false,
              });
              /* ENOJO */
              const recursoEnojo = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.enojo
                : false;
              const enojoSumPuntuacion = sumaPuntuacion(recursoEnojo);

              temp[0].enojoPuntuacion = enojoSumPuntuacion;

              const categoriaEnojo = diagnostico(
                recursos.enojo.puntuacion,
                enojoSumPuntuacion
              );
              temp[0].enojoClasificacion = categoriaEnojo.length
                ? categoriaEnojo[0]
                : 'no contesto';
              /* ENOJO */
              /* AUTOCONTROL */
              const recursoAutocontrol = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.autocontrol
                : false;

              const autocontrolSumPuntuacion =
                sumaPuntuacion(recursoAutocontrol);

              temp[0].autocontrolPuntuacion = autocontrolSumPuntuacion;

              const categoriaAutocontrol = diagnostico(
                recursos.autocontrol.puntuacion,
                autocontrolSumPuntuacion
              );
              temp[0].autocontrolClasificacion = categoriaAutocontrol.length
                ? categoriaAutocontrol[0]
                : 'no contesto';

              /* AUTOCONTROL */
              /* EQUILIBRIO */
              const recursoEquilibrio = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.equilibrio
                : false;

              const equilibrioSumPuntuacion = sumaPuntuacion(recursoEquilibrio);

              temp[0].equilibrioPuntuacion = equilibrioSumPuntuacion;

              const categoriaEquilibrio = diagnostico(
                recursos.equilibrio.puntuacion,
                equilibrioSumPuntuacion
              );
              temp[0].equilibrioClasificacion = categoriaEquilibrio.length
                ? categoriaEquilibrio[0]
                : 'no contesto';
              /* EQUILIBRIO */
              /*  OPTIMISMO */
              const recursoOptimismo = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.optimismo
                : false;

              const optimismoSumPuntuacion = sumaPuntuacion(recursoOptimismo);

              temp[0].optimismoPuntuacion = optimismoSumPuntuacion;

              const categoriaOptimismo = diagnostico(
                recursos.optimismo.puntuacion,
                optimismoSumPuntuacion
              );
              temp[0].optimismoClasificacion = categoriaOptimismo.length
                ? categoriaOptimismo[0]
                : 'no contesto';
              /* OPTIMISMO */
              /*  REDAPOYO */
              const recursoRedApoyo = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.redApoyo
                : false;

              const redApoyoSumPuntuacion = sumaPuntuacion(recursoRedApoyo);

              temp[0].redApoyoPuntuacion = redApoyoSumPuntuacion;

              const categoriaRedApoyo = diagnostico(
                recursos.redApoyo.puntuacion,
                redApoyoSumPuntuacion
              );
              temp[0].redApoyoClasificacion = categoriaRedApoyo.length
                ? categoriaRedApoyo[0]
                : 'no contesto';
              /* REDAPOYO */
              /*  REFLEXION */
              const recursoReflexion = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.reflexion
                : false;

              const reflexionSumPuntuacion = sumaPuntuacion(recursoReflexion);

              temp[0].reflexionPuntuacion = reflexionSumPuntuacion;

              const categoriaReflexion = diagnostico(
                recursos.reflexion.puntuacion,
                reflexionSumPuntuacion
              );
              temp[0].reflexionClasificacion = categoriaReflexion.length
                ? categoriaReflexion[0]
                : 'no contesto';
              /* REFLEXION */
              /*  SOLAYUDA */
              const recursoSolAyuda = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.solAyuda
                : false;

              const solAyudaSumPuntuacion = sumaPuntuacion(recursoSolAyuda);

              temp[0].solAyudaPuntuacion = solAyudaSumPuntuacion;

              const categoriaSolAyuda = diagnostico(
                recursos.solAyuda.puntuacion,
                solAyudaSumPuntuacion
              );
              temp[0].solAyudaClasificacion = categoriaSolAyuda.length
                ? categoriaSolAyuda[0]
                : 'no contesto';
              /* SOLAYUDA */
              /*  TRISTEZA */
              const recursoTristeza = respuestaPersona[key].recursos
                ? respuestaPersona[key].recursos.tristeza
                : false;

              const tristezaSumPuntuacion = sumaPuntuacion(recursoTristeza);

              temp[0].tristezaPuntuacion = tristezaSumPuntuacion;

              const categoriaTristeza = diagnostico(
                recursos.tristeza.puntuacion,
                tristezaSumPuntuacion
              );
              temp[0].tristezaClasificacion = categoriaTristeza.length
                ? categoriaTristeza[0]
                : 'no contesto';
              /* TRISTEZA */

              newResultado.push(temp[0]);
            });
            setResultados(newResultado);
          })
          .catch(() => {
            NotificationManager.warning(
              'ERROR RECURSOS',
              '',
              3000,
              null,
              null,
              ''
            );
          });
      })
      .catch(() => {
        NotificationManager.warning(
          'ERRORRESPUESTAS',
          '',
          3000,
          null,
          null,
          ''
        );
      });
  }, []);

  const headers = [
    { key: 'iniciales', name: 'Iniciales', label: 'Iniciales' },
    { key: 'enojoPuntuacion', name: 'Puntaje enojo', label: 'Puntaje enojo' },
    {
      key: 'enojoClasificacion',
      name: 'Diagnostico enojo',
      label: 'Diagnostico enojo',
    },
    {
      key: 'tristezaPuntuacion',
      name: 'Puntaje tristeza',
      label: 'Puntaje tristeza',
    },
    {
      key: 'tristezaClasificacion',
      name: 'Diagnostico tristeza',
      label: 'Diagnostico tristeza',
    },
    {
      key: 'autocontrolPuntuacion',
      name: 'Puntaje autocontrol',
      label: 'Puntaje autocontrol',
    },
    {
      key: 'autocontrolClasificacion',
      name: 'Diagnostico autocontrol',
      label: 'Diagnostico autocontrol',
    },
    {
      key: 'equilibrioPuntuacion',
      name: 'Puntaje equilibrio',
      label: 'Puntaje equilibrio',
    },
    {
      key: 'equilibrioClasificacion',
      name: 'Diagnostico equilibrio',
      label: 'Diagnostico equilibrio',
    },
    {
      key: 'optimismoPuntuacion',
      name: 'Puntaje optimismo',
      label: 'Puntaje optimismo',
    },
    {
      key: 'optimismoClasificacion',
      name: 'Diagnostico optimismo',
      label: 'Diagnostico optimismo',
    },
    {
      key: 'redApoyoPuntuacion',
      name: 'Puntaje red apoyo',
      label: 'Puntaje red apoyo',
    },
    {
      key: 'redApoyoClasificacion',
      name: 'Diagnostico red apoyo',
      label: 'Diagnostico red apoyo',
    },
    {
      key: 'reflexionPuntuacion',
      name: 'Puntaje reflexion',
      label: 'Puntaje reflexion',
    },
    {
      key: 'reflexionClasificacion',
      name: 'Diagnostico reflexion',
      label: 'Diagnostico reflexion',
    },
    {
      key: 'solAyudaPuntuacion',
      name: 'Puntaje solicitar ayuda',
      label: 'Puntaje solicitar ayuda',
    },
    {
      key: 'solAyudaClasificacion',
      name: 'Diagnostico solicitar ayuda',
      label: 'Diagnostico solicitar ayuda',
    },
  ];

  return (
    <>
      <div>
        <Row>
          <Colxx xxs="12">
            <CSVLink
              data={resultados}
              headers={headers}
              filename="corymidb.csv"
              className="btn btn-primary"
            >
              Descargar .csv
            </CSVLink>
          </Colxx>
        </Row>
        <TableContainer data={resultados} />
      </div>
      <Row className="text-center">
        <Colxx xxs="12" className="mb-5">
          <Button
            onClick={() => {
              auth
                .signOut()
                .then(() => {
                  history.push({
                    pathname: '/',
                  });
                })
                .catch((error) => {
                  console.log(error); // An error happened.
                });
            }}
            className="btn-lg primary"
          >Salir
          </Button>
        </Colxx>
      </Row>
    </>
  );
};
export default TableResultados;
