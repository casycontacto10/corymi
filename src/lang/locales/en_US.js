/* Gogo Language Texts

Table of Contents

01.General
02.User Login, Logout, Register
03.Menu
04.Error Page
*/

module.exports = {
  /* 01.General */
  'general.copyright': 'Gogo React © 2018 All Rights Reserved.',

  'unauthorized.title': 'Unauthorized Access Attempt',
  'unauthorized.detail':
    'You are not authorized to view the page you are trying to access.',

  /* 02.User Login, Logout, Register */
  'user.login-title': 'Login',
  'user.register': 'Register',
  'user.forgot-password': 'Forgot Password',
  'user.email': 'E-mail',
  'user.password': 'Password',
  'user.forgot-password-question': 'Forget password?',
  'user.fullname': 'Full Name',
  'user.login-button': 'LOGIN',
  'user.register-button': 'REGISTER',
  'user.reset-password-button': 'RESET',
  'user.buy': 'BUY',
  'user.username': 'Username',

  /* 03.Menu */
  'menu.home': 'Home',
  'menu.app': 'Home',
  'menu.dashboards': 'Dashboards',
  'menu.gogo': 'Gogo',
  'menu.start': 'Start',
  'menu.second-menu': 'Second Menu',
  'menu.second': 'Second',
  'menu.ui': 'UI',
  'menu.charts': 'Charts',
  'menu.chat': 'Chat',
  'menu.survey': 'Survey',
  'menu.todo': 'Todo',
  'menu.search': 'Search',
  'menu.docs': 'Docs',
  'menu.blank-page': 'Blank Page',

  /* 04.Error Page */
  'pages.error-title': 'Ooops... looks like an error occurred!',
  'pages.error-code': 'Error code',
  'pages.go-back-home': 'GO BACK HOME',

  /* 07.21.Wizards */
  'wizard.step-name-1': 'Step 1',
  'wizard.step-name-2': 'Step 2',
  'wizard.step-name-3': 'Step 3',
  'wizard.step-desc-1': 'First step description',
  'wizard.step-desc-2': 'Second step description',
  'wizard.step-desc-3': 'Third step description',
  'wizard.content-1': 'Step content for first step.',
  'wizard.content-2': 'Step content for second step.',
  'wizard.content-3': 'Last step content!',
  'wizard.content-thanks': 'Thank You!',
  'wizard.next': 'Next',
  'wizard.prev': 'Back',
  'wizard.registered': 'Your registration completed successfully!',
  'wizard.async': 'Async save for 3 seconds!',
};
