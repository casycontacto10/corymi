/* Gogo Language Texts

Table of Contents

01.General
02.User Login, Logout, Register
03.Menu
04.Error Page
*/

module.exports = {
  /* 01.General */
  'general.copyright': 'Gogo React © Todos los derechos reservados.',

  'unauthorized.title': 'Unauthorized Access Attempt',
  'unauthorized.detail':
    'You are not authorized to view the page you are trying to access.',

  /* 02.Inicio de sesión de usuario, cierre de sesión, registro */
  'user.login-title': 'Iniciar sesión',
  'user.register': 'Registro',
  'user.forgot-password': 'Se te olvidó tu contraseña',
  'user.email': 'Email',
  'user.password': 'Contraseña',
  'user.forgot-password-question': '¿Contraseña olvidada?',
  'user.fullname': 'Nombre completo',
  'user.login-button': 'INICIAR SESIÓN',
  'user.register-button': 'REGISTRO',
  'user.reset-password-button': 'REINICIAR',
  'user.buy': 'INICIAR DIAGNÓSTICO',
  'user.username': 'Nombre de Usuario',

  /* 03.Menú */
  'menu.home': 'Inicio',
  'menu.app': 'Inicio',
  'menu.dashboards': 'Tableros',
  'menu.gogo': 'CORYMI',
  'menu.start': 'Comienzo',
  'menu.second-menu': 'Segundo menú',
  'menu.second': 'Segundo',
  'menu.ui': 'IU',
  'menu.charts': 'Gráficos',
  'menu.chat': 'Chatea',
  'menu.survey': 'Encuesta',
  'menu.todo': 'Notas',
  'menu.search': 'Búsqueda',
  'menu.docs': 'Docs',
  'menu.blank-page': 'Blank Page',

  /* 04.Error Page */
  'pages.error-title': 'Vaya, parece que ha ocurrido un error!',
  'pages.error-code': 'Código de error',
  'pages.go-back-home': 'REGRESAR A INICIO',

   /* 07.21.Wizards */
   'wizard.step-name-1': 'Paso 1',
   'wizard.step-name-2': 'Paso 2',
   'wizard.step-name-3': 'Paso 3',
   'wizard.step-desc-1': 'Descripción del primer paso',
   'wizard.step-desc-2': 'Descripción del segundo paso',
   'wizard.step-desc-3': 'Descripción del tercer paso',
   'wizard.content-1': 'Contenido para el primer paso.',
   'wizard.content-2': 'Contenido para el segundo paso.',
   'wizard.content-3': 'Contenido del último paso!',
   'wizard.content-thanks': 'Gracias!',
   'wizard.next': 'Siguiente',
   'wizard.prev': 'Anterior',
   'wizard.registered': '¡Su registro se completó con éxito!',
   'wizard.async': '¡Ahorro asíncrono durante 3 segundos!',
   
};
